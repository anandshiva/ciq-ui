import { mount } from '@vue/test-utils'
import Button from '../src/components/button/Button.vue'

describe('Button Component', () => {
    test('is a Vue instance', () => {
        const wrapper = mount(Button)
        expect(wrapper.isVueInstance()).toBeTruthy()
    })
    test('is props set shown', () => {
        const wrapper = mount(Button, { propsData: { text: "CIQ" } })
        console.log(wrapper.html())
        expect(wrapper.text()).toBe('CIQ')
    })

})