var path = require('path')

function resolve (dir) {
  return path.join(__dirname, '.', dir)
}

// vue.config.js
module.exports = {
  configureWebpack: {
    resolve: {
      extensions: ['*', '.js', '.vue', '.json'],
      alias: {
        '@': resolve('src')
      }
    }
  }
}
