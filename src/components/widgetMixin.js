import HttpLayer from '@/utils/services/http-layer'
import transformer from '@/utils/services/data-transformer'
import { eventBus } from '@/utils/services/eventBus'
function fetch (config) {

}

function readFromLocalStorage (lsKey, storeSetter) {
  var _selectedFilters = localStorage.getItem(lsKey) || '{}'
  _selectedFilters = JSON.parse(_selectedFilters)
  for (var i in _selectedFilters) {
    this.$store.dispatch(storeSetter, {
      filterValueKey: i,
      values: _selectedFilters[i]
    })
  }
}

var oObject = {
  beforeCreate () {
    this.readFromLocalStorage = readFromLocalStorage.bind(this)
  },
  created () {
    if (this.config) {
      for (let key in this.config.widgets) {
        if (this.config.widgets[key].meta && this.config.widgets[key].meta.localFilters) {
          this.config.widgets[key].meta.localFilters = this.config.widgets[key].meta.localFilters.filter((obj) => {
            return !(obj.dimensionName === 'search')
          })
        }
      }
      fetch.call(this, this.config)
    }

    if (this.config && this.config.filters && this.config.filters.listen) {
      for (var i in this.config.filters.listen) {
        eventBus.$on(i, function (data, mapping) {
          if (mapping) {
            this.config.mapping = mapping
            if (this.config.filters.listen[i].transform) {
              this.config = this.config.filters.listen[i].transform(this.config, data, this)
            }
            this.$store.dispatch(this.config.filters.listen[i].action, this.config)
          }
        }.bind(this))
      }
    }
  },
  computed: {
    getMarketPlace () {
      return this.$store.getters.getMarketPlace
    },
    outsideIn () {
      return this.$store.getters.getOutsideIn
    },
    getColorPattern () {
      return ['#ffa800', '#bd10e0', '#ff6072', '#97cc04', '#23b5d3', '#f5d908', '#ff909d', '#ffc24c', '#d158ea', '#f8e552', '#b6dc4f', '#65cce1']
    }
  },
  data () {
    return {
      filterData: [],
      primaryFilterData: [],
      secondaryFilterData: {}
    }
  },
  methods: {
    fetchFilters (cube, endPoint, page, where) {
      var that = this
      return HttpLayer.post({
        cube: endPoint || 'FETCH_FILTERS',
        APIData: {
          cubeName: cube,
          pageName: page,
          where: where
        }
      }).then(response => {
        var data = transformer.mergeResultDimension(response.data)
        that.filterData = data
        // that.primaryFilterData = ((response && response.fullResponse && response.fullResponse.metadata || {}).dimensionMappingData || [])
      })
    },
    getComputedFn (widget) {
      return this._computedWatchers[widget.dataSource].getter()
    },
    applyFilter () {
      if (this.config) {
        fetch.call(this, this.config)
      }
    },
    getAllDates () {
      var maxDates = this.$store.getters.getMaxDate
      var returnDates = {}
      var dateRangeValues = {}

      var selectedDateRange = this.$store.getters[this.filterState.getter]['date_range'].name
      var selectedDateRangeValues = this.$store.getters.getDateRangeValues

      if (selectedDateRange) {
        for (var i in selectedDateRangeValues) {
          dateRangeValues[i] = selectedDateRangeValues[i][selectedDateRange]
        }

        for (i in maxDates) {
          returnDates[i] = maxDates[i].max_feed_date
        }
      }
      returnDates['dateRange'] = selectedDateRange
      returnDates['dateRangeValues'] = dateRangeValues

      return returnDates
    }
  }
}

export default oObject
