export default {
  shareOfVoiceData: [
    {
      name: 'Total Sales',
      key: 'Total Sales',
      data: [
        {
          key: 'Temp 1',
          amount: '48'
        },
        {
          key: 'Temp 2',
          amount: '20'
        },
        {
          key: 'Temp 3',
          amount: '70'
        },
        {
          key: 'Temp 4',
          amount: '30'
        }
      ]
    },
    {
      name: 'Share of Sponsored Search',
      key: 'Share of Sponsored Search',
      data: [
        {
          key: 'Temp 1',
          amount: '20'
        },
        {
          key: 'Temp 2',
          amount: '60'
        },
        {
          key: 'Temp 3',
          amount: '10'
        },
        {
          key: 'Temp 4',
          amount: '10'
        }
      ]
    },
    {
      name: 'Share of Sponsored Brands',
      key: 'Share of Sponsored Brands',
      data: [
        {
          key: 'Temp 1',
          amount: '10'
        },
        {
          key: 'Temp 2',
          amount: '20'
        },
        {
          key: 'Temp 3',
          amount: '20'
        },
        {
          key: 'Temp 4',
          amount: '50'
        }
      ]
    },
    {
      name: 'Share of Organic Search',
      key: 'Share of Organic Search',
      data: [
        {
          key: 'Temp 1',
          amount: '25'
        },
        {
          key: 'Temp 2',
          amount: '25'
        },
        {
          key: 'Temp 3',
          amount: '20'
        },
        {
          key: 'Temp 4',
          amount: '30'
        }
      ]
    },
    {
      name: 'Share of Page 1',
      key: 'Share of Page 1',
      data: [
        {
          key: 'Temp 1',
          amount: '40'
        },
        {
          key: 'Temp 2',
          amount: '30'
        },
        {
          key: 'Temp 3',
          amount: '20'
        },
        {
          key: 'Temp 4',
          amount: '10'
        }
      ]
    },
    {
      name: 'AMS Spend',
      key: 'AMS Spend',
      data: [
        {
          key: 'Temp 1',
          amount: '40'
        },
        {
          key: 'Temp 2',
          amount: '30'
        },
        {
          key: 'Temp 3',
          amount: '20'
        },
        {
          key: 'Temp 4',
          amount: '10'
        }
      ]
    },
    {
      name: 'Total ROI',
      key: 'Total ROI',
      data: [
        {
          key: 'Temp 1',
          amount: '40'
        },
        {
          key: 'Temp 2',
          amount: '30'
        },
        {
          key: 'Temp 3',
          amount: '20'
        },
        {
          key: 'Temp 4',
          amount: '10'
        }
      ]
    }
  ]
}
