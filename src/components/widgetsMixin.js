import { eventBus } from '@/utils/services/eventBus'
import HttpLayer from '@/utils/services/http-layer'
import transformer from '@/utils/services/data-transformer'
import Vue from 'vue'

function fetch (config, searchFilter) {
  if (config.widgets && config.widgets.length > 0) {
    for (var i = 0; i < config.widgets.length; i++) {
      if (this.$store._actions && this.$store._actions[config.widgets[i].action]) {
        var _config = config.widgets[i] || {}
        if (searchFilter !== undefined) {
          _config.searchFilter = searchFilter
        }
        this.$store.dispatch(config.widgets[i].action, _config)
      }
    }
  }

  if (config.tables && config.tables.length > 0) {
    for (i = 0; i < config.tables.length; i++) {
      if (this.$store._actions && this.$store._actions[config.tables[i].action]) {
        this.$store.dispatch(config.tables[i].action, config.tables[i] || {})
      }
    }
  }
}

function readFromLocalStorage (lsKey, storeSetter) {
  var _selectedFilters = localStorage.getItem(lsKey) || '{}'
  _selectedFilters = JSON.parse(_selectedFilters)
  for (var i in _selectedFilters) {
    this.$store.dispatch(storeSetter, {
      filterValueKey: i,
      values: _selectedFilters[i]
    })
  }
}

var oObject = {
  computed: {
    getMarketPlace () {
      return this.$store.getters.getMarketPlace
    },
    outsideIn () {
      return this.$store.getters.getOutsideIn
    }
  },
  beforeCreate () {
    this.readFromLocalStorage = readFromLocalStorage.bind(this)
  },
  created () {
    if (this.config) {
      // fetch.call(this, this.config);
      if (this.config.filters && this.config.filters.search && this.config.filters.search.emit) {
        eventBus.$on(this.config.filters.search.emit, function (data) {
          fetch.call(this, Vue.util.extend({}, this.config), data)
        }.bind(this))
      }
      if (this.config.filters && this.config.filters.listen) {
        for (var i in this.config.filters.listen) {
          eventBus.$on(i, function (data, mapping) {
            this.config.mapping = mapping
            if (this.config.filters.listen[i].transform) {
              this.config = this.config.filters.listen[i].transform(this.config, data, this)
            }
            this.$store.dispatch(this.config.filters.listen[i].action, this.config)
          }.bind(this))
        }
      }
    }
  },
  data () {
    return {
      filterData: [],
      primaryFilterData: [],
      secondaryFilterData: {}
    }
  },
  methods: {
    // page params is added as it is required for saved filters. if page is not undefined, it is added to the payload object.
    fetchFilters (cube, page, customFunction, amsLite) {
      var obj = {
        cubeName: cube
      }
      var cubeToCall = 'FETCH_FILTERS'
      if (page !== undefined) {
        obj.pageName = page
        cubeToCall = 'FETCH_FILTERS_V2'
      }
      var that = this
      return HttpLayer.post({
        cube: cubeToCall,
        APIData: obj
      }).then(response => {
        if (typeof (Worker) !== 'undefined') {
          this.$worker.run(transformer.mergeResultDimension, [response.data]).then((data) => {
            that.filterData = data
            if (customFunction) {
              var fnc = customFunction()
              this.$worker.run(fnc, [that.filterData]).then((customData) => {
                that.filterData = customData
              })
            }

            let primaryFilterData = ((response.fullResponse.metadata || {}).dimensionMappingData || [])
            let dimensionList = ((response.fullResponse.metadata || {}).groupByDimensionList || [])

            if (amsLite) {
              var excludeList = dimensionList.filter((item) => {
                if (!(item && item.filterProductTag)) {
                  return false
                }

                return (item.filterProductTag.length === 1 && item.filterProductTag[0] === 'MarketingIQ')
              }).map((value) => {
                return value.name
              })

              primaryFilterData = primaryFilterData.filter((item) => {
                return !excludeList.includes(item.dimensionName)
              })
            }
            that.primaryFilterData = primaryFilterData
          })
        } else {
          var data = transformer.mergeResultDimension(response.data)
          that.filterData = data
          if (customFunction) {
            var fnc = customFunction()
            that.filterData = fnc(data)
          }
          that.primaryFilterData = ((response.fullResponse.metadata || {}).dimensionMappingData || [])
        }
      })
    },
    getComputedFn (widget) {
      return this._computedWatchers[widget.dataSource].getter()
    },
    applyFilter () {
      if (this.config) {
        fetch.call(this, this.config)
      }
    },
    getAllDates () {
      var maxDates = this.$store.getters.getMaxDate
      var returnDates = {}

      for (var i in maxDates) {
        returnDates[i] = maxDates[i].max_feed_date
      }

      return returnDates
    }
  }
}

export default oObject
