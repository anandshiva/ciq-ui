import MyButton from './Button.vue'

export default { title: 'Button' }

export const asAComponent = () => ({
  components: { MyButton },
  template: '<ciq-button text="hi" ></ciq-button>'
})
