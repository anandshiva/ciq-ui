export default {
  config: {
    search: {
      enable: true,
      emit: 'performanceOverviewSearchTriggered',
      placeholder: 'Search for a ASIN'
    },
    eventsIconMap: {
      '3P-variant': {
        icon: '3P-variant',
        color: 'u-color-red-base'
      },
      'add-on': {
        icon: 'add-on',
        color: 'u-color-blue-base'
      },
      'amazon-choice': {
        icon: 'amazon-choice',
        color: 'u-color-grey-base'
      },
      'best-seller': {
        icon: 'best-seller',
        color: 'u-color-orange-base'
      },
      'campaign': {
        icon: 'campaign',
        color: 'u-color-cyan-base'
      },
      'content-change': {
        icon: 'content-change',
        color: 'u-color-green-light'
      },
      'OOS': {
        icon: 'OOS',
        color: 'u-color-yellow-base'
      },
      'search-event': {
        icon: 'search-event',
        color: 'u-color-pink-base'
      },
      'SnS': {
        icon: 'SnS',
        color: 'u-color-purple-base'
      },
      'suppressed': {
        icon: 'suppressed',
        color: 'u-color-green-base'
      },
      'unavailable': {
        icon: 'unavailable',
        color: 'u-color-turquoise-base'
      }
    },
    chart: {
      'chartOptions': {
        'legend': false,
        'xFormat': '%m/%d/%Y',
        'timeseries': 'report_date',
        'tooltip_format': {
          'Active': null,
          'SKU': null,
          'SKU Title': null,
          'Timeline': null,
          'Search Term': null,
          'Keyword': null,
          'Campaign Name': null,
          'Campaign type': null,
          'Targeting type': null,
          'Start Date': null,
          'End Date': null,
          'Budget': {
            'pre': 'currency'
          },
          'Spend': {
            'pre': 'currency'
          },
          'Bid +': null,
          'Paid sales 1 day': {
            'pre': 'currency'
          },
          'Incremental ACoS': {
            'pre': 'currency'
          },
          'Paid sales 14 days': {
            'pre': 'currency'
          },
          'ACoS 1 day': {
            'suff': '%'
          },
          'ACoS 7 days': {
            'suff': '%'
          },
          'ACoS 14 days': {
            'suff': '%'
          },
          'ROI': null,
          'Impressions': null,
          'Clicks': null,
          'Click through rate': {
            'suff': '%'
          },
          'Orders 1 day': null,
          'Orders 7 days': null,
          'Orders 14 days': null,
          'Units 1 day': null,
          'Units 7 days': null,
          'Units 14 days': null,
          'Conversion 1 day': {
            'suff': '%'
          },
          'Conversion 7 days': {
            'suff': '%'
          },
          'Conversion 14 days': {
            'suff': '%'
          },
          'Cost per click': {
            'pre': 'currency'
          },
          'Keyword ID': null,
          'Match type keyword': null,
          'Search Term type': null,
          'Current Bid': {
            'pre': 'currency'
          },
          'Share of organice search': {
            'suff': '%'
          },
          'Share of SP Ad Space': {
            'suff': '%'
          },
          'Category': null,
          'Sub Category': null,
          'Brand': null,
          'Inventory': null,
          'Amazon revenue': {
            'pre': 'currency'
          },
          'PCOGS': {
            'pre': 'currency'
          },
          'Total sales': {
            'pre': 'currency'
          },
          'Offer price': {
            'pre': 'currency'
          },
          'Subscribe and Save Price': {
            'pre': 'currency'
          },
          'Organic sales': {
            'pre': 'currency'
          },
          'Average Selling price': {
            'pre': 'currency'
          },
          'Promo types': null,
          'Promotion Messages': null,
          'Promotions': null,
          'Category 1': null,
          'Category 1 BSR': null,
          'Category 2': null,
          'Category 2 BSR': null,
          'Category 3': null,
          'Category 3 BSR': null,
          'Category 4': null,
          'Category 4 BSR': null,
          'Avg product rating': null,
          'Total review count': null,
          'New review count': null,
          'Title Length': null,
          'Length of description': null,
          'Ordered Unit': null,
          'Subcategory (Sales Rank)': null,
          'LBB rate': null,
          'Rep OOS': {
            'suff': '%'
          },
          'Change in Traffic %': null,
          'Change in Conversion': null,
          'Brand Halo Sales 14 day': {
            'pre': 'currency'
          },
          'Brand Halo Sales 7 day': {
            'pre': 'currency'
          },
          'Brand Halo Sales 1 day': {
            'pre': 'currency'
          },
          'Ad SKU Paid sales 14 days': {
            'pre': 'currency'
          }
        },
        'axes': {
          'Active': 'y2',
          'SKU': 'y2',
          'SKU Title': 'y2',
          'Timeline': 'y2',
          'Search Term': 'y2',
          'Keyword': 'y2',
          'Campaign Name': 'y2',
          'Campaign type': 'y2',
          'Targeting type': 'y2',
          'Start Date': 'y2',
          'End Date': 'y2',
          'Budget': 'y',
          'Spend': 'y',
          'Bid +': 'y2',
          'Paid sales 1 day': 'y',
          'Incremental ACoS': 'y',
          'Paid sales 14 days': 'y',
          'ACoS 1 day': 'y2',
          'ACoS 7 days': 'y2',
          'ACoS 14 days': 'y2',
          'ROI': 'y2',
          'Impressions': 'y2',
          'Clicks': 'y2',
          'Click through rate': 'y2',
          'Orders 1 day': 'y2',
          'Orders 7 days': 'y2',
          'Orders 14 days': 'y2',
          'Units 1 day': 'y2',
          'Units 7 days': 'y2',
          'Units 14 days': 'y2',
          'Conversion 1 day': 'y2',
          'Conversion 7 days': 'y2',
          'Conversion 14 days': 'y2',
          'Cost per click': 'y',
          'Keyword ID': 'y2',
          'Match type keyword': 'y2',
          'Search Term type': 'y2',
          'Current Bid': 'y',
          'Share of organice search': 'y2',
          'Share of SP Ad Space': 'y2',
          'Category': 'y2',
          'Sub Category': 'y2',
          'Brand': 'y2',
          'Inventory': 'y2',
          'Amazon revenue': 'y',
          'PCOGS': 'y',
          'Total sales': 'y',
          'Offer price': 'y',
          'Subscribe and Save Price': 'y',
          'Organic sales': 'y',
          'Average Selling price': 'y',
          'Promo types': 'y2',
          'Promotion Messages': 'y2',
          'Promotions': 'y2',
          'Category 1': 'y2',
          'Category 1 BSR': 'y2',
          'Category 2': 'y2',
          'Category 2 BSR': 'y2',
          'Category 3': 'y2',
          'Category 3 BSR': 'y2',
          'Category 4': 'y2',
          'Category 4 BSR': 'y2',
          'Avg product rating': 'y2',
          'Total review count': 'y2',
          'New review count': 'y2',
          'Title Length': 'y2',
          'Length of description': 'y2',
          'Ordered Unit': 'y2',
          'Subcategory (Sales Rank)': 'y2',
          'LBB rate': 'y2',
          'Rep OOS': 'y2',
          'Change in Traffic %': 'y2',
          'Change in Conversion': 'y2',
          'Brand Halo Sales 14 day': 'y',
          'Brand Halo Sales 7 day': 'y',
          'Brand Halo Sales 1 day': 'y',
          'Ad SKU Paid sales 14 days': 'y'
        },
        'axis_format': {},
        'xDataKey': 'x',
        'grid': 'xy',
        'type': 'line',
        'types': {
          'content-change': 'scatter',
          'add-on': 'scatter',
          'campaign': 'scatter'
        },
        'xAxisType': 'category',
        'stack': [],
        'chartGetter': '',
        'events': [
          {
            key: 'content-change',
            icon: 'content-change'
          },
          {
            key: 'add-on',
            icon: 'add-on'
          },
          {
            key: 'campaign',
            icon: 'campaign'
          }
        ]
      }
    }
  },
  'chartData': {
    'data': {
      'data': [
        ['Paid sales 1 day', 31208.4, 41664.59, 31208.4, 49349.67, 41830.5, 37623.7, 38360.05, 35624.69, 41721.53, 48272.63, 37559.69, 42011.24, 39719.17, 54052.09, 41871.61, 35661.31, 45900.89, 35165.61, 32463.37, 45430.86, 44600.13, 38847.74, 49764.21, 75239.76, 46646.02, 38576.01, 45818.42, 41656.62, 45526.67, 45594.56],
        ['Units 7 days', 2982, 3848, 2871, 4618, 3735, 3974, 3455, 3269, 3264, 4270, 3779, 3587, 3431, 4622, 3601, 3144, 3784, 2946, 2950, 3490, 3498, 3247, 4766, 5654, 3806, 3561, 3363, 3511, 3597, 3486],
        ['Incremental ACoS', 41980.69, 48397.17, 37737.56, 58132.97, 49012.8, 44735.78, 46150.09, 42506.51, 48744.19, 55915.38, 46867.05, 48861.57, 46905.74, 63968.49, 49514.5, 42762.44, 54801.31, 40772.31, 39204.53, 53096.91, 52021.27, 45602.64, 60140.2, 83731.66, 54641.35, 45739.79, 52063.96, 49334.41, 53651.1, 51537.64],
        ['Inventory', 5119499, 27, 5142023, 154, 5115117, null, 5113625, null, 5082090, 0, 5019339, 400, 5066624, 2507, 5136404, null, 5105380, 179, 5113522, null, 5044047, 30, 5050346, null, 5101546, 362, 4795317, null, 4844789, 2312, 5018778, null, 4752078, null, 5169539, 642, 5093478, 302, 4875345, 179, 5022463, 695, 5152300, 3, 5040528, 2025, 5026615, 30, 5140729, 4818, 5142263, 65781, 5140481, 213, 5098523, 11, 4943026, 460, 5119583, 362],
        ['PCOGS', 743611.85, 812819.52, 655799.96, 1123599.73, 776054.26, 923914.54, 789416.49, 815812.24, 849560.41, 954088.41, 898870.44, 809093.23, 914173.46, 977249.83, 856325.76, 842727.2, 866290.97, 959917.92, 794315.85, 843203.46, 842087.03, 861526.34, 993674.46, 1741960.22, 930475.11, 768353.68, 977430.52, 811133.38, 849842.65, 875335.76],
        ['Orders 7 days', 2503, 2953, 2328, 3733, 3069, 3367, 2962, 2763, 2771, 3657, 3139, 2995, 2938, 3904, 2992, 2690, 3189, 2512, 2452, 2970, 2886, 2788, 3994, 4794, 3240, 3051, 2832, 2976, 3037, 2973],
        ['ACoS 1 day', 15.280501, 13.659993, 16.651767, 14.578942, 13.465151, 14.766304, 14.270758, 14.014999, 12.95928, 14.235727, 18.517991, 13.383347, 14.254527, 12.418317, 12.892387, 14.947011, 12.152618, 14.04628, 16.432829, 11.766055, 11.673755, 14.44223, 15.905628, 13.060249, 13.108835, 14.625411, 12.681625, 13.24589, 11.558697, 12.608478],
        ['Organic sales', 365048.13, 377741.06, 292516.81, 499090.05, 394934.69, 409979.75, 383439.68, 395635.06, 443534.88, 489792.75, 375701.65, 389569.43, 533119.51, 544888.64, 421020.4, 405892.61, 447587.11, 452959.98, 371825.18, 427148.72, 483083.14, 439417.16, 413101.95, 825493.64, 485919.1, 343917.59, 485840.49, 437443.66, 434373.79, 534479.46],
        ['Spend', 5189.31, 5691.38, 5196.75, 7194.66, 5632.54, 5555.63, 5474.27, 4992.8, 5406.81, 6871.96, 6955.3, 5622.51, 5661.78, 6712.36, 5398.25, 5330.3, 5578.16, 4939.46, 5334.65, 5345.42, 5206.51, 5610.48, 7915.31, 9826.5, 6114.75, 5641.9, 5810.52, 5517.79, 5262.29, 5748.78],
        ['Paid sales 14 days', 47470.09, 56029.19, 43954.45, 67813.15, 55351.06, 51728.83, 52588.11, 50081.16, 54270.12, 62951.83, 54626.53, 56614.82, 51620.51, 70552.64, 55597.07, 50297.67, 60907.04, 45670.71, 45309.84, 58028.91, 57318.92, 50346.35, 69833.1, 94978.67, 61621.28, 51737.45, 57476.77, 55172.82, 58818.66, 56243.45],
        ['Cost per click', 0.52968358, 0.53245205, 0.56284523, 0.57511271, 0.5412261, 0.48991446, 0.51271612, 0.51119074, 0.5792597, 0.53349585, 0.5699664, 0.53253552, 0.56336119, 0.53429595, 0.54571876, 0.53565471, 0.54283379, 0.56264495, 0.55269892, 0.55022337, 0.54776539, 0.55510834, 0.57274313, 0.63091493, 0.56114068, 0.54474269, 0.57632613, 0.54282243, 0.54941428, 0.57310139],
        ['ACoS 7 days', 12.361183, 11.759737, 13.770763, 12.376213, 11.491978, 12.418762, 11.861884, 11.745966, 11.092214, 12.289928, 14.84049, 11.507019, 12.070548, 10.493229, 10.902362, 12.464911, 10.178881, 12.114742, 13.607229, 10.06729, 10.008425, 12.302972, 13.161429, 11.735704, 11.190701, 12.334775, 11.16035, 11.184465, 9.808354, 11.154527],
        ['Orders 1 day', 2000, 2478, 1892, 3156, 2579, 2754, 2419, 2279, 2299, 3093, 2433, 2501, 2445, 3295, 2515, 2242, 2659, 2101, 1948, 2481, 2447, 2273, 3217, 4168, 2695, 2507, 2398, 2445, 2514, 2536],
        ['Total sales', 1293329.22, 1322459.58, 1093421.3, 1756464.33, 1322784.78, 1551383.34, 1345053.37, 1408581.74, 1412082.33, 1602105.69, 1428907.82, 1363103.81, 1536933.84, 1624239.46, 1360944.08, 1405934.12, 1419890.62, 1610771.56, 1305555.68, 1377313.85, 1410045.42, 1483050.06, 1571621.54, 2480704.72, 1529549.87, 1353995.81, 1615406.38, 1344451.43, 1408480.14, 1504983.89],
        ['report_date', '11/26/2019', '11/21/2019', '11/28/2019', '11/29/2019', '11/19/2019', '11/24/2019', '11/23/2019', '11/25/2019', '11/14/2019', '11/17/2019', '11/30/2019', '11/20/2019', '11/11/2019', '11/03/2019', '11/05/2019', '11/22/2019', '11/04/2019', '11/15/2019', '11/27/2019', '11/06/2019', '11/08/2019', '11/16/2019', '12/01/2019', '12/02/2019', '11/10/2019', '11/18/2019', '11/13/2019', '11/09/2019', '11/07/2019', '11/12/2019'],
        ['Click through rate', 0.5892, 0.6039, 0.5761, 0.5698, 0.5532, 0.6212, 0.6248, 0.6019, 0.5832, 0.5803, 0.604, 0.5675, 0.5838, 0.6211, 0.5618, 0.5987, 0.5696, 0.5842, 0.6039, 0.5833, 0.6058, 0.6057, 0.5894, 0.5149, 0.6134, 0.5397, 0.6168, 0.642, 0.5871, 0.5847],
        ['Orders 14 days', 2874, 3462, 2696, 4318, 3499, 3877, 3377, 3239, 3146, 4185, 3628, 3469, 3348, 4366, 3444, 3127, 3634, 2841, 2841, 3334, 3263, 3099, 4666, 5505, 3730, 3505, 3241, 3405, 3390, 3323],
        ['Conversion 7 days', 25.548637, 27.626532, 25.213907, 29.840128, 29.489767, 29.691358, 27.741875, 28.289137, 29.687165, 28.390653, 25.723183, 28.367115, 29.233831, 31.07538, 30.246664, 27.032459, 31.033476, 28.613737, 25.404061, 30.571282, 30.362967, 27.584842, 28.900145, 30.780096, 29.732954, 29.458337, 28.089665, 29.276931, 31.708081, 29.638122],
        ['Average Selling price', 14.41, 12.82, 14.09, 13.56, 13.13, 13.59, 14.02, 14.75, 13.94, 14.25, 13.9, 13.98, 13.6, 13.85, 13.41, 14.07, 13.79, 14.09, 15.02, 13.82, 14.39, 14.33, 14.21, 14.9, 13.21, 13.78, 15.08, 14.42, 14.41, 14.08],
        ['Brand Halo Sales 14 day', 20593.5, 23297.33, 19471.24, 29279.95, 23032.85, 24202.15, 22865.44, 21770.82, 22421.05, 27524.89, 23517.02, 23314.67, 23213.63, 31411.07, 23992.91, 21035.03, 27503.82, 19330.3, 18882.52, 24310.06, 24469.83, 22375.05, 30619.28, 33554.97, 27575.1, 23523.11, 24669.83, 25164.85, 24147.76, 24328.6],
        ['Impressions', 1662641, 1770010, 1602672, 2195467, 1881271, 1825366, 1708863, 1622794, 1600393, 2219622, 2020242, 1860397, 1721354, 2022821, 1760722, 1662129, 1803997, 1502746, 1598382, 1665564, 1569003, 1668536, 2344761, 3025037, 1776457, 1919112, 1634589, 1583370, 1631371, 1715499],
        ['Brand Halo Sales 7 day', 18018.43, 19519.83, 16991.44, 25278.44, 20440.11, 20959.98, 20136.98, 18341.27, 19529.91, 23817.15, 20339.91, 20112.84, 21042.9, 28233.54, 21043.13, 17699.23, 24389.37, 17346.54, 16233.15, 22335.07, 21769.6, 20232.22, 26147.74, 29079.76, 23917.84, 20804.01, 22040.59, 22164.57, 21543.11, 22212.43],
        ['ACoS 14 days', 10.931747, 10.157884, 11.823035, 10.609535, 10.176029, 10.73991, 10.40971, 9.969418, 9.962775, 10.91622, 12.732458, 9.931163, 10.968082, 9.513974, 9.709594, 10.597509, 9.158482, 10.815378, 11.773712, 9.21165, 9.083406, 11.143767, 11.334611, 10.346007, 9.923114, 10.904867, 10.109336, 10.000921, 8.946634, 10.221244],
        ['customer_orders', 89771, 103124, 77583, 129511, 100742, 114161, 95908, 95488, 101277, 112465, 102773, 97531, 112969, 117270, 101518, 99909, 102964, 114332, 86920, 99672, 97999, 103490, 110628, 166452, 115810, 98266, 107089, 93214, 97721, 106885],
        ['Brand Halo Sales 1 day', 13089.81, 15765.25, 12987.53, 20100.94, 16398.93, 17014.06, 15996.9, 14380.24, 15721.5, 19634.73, 15210.92, 16403.16, 16976.5, 22126.84, 16232.79, 13441.23, 19078.98, 14255.48, 12572.89, 17848.1, 17379.66, 16672.63, 20212.68, 24646.59, 18994.08, 16489.89, 18708.17, 17260.98, 17121.43, 18329.57],
        ['Clicks', 9797, 10689, 9233, 12510, 10407, 11340, 10677, 9767, 9334, 12881, 12203, 10558, 10050, 12563, 9892, 9951, 10276, 8779, 9652, 9715, 9505, 10107, 13820, 15575, 10897, 10357, 10082, 10165, 9578, 10031],
        ['Units 14 days', 3193, 4147, 3070, 4923, 3974, 4258, 3686, 3581, 3478, 4635, 4048, 3882, 3657, 4891, 3871, 3420, 4006, 3128, 3197, 3659, 3709, 3443, 5198, 5975, 4094, 3857, 3616, 3770, 3785, 3676],
        ['Conversion 14 days', 29.335511, 32.388437, 29.19961, 34.516387, 33.621601, 34.188713, 31.628735, 33.162691, 33.704735, 32.489714, 29.730394, 32.856602, 33.313433, 34.752846, 34.816013, 31.423977, 35.363955, 32.361317, 29.434314, 34.318065, 34.3293, 30.661917, 33.762663, 35.345104, 34.229604, 33.841846, 32.1464, 33.497295, 35.39361, 33.127305],
        ['Units 1 day', 2356, 3278, 2341, 3930, 3153, 3229, 2815, 2691, 2692, 3622, 2941, 2969, 2823, 3890, 3046, 2612, 3123, 2459, 2347, 2920, 2957, 2655, 3818, 4913, 3159, 2908, 2841, 2887, 2984, 2965],
        ['Conversion 1 day', 20.414413, 23.182711, 20.491715, 25.227818, 24.781397, 24.285714, 22.656177, 23.333675, 24.630384, 24.012111, 19.93772, 23.688199, 24.328358, 26.227812, 25.424586, 22.530399, 25.875827, 23.932111, 20.182346, 25.537828, 25.744345, 22.489364, 23.277858, 26.760835, 24.731577, 24.205851, 23.784963, 24.053123, 26.247651, 25.281627],
        ['content-change', null, null, 0, null, null, 0, null, 0, null, 0, null, null, 0, null, null, null, 0, null, null, null, 0, null, null, null, null, null, null, 0, null, 0],
        ['campaign', null, null, null, null, null, null, null, 0, null, null, 0, null, 0, null, null, null, 0, null, null, null, 0, null, null, null, 0, null, null, 0, null, 0],
        ['add-on', null, null, null, null, null, null, null, 0, null, null, 0, null, 0, null, null, null, 0, null, null, null, 0, null, null, null, 0, null, null, 0, null, 0]
      ],
      'xs': null,
      'legends': '__vue_devtool_undefined__'
    },
    'rows': '__vue_devtool_undefined__',
    'totalRows': '__vue_devtool_undefined__',
    'metrics': {
      'Spend': {
        'tag1': 15.469,
        'tag2': 7
      },
      'auto_cubesdk_count': {
        'tag1': 3860,
        'tag2': '__vue_devtool_undefined__'
      },
      'Total sales': {
        'tag1': 247900,
        'tag2': -2.7
      },
      'Organic sales': {
        'tag1': 14910645.11,
        'tag2': 12.84
      },
      'Impressions': {
        'tag1': 54575188,
        'tag2': 12.84
      },
      'Clicks': {
        'tag1': 320391,
        'tag2': 9.44
      },
      'Units 1 day': {
        'tag1': 91324,
        'tag2': 1.45
      },
      'Units 7 days': {
        'tag1': 110109,
        'tag2': 2.21
      },
      'Units 14 days': {
        'tag1': 117827,
        'tag2': 2.42
      },
      'Orders 1 day': {
        'tag1': 76769,
        'tag2': 0.02
      },
      'Orders 7 days': {
        'tag1': 92458,
        'tag2': 0.7
      },
      'Orders 14 days': {
        'tag1': 105832,
        'tag2': 2.4
      },
      'Paid sales 1 day': {
        'tag1': 1281722.08,
        'tag2': 19.57
      },
      'Incremental ACoS': {
        'tag1': 52.3,
        'tag2': -6.7
      },
      'Paid sales 14 days': {
        'tag1': 49604,
        'tag2': 11
      },
      'Click through rate': {
        'tag1': 0.5871,
        'tag2': -0.02
      },
      'Conversion 1 day': {
        'tag1': 23.961035,
        'tag2': -2.26
      },
      'Conversion 7 days': {
        'tag1': 28.857864,
        'tag2': -2.5
      },
      'Conversion 14 days': {
        'tag1': 33.032139,
        'tag2': -2.27
      },
      'ACoS 1 day': {
        'tag1': 13.789193,
        'tag2': -0.21
      },
      'ACoS 7 days': {
        'tag1': 11.715968,
        'tag2': -0.2
      },
      'ACoS 14 days': {
        'tag1': 31.2,
        'tag2': -3.57
      },
      'Cost per click': {
        'tag1': 1.91,
        'tag2': -2
      },
      'PCOGS': {
        'tag1': 26858664.68,
        'tag2': 23.1
      },
      'Inventory': {
        'tag1': null,
        'tag2': '__vue_devtool_undefined__'
      },
      'Average Selling price': {
        'tag1': 14.03,
        'tag2': 4.39
      },
      'customer_orders': {
        'tag1': 3153442,
        'tag2': 25.32
      },
      'Brand Halo Sales 1 day': {
        'tag1': 511052.46,
        'tag2': 21.08
      },
      'Brand Halo Sales 7 day': {
        'tag1': 641721.09,
        'tag2': 20.82
      },
      'Brand Halo Sales 14 day': {
        'tag1': 731398.63,
        'tag2': 20.22
      }
    },
    'metricsUnits': {
      'Spend': {
        'invertTag2': false,
        'tag1Unit': {
          'pre': 'currency'
        },
        'tag2Unit': {
          'suff': '%'
        }
      },
      'auto_cubesdk_count': {
        'invertTag2': false,
        'tag1Unit': '__vue_devtool_undefined__',
        'tag2Unit': {
          'suff': '%'
        }
      },
      'Total sales': {
        'invertTag2': false,
        'tag1Unit': {
          'pre': 'currency'
        },
        'tag2Unit': {
          'suff': '%'
        }
      },
      'Organic sales': {
        'invertTag2': false,
        'tag1Unit': {
          'pre': 'currency'
        },
        'tag2Unit': {
          'suff': '%'
        }
      },
      'Impressions': {
        'invertTag2': false,
        'tag1Unit': null,
        'tag2Unit': {
          'suff': '%'
        }
      },
      'Clicks': {
        'invertTag2': false,
        'tag1Unit': null,
        'tag2Unit': {
          'suff': '%'
        }
      },
      'Units 1 day': {
        'invertTag2': false,
        'tag1Unit': null,
        'tag2Unit': {
          'suff': '%'
        }
      },
      'Units 7 days': {
        'invertTag2': false,
        'tag1Unit': null,
        'tag2Unit': {
          'suff': '%'
        }
      },
      'Units 14 days': {
        'invertTag2': false,
        'tag1Unit': null,
        'tag2Unit': {
          'suff': '%'
        }
      },
      'Orders 1 day': {
        'invertTag2': false,
        'tag1Unit': null,
        'tag2Unit': {
          'suff': '%'
        }
      },
      'Orders 7 days': {
        'invertTag2': false,
        'tag1Unit': null,
        'tag2Unit': {
          'suff': '%'
        }
      },
      'Orders 14 days': {
        'invertTag2': false,
        'tag1Unit': null,
        'tag2Unit': {
          'suff': '%'
        }
      },
      'Paid sales 1 day': {
        'invertTag2': false,
        'tag1Unit': {
          'pre': 'currency'
        },
        'tag2Unit': {
          'suff': '%'
        }
      },
      'Incremental ACoS': {
        'invertTag2': false,
        'tag1Unit': {
          'pre': 'currency'
        },
        'tag2Unit': {
          'suff': '%'
        }
      },
      'Paid sales 14 days': {
        'invertTag2': false,
        'tag1Unit': {
          'pre': 'currency'
        },
        'tag2Unit': {
          'suff': '%'
        }
      },
      'Click through rate': {
        'invertTag2': false,
        'tag1Unit': {
          'suff': '%'
        },
        'tag2Unit': {
          'suff': '%'
        }
      },
      'Conversion 1 day': {
        'invertTag2': false,
        'tag1Unit': {
          'suff': '%'
        },
        'tag2Unit': {
          'suff': '%'
        }
      },
      'Conversion 7 days': {
        'invertTag2': false,
        'tag1Unit': {
          'suff': '%'
        },
        'tag2Unit': {
          'suff': '%'
        }
      },
      'Conversion 14 days': {
        'invertTag2': false,
        'tag1Unit': {
          'suff': '%'
        },
        'tag2Unit': {
          'suff': '%'
        }
      },
      'ACoS 1 day': {
        'invertTag2': true,
        'tag1Unit': {
          'suff': '%'
        },
        'tag2Unit': {
          'suff': '%'
        }
      },
      'ACoS 7 days': {
        'invertTag2': true,
        'tag1Unit': {
          'suff': '%'
        },
        'tag2Unit': {
          'suff': '%'
        }
      },
      'ACoS 14 days': {
        'invertTag2': true,
        'tag1Unit': {
          'suff': '%'
        },
        'tag2Unit': {
          'suff': '%'
        }
      },
      'Cost per click': {
        'invertTag2': true,
        'tag1Unit': {
          'pre': 'currency'
        },
        'tag2Unit': {
          'suff': '%'
        }
      },
      'PCOGS': {
        'invertTag2': false,
        'tag1Unit': {
          'pre': 'currency'
        },
        'tag2Unit': {
          'suff': '%'
        }
      },
      'Inventory': {
        'invertTag2': false,
        'tag1Unit': null,
        'tag2Unit': {
          'suff': '%'
        }
      },
      'Average Selling price': {
        'invertTag2': false,
        'tag1Unit': {
          'pre': 'currency'
        },
        'tag2Unit': {
          'suff': '%'
        }
      },
      'customer_orders': {
        'invertTag2': false,
        'tag1Unit': '__vue_devtool_undefined__',
        'tag2Unit': {
          'suff': '%'
        }
      },
      'Brand Halo Sales 1 day': {
        'invertTag2': false,
        'tag1Unit': {
          'pre': 'currency'
        },
        'tag2Unit': {
          'suff': '%'
        }
      },
      'Brand Halo Sales 7 day': {
        'invertTag2': false,
        'tag1Unit': {
          'pre': 'currency'
        },
        'tag2Unit': {
          'suff': '%'
        }
      },
      'Brand Halo Sales 14 day': {
        'invertTag2': false,
        'tag1Unit': {
          'pre': 'currency'
        },
        'tag2Unit': {
          'suff': '%'
        }
      }
    },
    'page': '__vue_devtool_undefined__',
    'load': false,
    'error': false
  }
}
