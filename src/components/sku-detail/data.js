export default {
  demoData: {
    skuInfoData: {
      price: 57.99,
      star: 4.5,
      name: 'Super Clean Daily Surface Cleaner for the Bathroom, 23 ounce Spray (Pack of 2), Kills 99.9% of Bacteria, Viruses, Germs, Mold, and Fungus',
      asin: 'B009B87PBY',
      imgUrl: 'https://images-na.ssl-images-amazon.com/images/I/717IIqmyZtL._AC_SL1500_.jpg',
      events: [
        {
          labelText: 'In-stock',
          impact: 'POSITIVE'
        },
        {
          labelText: 'Lost Buy Box',
          impact: 'NEGATIVE'
        }
      ],
      infoList: [
        {
          labelText: 'Units in hand',
          value: 631
        },
        {
          labelText: 'Weeks of coverage',
          value: 3
        },
        {
          labelText: 'Price elasticity',
          value: 0.85
        },
        {
          labelText: 'Rep Code',
          value: 'In-stock'
        },
        {
          labelText: 'Revenue rank',
          numerator: 15,
          denominator: 50
        }
      ],
      badges: [
        'amazon_choice',
        'prime',
        's&s'
      ]
    }
  }
}
