import Button from './components/button/Button.vue'
import Icon from './components/icon/Icon.vue'
import SVPDashBoard from './components/SVP-dashboard/SVP-dashboard.vue'

// export const button = Button
// export const icon = Icon

export {
  Button,
  Icon,
  SVPDashBoard
}
