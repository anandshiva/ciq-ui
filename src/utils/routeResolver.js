export default {
  routes: [
    'brand',
    'category'
  ],
  check (value) {
    return this.routes.indexOf(value) > -1
  },
  getAsRouteParams (text, type) {
    var _returnObject = {}
    for (var i = 0; i < this.routes.length; i++) {
      if (type) {
        _returnObject[type] = (this.check(type) ? text.toLowerCase() : undefined)
      }
    }

    return _returnObject
  },
  getPageIDs (object) {
    var _returnObject = {}
    for (var type in object) {
      if (object[type] && type !== 'edit') {
        _returnObject['type'] = this.check(type) ? type.toLowerCase() : undefined
        _returnObject['title'] = this.check(type) ? (object[type] ? object[type].toLowerCase() : undefined) : undefined
      }
    }

    if (Object.keys(_returnObject).length === 0) {
      _returnObject = {
        type: '',
        title: 'Overall'
      }
    }

    return _returnObject
  }
}
