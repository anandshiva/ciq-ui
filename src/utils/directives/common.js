import router from '@/router/index'
import jQuery from 'jquery'
var clickToExpandListenerPages = ['recommendations', 'data', 'insights']

router.beforeEach((to, from, next) => {
  document.removeEventListener('click', clickToExpandListener)
  if (clickToExpandListenerPages.indexOf(to.path.split('/')[1]) !== -1) {
    document.addEventListener('click', clickToExpandListener)
  } else {
    document.removeEventListener('click', clickToExpandListener)
  }

  next()
})

var clickToExpandListener = function (event) {
  var previouslyOpened = document.getElementsByClassName('clickToExpandParent')
  for (var i = 0; i < previouslyOpened.length; i++) {
    previouslyOpened[i].classList = previouslyOpened[i].classList.value.replace('clickToExpandParent', '')
  }
}

export default {
  'click-outside': {
    bind: function (el, binding, vnode) {
      el.event = function (event) {
        if (!(el === event.target || el.contains(event.target))) {
          vnode.context[binding.expression](event)
        }
      }
      document.body.addEventListener('click', el.event)
    },
    unbind: function (el) {
      document.body.removeEventListener('click', el.event)
    }
  },
  'trend-indicator': {
    bind: function (el, binding, vnode) {
      el.classList = (el.classList.value || '') + ' u-display-inline-flex u-flex-align-items-center'
      var value = parseFloat(el.textContent.trim())
      if (!isNaN(value)) {
        var trendIcon = 'icon-arrow-up'
        if (value < 0) {
          trendIcon = 'icon-arrow-down'
          el.textContent = el.textContent.replace('-', '')
        }
        var icon = document.createElement('span')
        icon.classList = 'rb-icon rb-icon--small u-spacing-mr-xs ' + trendIcon
        el.insertBefore(icon, el.firstChild)
      }
    }
  },
  'click-to-expand': {
    bind: function (el) {
      el.onclick = function (event) {
        var totalLength = jQuery(el).closest('tr').parent().children().length
        var previouslyOpened = document.getElementsByClassName('clickToExpandParent')
        for (var i = 0; i < previouslyOpened.length; i++) {
          previouslyOpened[i].classList = previouslyOpened[i].classList.value.replace('clickToExpandParent', '')
        }

        var expand = false
        if (el.clientWidth < el.scrollWidth) {
          expand = true
        } else if (el.children && el.children.length > 0) {
          const children = el.querySelectorAll('*')
          for (const child of children) {
            if (child.clientWidth < el.scrollWidth || child.clientWidth < child.scrollWidth) {
              expand = true
              break
            }
          }
        }

        if (expand) {
          this.classList = this.classList.value.replace(' clickToExpandItem', '')
          this.classList = (this.classList.value || '') + ' clickToExpandItem'
          el.parentElement.parentElement.classList = (el.parentElement.parentElement.classList.value || '') + ' clickToExpandParent'
          if (totalLength > 5 && (jQuery(el).closest('tr').is(':last-child') || jQuery(el).closest('tr').is(':nth-last-child(2)') || jQuery(el).closest('tr').is(':nth-last-child(3)'))) {
            jQuery(el).closest('.cell').css('bottom', '5px')
            jQuery(el).closest('.cell').css('top', 'auto')
          } else {
            jQuery(el).closest('.cell').css('top', '5px')
            jQuery(el).closest('.cell').css('bottom', 'auto')
          }
        }
        event.stopImmediatePropagation()
      }
    }
  }
}
