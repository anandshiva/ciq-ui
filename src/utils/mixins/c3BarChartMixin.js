export default {
  mounted () {
    this.chartConfig.data.columns = this.chartData
    this.chartConfig.bindto = this.$el
  },
  data () {
    return {
      chartConfig: {
        color: {
          pattern: [
            'rgba(44,127,232,1)',
            'rgba(134,221,178,1)',
            'rgba(232,200,0,1)',
            'rgba(243,156,92,1)',
            'rgba(240,96,96,1)',
            'rgba(92,75,126,1)',
            'rgba(0,181,136,1)',
            'rgba(242,235,191,1)',
            'rgba(243,181,98,1)',
            'rgba(240,96,96,1)',
            'rgba(92,75,81,1)'
          ]
        },
        data: {
          x: 'x',
          type: 'bar',
          columns: []
        },
        axis: {
          x: {
            type: 'category'
          }
        }
      }
    }
  }
}
