import Vue from 'vue'
import chartSvg from '@/utils/helpers/chartSvg'
function isFunction (functionToCheck) {
  return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]'
}

export default {
  props: {
    config: Object,
    data: [Object, Array, String]
  },
  created () {
    this.config.xAxisType = this.config.xAxisType || 'category'
    this.config.stack = this.config.stack || []
    this.config.chartGetter = this.config.chartGetter || ''
    this.chartConfig.data.type = this.config.chartOptions.type || this.config.type || 'line'
    if (this.config.chartOptions.types) {
      this.chartConfig.data.types = this.config.chartOptions.types
    }

    this.chartConfig.axis.x.type = this.config.xAxisType
    if (this.config.axis) {
      this.chartConfig.axis.rotated = this.config.axis.rotated || false
    }

    if (this.chartConfig.data.type === 'donut') {
      this.chartConfig.donut = this.config.donut || undefined
    }

    if (this.chartConfig.data.type === 'bar') {
      // this.chartConfig.data.groups = this.config.groups;
      this.chartConfig.data.x = 'x'
      if (this.config.bar !== undefined && this.config.bar.width !== undefined && this.config.bar.width.ratio !== undefined) {
        this.chartConfig.bar = this.config.bar
      }
      this.chartConfig.data.x = 'x'
      this.chartConfig.axis.x = {
        show: true,
        type: 'category'
      }
      if (this.chartConfig.xDataKey !== undefined) {
        this.chartConfig.data.x = this.chartConfig.xDataKey
      }
    }

    if (this.config.chartOptions && this.config.chartOptions.timeseries !== undefined) {
      // -----------------------------------------------------------------------------------------
      //  /_\  Please inform Subhash about changes in this part of code so that recommendations
      //       screens can be adapted.
      // -----------------------------------------------------------------------------------------
      if (typeof this.config.chartOptions.timeseries === 'object') {
        this.chartConfig.data.xs = this.config.chartOptions.timeseries
      } else {
        this.chartConfig.data.x = this.config.chartOptions.timeseries
      }
      this.chartConfig.axis.x = {
        show: true,
        type: 'timeseries',
        tick: {
          format: '%e %b',
          fit: true,
          outer: false,
          count: this.config.chartOptions.chartWidth === 's' ? 5 : undefined
        }
      }
      if (this.config.chartOptions.axis_format && this.config.chartOptions.axis_format.x) {
        this.chartConfig.axis.x.tick.format = this.config.chartOptions.axis_format.x
      }
      this.chartConfig.axis.y = {
        show: false,
        outer: false,
        tick: {
          format: function (a) { return Vue.prototype.$currency + a.toFixed(2) }
        }
      }
      if (this.config.chartOptions.axis_format && this.config.chartOptions.axis_format.y) {
        this.chartConfig.axis.y.show = true
        this.chartConfig.axis.y.tick.format = (value) => {
          return Vue.options.filters.num_format(value, this.config.chartOptions.axis_format.y.pre, this.config.chartOptions.axis_format.y.suff, this.config.chartOptions.axis_format.y.min, this.config.chartOptions.axis_format.y.roundoff)
        }
        if (this.config.chartOptions.axis_format.y.inverted) {
          this.chartConfig.axis.y.inverted = true
        }
        if (this.config.chartOptions.axis_format.y.min) {
          this.chartConfig.axis.y.min = this.config.chartOptions.axis_format.y.min
        }
      }
      this.chartConfig.axis.y2 = {
        show: false,
        tick: {
          outer: false,
          format: function (a) { return Vue.prototype.$currency + a.toFixed(2) }
        }
      }
      if (this.config.chartOptions.axis_format && this.config.chartOptions.axis_format.y2) {
        this.chartConfig.axis.y2.show = true
        this.chartConfig.axis.y2.tick.format = (value) => {
          return Vue.options.filters.num_format(value, this.config.chartOptions.axis_format.y2.pre, this.config.chartOptions.axis_format.y2.suff, this.config.chartOptions.axis_format.y2.min, this.config.chartOptions.axis_format.y2.roundoff)
        }
        if (this.config.chartOptions.axis_format.y2.inverted) {
          this.chartConfig.axis.y2.inverted = true
        }
        if (this.config.chartOptions.axis_format.y2.min) {
          this.chartConfig.axis.y2.min = this.config.chartOptions.axis_format.y2.min
        }
      }
      this.chartConfig.grid = {
        y: {
          show: false
        },
        x: {
          show: false
        }
      }
      if (this.config.chartOptions.grid && this.config.chartOptions.grid.constructor === String) {
        if (this.config.chartOptions.grid.indexOf('x') !== -1) {
          this.chartConfig.grid.x.show = true
        }
        if (this.config.chartOptions.grid.indexOf('y') !== -1) {
          this.chartConfig.grid.y.show = true
        }
      } else if (this.config.chartOptions.grid && this.config.chartOptions.grid.constructor === Object) {
        this.chartConfig.grid = this.config.chartOptions.grid
      }
    } else if (this.config.xAxisType === 'category') {
      this.chartConfig.axis.x.categories = []
    }

    if (this.config.chartOptions.padding) {
      for (var i in this.config.chartOptions.padding) {
        this.chartConfig.padding[i] = this.config.chartOptions.padding[i]
      }
    }

    if (this.config.chartOptions.axes) {
      this.chartConfig.data.axes = this.config.chartOptions.axes
      if (this.config.chartOptions.hideY2 === true) {
        this.chartConfig.axis.y2 = {
          show: false
        }
      }
    }

    if (this.config.chartOptions.xFormat) {
      this.chartConfig.data.xFormat = this.config.chartOptions.xFormat
    }

    if (this.config.chartOptions.tooltip) {
      this.chartConfig.tooltip = this.config.chartOptions.tooltip
    }

    if (this.config.chartOptions.regions) {
      this.chartConfig.data.regions = this.config.chartOptions.regions
    }
    if (this.config.chartOptions.events) {
      this.chartConfig.events = this.config.chartOptions.events
    }
    if (this.config.regions) {
      this.chartConfig.regions = this.config.regions
    }
  },
  mounted () {
    this.chartConfig.data.columns = this.data || []
    this.chartConfig.bindto = this.$el
    if (this.chartConfig.grid !== undefined && (this.chartConfig.grid.x.show || this.chartConfig.grid.y.show)) {
      this.$el.classList.add('graphWithGrids')
    }
  },
  data () {
    var chartWidget = this
    var chartOptions = this.config.chartOptions
    let localChartSvg = chartSvg
    return {
      customLegends: null,
      chartInstance: null,
      chartConfig: {
        line: {
          connectNull: true
        },
        axis: {
          x: {
            show: false
          },
          y: {
            show: false
          },
          y2: {
            min: 0,
            show: false
          }
        },
        point: {
          r: function (data) {
            var pointFormat = (((this.config.chartOptions || {}).point_format || {})[data.id] || {})
            if ((this.config.chartOptions.events || []).length > 0) {
              let eventsKey = this.config.chartOptions.events.findIndex((item) => {
                return item.key === data.id
              })
              if (eventsKey !== -1) {
                return 8
              }
            }
            if (isFunction(pointFormat)) {
              return pointFormat(data)
            }
            if ((data.id.toLowerCase().indexOf('promo') > -1 || data.id.toLowerCase().indexOf('timeline') > -1) && data.id.toLowerCase().indexOf('promotions') === -1) {
              return 5
            }
            return 3
          }.bind(this)
        },
        data: {
          columns: []
        },
        color: {
          pattern: ['#ffa800', '#bd10e0', '#ff6072', '#97cc04', '#23b5d3', '#f5d908', '#ff909d', '#ffc24c', '#d158ea', '#f8e552', '#b6dc4f', '#65cce1']
        },
        oninit: function () {
          var legendItems = []
          this.chartOptions = chartOptions
          this.localChartSvg = localChartSvg
          var element = this.config.bindto
          if ((this.chartOptions.events || []).length > 0) {
            let svgElement = this.d3.select(element).select('svg')
            for (let i = 0; i < this.chartOptions.events.length; i++) {
              let event = this.chartOptions.events[i]
              let url = this.localChartSvg[event.icon].src
              svgElement.append('filter')
                .attr('id', event.key)
                .attr('width', '100%')
                .attr('height', '100%')
                .append('feImage')
                .attr('xlink:href', url)
            }
          }
          if (this.data.targets && this.data.targets.length > 0 && chartOptions.legend !== false) {
            for (var i = 0; i < this.data.targets.length; i++) {
              legendItems.push(this.data.targets[i]['id'])
            }
            var that = this
            this.d3.select(element).insert('div', '.chart').attr('class', 'u-display-flex u-flex-justify-content-center u-spacing-mb-m').selectAll('span')
              .data(legendItems)
              .enter().append('span')
              .attr('data-id', function (id) { return id })
              .attr('class', 'u-display-flex u-flex-align-items-center')
              .html(function (id) { return '<span class="legendText u-spacing-ml-m u-spacing-mr-xs u-font-size-6 u-cursor-pointer"><span class="u-spacing-mr-xs" style="border-radius:100%; display:inline-block; width: 8px; height:8px; background:' + that.color(id) + '"></span><span>' + id + '</span></span>' + ((chartWidget.customLegends && chartWidget.customLegends[id] ? '<a target="_blank" href="' + chartWidget.customLegends[id] + '"><span class="rb-icon icon-open-new-window rb-icon--x-small u-color-grey-x-light"></span></a>' : '')) })
              .select('.legendText')
              .on('mouseover', function (id) {
                that.api.focus(id)
              })
              .on('mouseout', function (id) {
                that.api.revert()
              })
              .on('click', function (id) {
                var opacity = this.style.opacity
                if (opacity === '1' || !opacity) {
                  this.style.opacity = '0.3'
                } else {
                  this.style.opacity = '1'
                }
                that.api.toggle(id)
              })

            this.d3.select(element)
              .selectAll('.legendText')
              .attr('data-id', function (id) {
                if (that.chartOptions && that.chartOptions.disableLegends && that.chartOptions.disableLegends.length > 0) {
                  if (that.chartOptions.disableLegends.indexOf(id) !== -1) {
                    setTimeout(function () {
                      this.click()
                    }.bind(this), 100)
                  }
                }
                return id
              })
          }
        },
        onrendered: function () {
          var $$ = this
          if (this.data.targets && this.data.targets.length > 1 && chartOptions.show_axis_colors) {
            var element = this.config.bindto
            this.data.targets.forEach((item) => {
              let axisToColor = $$.config.data_axes[item.id]
              let classToSelect = axisToColor === 'y2' ? '.c3-axis-y2 .tick' : '.c3-axis-y .tick'
              let color = $$.config.data_axes[item.id + '_color']
              $$.d3.select(element)
                .selectAll(classToSelect).style('fill', color)
            })
          }

          var circles = $$.getCircles()
          let singleCircleMap = {}
          if ((this.chartOptions.events || []).length > 0) {
            for (let i = 0; i < circles._groups[0].length; i++) {
              let singleCircle = circles._groups[0][i]
              if (singleCircle.__data__.value == null) {
                continue
              }
              let eventsKey = this.chartOptions.events.findIndex((item) => {
                return item.key === singleCircle.__data__.id
              })
              if (eventsKey !== -1) {
                if (!singleCircleMap[singleCircle.__data__.x]) {
                  singleCircleMap[singleCircle.__data__.x] = []
                }
                singleCircleMap[singleCircle.__data__.x].push(singleCircle)
                singleCircle.setAttribute('filter', `url(#${this.chartOptions.events[eventsKey].key})`)
                let classString = singleCircle.getAttribute('class') + ' u-opacity-1'
                singleCircle.setAttribute('class', classString)
              }
            }
          }
          let shiftBy = 8
          for (let key in singleCircleMap) {
            let dayArray = (singleCircleMap[key] || [])
            if (dayArray.length > 1) {
              for (let i = 0; i < dayArray.length; i++) {
                let currentDayPoint = dayArray[i]
                let currentCy = parseFloat(currentDayPoint.getAttribute('cy'))
                let currentCx = parseFloat(currentDayPoint.getAttribute('cx'))
                let newCx = currentCx
                let newCy = currentCy
                switch (i) {
                  case 0:
                    newCx = currentCx + shiftBy
                    break
                  case 1:
                    newCx = currentCx - shiftBy
                    break
                  case 2:
                    newCy = currentCy - shiftBy
                }
                currentDayPoint.setAttribute('cy', newCy)
                currentDayPoint.setAttribute('cx', newCx)
              }
            }
          }
          for (var i = 0; i < circles.length; i++) {
            for (var j = 0; j < circles[i].length; j++) {
              $$.getCircles(j).style('fill', '#FFF').style('stroke', $$.color).style('stroke-width', 0.75)
            }
          }
        },
        legend: {
          show: false
        },
        tooltip: {
          contents: function (d, defaultTitleFormat, defaultValueFormat, color) {
            var $$ = this
            var config = $$.config
            var titleFormat = config.tooltip_format_title || defaultTitleFormat
            var nameFormat = config.tooltip_format_name || function (name) { return name }
            var valueFormat = config.tooltip_format_value || defaultValueFormat
            var text = ''
            var i = 0
            var title = ''
            var value = ''
            var name = ''
            var bgcolor = ''
            var meta = this.config.data_classes
            let eventCounts = 0
            let eventText = `
              <div class="u-spacing-pt-s u-spacing-pb-m u-spacing-pl-m u-border-top u-border-width-s u-border-color-grey-xxx-light u-display-flex">
                <span class="u-font-size-5 u-color-grey-light u-font-weight-600">Events</span>
               </div>`

            for (i = 0; i < d.length; i++) {
              if (!(d[i] && (d[i].value || d[i].value === 0))) { continue }
              let eventIndex = (this.chartOptions.events || []).findIndex((item) => {
                return item.key === d[i].id
              })
              var nameFromData = d[i].name
              var indexFromData = d[i].index
              if (!text) {
                title = titleFormat ? titleFormat(d[i].x) : d[i].x
                if (d[i].x && d[i].x.constructor.name === 'Date') {
                  title = Vue.options.filters.printable_date(new Date(d[i].x))
                }
                text = `<div class="card u-spacing-pb-s" style="background:rgba(255, 255, 255, 0.98); box-shadow: 0 0 4px 0 #caccce !important;">
                          <div class="u-spacing-p-m u-border-bottom u-border-width-s u-border-color-grey-xxx-light u-display-flex u-spacing-mb-s">
                            <span class="u-font-size-4 u-color-grey-light u-font-weight-600">${title}</span>
                          </div>
                          <div class="u-spacing-pl-m u-spacing-pt-s u-spacing-pb-m u-spacing-pr-s u-display-flex">
                            <span class="u-font-size-5 u-color-grey-light u-font-weight-600">Metrics</span>
                          </div>`
              }
              if (Object.keys(meta).length > 0 && meta[d[i].name] && this.chartOptions.tooltip_mapper[nameFromData] && meta[nameFromData] && meta[nameFromData][indexFromData] && meta[nameFromData][indexFromData][this.chartOptions.tooltip_mapper[nameFromData]]) {
                name = meta[nameFromData][indexFromData][this.chartOptions.tooltip_mapper[nameFromData]]
              } else {
                name = d[i].name
              }
              name = nameFormat(name)
              value = valueFormat(d[i].value, d[i].ratio, d[i].id, d[i].index)
              bgcolor = $$.levelColor ? $$.levelColor(d[i].value) : color(d[i].id)
              if (eventIndex !== -1) {
                let url = this.localChartSvg[d[i].id].src
                eventText += `<div style="min-width: 240px; max-width: 500px;" class="u-display-flex u-flex-justify-content-space-between u-flex-align-items-flex-start u-font-size-5 u-color-grey-light u-spacing-pb-s u-spacing-ph-m">
                            <span class="u-display-flex u-flex-align-items-center" style="min-width:136px">
                              <div class="u-spacing-mr-s" style="border-radius:100%; display:inline-block; width: 12px; height:12px;">
                                <img src="${url}" class="u-display-flex u-height-100 u-width-100" />
                              </div>
                              <span class="u-spacing-mr-s">${name}</span>
                            </span>
                          </div>`
                eventCounts++
              } else {
                text += `<div style="min-width: 240px; max-width: 500px;" class="u-display-flex u-flex-justify-content-space-between u-flex-align-items-flex-start u-font-size-5 u-color-grey-light u-spacing-pb-s u-spacing-ph-m">
                <span class="u-display-flex u-flex-align-items-center" style="min-width:136px">
                  <span class="u-spacing-mr-s" style="border-radius:100%; display:inline-block; width: 8px; height:8px; background:${bgcolor}"></span>
                  <span class="u-spacing-mr-s">${name}</span>
                </span>
                <span  class="u-font-weight-600 u-line-height-1-3">${value}</span>
              </div>`
              }
            }
            if (eventCounts > 0) {
              text += `<div class="u-spacing-pb-s"></div>`
              return text + eventText + '</div>'
            } else {
              return text + '</div>'
            }
          },
          format: {
            value: function (value, ratio, id, index) {
              var tooltipFormat = (((this.config.chartOptions || {}).tooltip_format || {})[id] || {})
              if (isFunction(tooltipFormat)) {
                return tooltipFormat(value, ratio, id, index)
              }
              if (Object.keys(tooltipFormat).length === 0) {
                tooltipFormat = (((this.config.chartOptions || {}).tooltip_format || {})['All'] || {})
              }
              if (tooltipFormat.format !== false) {
                return Vue.options.filters.num_format(value, tooltipFormat.pre, tooltipFormat.suff, tooltipFormat.min, tooltipFormat.roundoff)
              } else {
                return value
              }
            }.bind(this)
          }
        }
      }
    }
  }
}
