export default {
  props: {
    category: {
      type: String,
      default: ''
    },
    brand: {
      type: String,
      default: ''
    },
    id: {
      type: String,
      default: ''
    },
    alertId: {
      type: String,
      default: ''
    },
    edit: {
      type: [String, Boolean],
      default: false
    }
  }
}
