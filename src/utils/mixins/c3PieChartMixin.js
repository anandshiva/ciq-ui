export default {
  axis: {
    x: {
      type: 'timeseries',
      tick: {
        format: '%m-%d'
      }
    }
  }
}
