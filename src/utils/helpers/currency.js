export default {
  install (Vue, options) {
    if (options) {
      Vue.prototype.$currency = options
    } else {
      Vue.prototype.$currency = '$'
    }
  }
}
