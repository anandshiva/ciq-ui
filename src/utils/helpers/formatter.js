import Vue from 'vue'
import moment from 'moment'

function getValueAlphabets (value) {
  var sign = ''
  if (value < 0) {
    sign = '-'
    value = value * -1
  }
  var q = ''
  if (value > 1000000000) {
    q = 'B'
    value = value / 1000000000
  } else if (value > 1000000) {
    q = 'M'
    value = value / 1000000
  } else if (value > 1000) {
    q = 'K'
    value = value / 1000
  }
  if (!Number.isInteger(value)) {
    value = value.toFixed(2)
  }
  return {
    sign: sign,
    value: value,
    notation: q
  }
}

export function formatter (value, format) {
  if (!format) {
    format = 'string'
  }
  if (value === undefined || value === null) {
    return 'NA'
  }
  if (format.toLowerCase() === 'string') {
    return value
  } else if (format.toLowerCase() === 'currency') {
    var obj = getValueAlphabets(value)
    return obj.sign + Vue.prototype.$currency + obj.value + obj.notation
  } else if (
    format.toLowerCase() === 'percent' ||
    format.toLowerCase() === 'percentage'
  ) {
    return value === 0 ? '0%' : value.toFixed(2) + '%'
  } else if (format.toLowerCase() === 'date') {
    // return new Date(value)
    //   .toLocaleString('en-US', { timeZone: 'UTC' })
    //   .split(',')[0];
    // Format Change for KC-UK
    return moment(value).format('MMM DD, YYYY')
  } else if (format === 'percentFraction') {
    return value + '/100'
  } else if (format.toLowerCase() === 'numeric') {
    obj = getValueAlphabets(value)
    return obj.sign + obj.value + obj.notation
  } else if (format.toLowerCase() === 'numberic_x') {
    return value + 'X'
  } else {
    return value
  }
}
