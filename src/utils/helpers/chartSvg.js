let base = '/static/chartIcons/'
export default {
  '3P-variant': {
    src: base + '3P-variant.svg'
  },
  'add-on': {
    src: base + 'add-on.svg'
  },
  'amazon-choice': {
    src: base + 'amazon-choice.svg'
  },
  'best-seller': {
    src: base + 'best-seller.svg'
  },
  'campaign': {
    src: base + 'campaign.svg'
  },
  'content-change': {
    src: base + 'content-change.svg'
  },
  'OOS': {
    src: base + 'OOS.svg'
  },
  'search-event': {
    src: base + 'search-event.svg'
  },
  'SnS': {
    src: base + 'SnS.svg'
  },
  'suppressed': {
    src: base + 'suppressed.svg'
  },
  'unavailable': {
    src: base + 'unavailable.svg'
  }
}
