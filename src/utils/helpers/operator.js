const operator = {
  EQUAL_TO: '=',
  GREATER_THAN: '>',
  GREATER_THAN_OR_EQUAL_TO: '> =',
  LESS_THAN: '<',
  LESS_THAN_OR_EQUAL_TO: '< =',
  NOT_EQUAL_TO: '! =',
  ILIKE: 'Has',
  BETWEEN: '> & <'
}
const operatorTitle = {
  '=': 'Equals',
  '>': 'Greater than',
  '> =': 'Greater than equal to',
  '<': 'Less than',
  '< =': 'Less than equal to',
  '! =': 'Not equal to',
  '> & <': 'Between'
}

export { operator, operatorTitle }
