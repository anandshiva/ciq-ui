export default {
  nativeExtend () {
    var that = this
    for (var i = 1; i < arguments.length; i++) {
      for (var key in arguments[i]) {
        if (arguments[i].hasOwnProperty(key)) {
          if (typeof arguments[0][key] === 'object' && typeof arguments[i][key] === 'object') {
            that.nativeExtend(arguments[0][key], arguments[i][key])
          } else {
            arguments[0][key] = arguments[i][key]
          }
        }
      }
    }
    return arguments[0]
  },
  internalUserCheck (userObject) {
    var toRet = false
    const email = userObject.email
    var internalDomains = window.configs.common.users.internalDomains.split(',')
    if (email && (internalDomains.indexOf(email.split('@')[1]) > -1)) {
      toRet = true
    }
    return toRet
  },
  setMinWidth (item) {
    var keyTypesObj = { string: 200, boolean: 100, recommended_action_type: 250, asin: 120, bsr_link: 200, link: 200 }
    if (item && item.keyType && keyTypesObj.hasOwnProperty(item.keyType.toLowerCase())) {
      return keyTypesObj[item.keyType]
    } else {
      return 120
    }
  }
}
