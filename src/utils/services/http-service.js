import axios from 'axios'
import UrlFactory from '@/utils/services/url-factory'
// import { store } from '@/store/store'

var getUrl = function (id, config) {
  // automatically replaces :param in a url like a/:param/b/;param
  var url = UrlFactory.getFinalURL(id)
  if (config && config.append) {
    url = url + config.append
  }
  if (!config || !config.pathParams) return url
  var paramList = Object.keys(config.pathParams)
  for (var param of paramList) {
    var regex1 = '/:' + param + '/'
    var regex2 = '/:' + param + '$'
    url = url.replace(new RegExp(regex1, 'g'), '/' + config.pathParams[param] + '/').replace(new RegExp(regex2, 'g'), '/' + config.pathParams[param])
  }
  return url
}

axios.interceptors.response.use(function (response) {
  return response
}, function (err) {
  // if (err.response.status === 401 && store.getters.getSessionValidity) {
  if (err.response.status === 401) {
    // store.commit('LOGOUT', 'returnslink=' + encodeURIComponent(window.location));
    return Promise.reject(err)
  } else {
    return Promise.reject(err)
  }
})

export default {
  all: axios.all,
  get (id, config) {
    return axios.get(getUrl(id, config), config)
  },
  post (id, data, config) {
    return axios.post(getUrl(id, config), data, config)
  },
  put (id, data, config) {
    return axios.put(getUrl(id, config), data, config)
  },
  patch (id, data, config) {
    return axios.patch(getUrl(id, config), data, config)
  }
}
