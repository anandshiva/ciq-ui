import { formatter } from '@/utils/helpers/formatter.js'
import { operator as operators } from '@/utils/helpers/operator.js'

export default {
  getUniqueFilters: (data, prefix, filterMappings) => {
    if (!prefix) {
      prefix = ''
    }

    if (!filterMappings) {
      filterMappings = JSON.parse(localStorage.getItem(prefix.replace('_', '') + '_filters_mapping'))
    }

    var categoryText = 'category'
    var subCategoryText = 'subcategory'
    if (data.length > 0 && data[0].DIMENSION['category'] === undefined) {
      // categoryText = 'l1';
    }
    if (data.length > 0 && data[0].DIMENSION['category'] === undefined && data[0].DIMENSION['l1'] === undefined) {
      categoryText = 'client_category'
    }

    if (data.length > 0 && data[0].DIMENSION['subcategory'] === undefined) {
      // subCategoryText = 'l2';
    }
    if (data.length > 0 && data[0].DIMENSION['subcategory'] === undefined && data[0].DIMENSION['l2'] === undefined) {
      subCategoryText = 'client_subcategory'
    }

    var category = {}
    var subcategory = {}
    var oObj = {}
    var _oReturn = {}
    for (var i = 0; i < data.length; i++) {
      if (data[i].DIMENSION[categoryText] !== undefined && data[i].DIMENSION[subCategoryText] !== undefined) {
        if (!category[data[i].DIMENSION[categoryText]]) {
          category[data[i].DIMENSION[categoryText]] = {
            title: data[i].DIMENSION[categoryText],
            values: []
          }
          subcategory[data[i].DIMENSION[categoryText]] = {}
        }
        if (!subcategory[data[i].DIMENSION[categoryText]][data[i].DIMENSION[subCategoryText]]) {
          subcategory[data[i].DIMENSION[categoryText]][data[i].DIMENSION[subCategoryText]] = true
          if (data[i].DIMENSION[subCategoryText]) {
            category[data[i].DIMENSION[categoryText]].values.push({
              title: data[i].DIMENSION[subCategoryText]
            })
          }
        }
      }

      for (var j in data[i].DIMENSION) {
        if (!oObj[prefix + j]) {
          oObj[prefix + j] = {}
        }

        var found = true

        if (filterMappings && filterMappings[prefix + j]) {
          for (var y in filterMappings[prefix + j]) {
            if (filterMappings[prefix + j][y] && filterMappings[prefix + j][y].length === 0) {
              found = true
              break
            }
            if (filterMappings[prefix + j][y].indexOf(data[i].DIMENSION[y.replace(prefix, '')]) === -1) {
              found = false
              break
            }
          }
        }

        if (found) {
          oObj[prefix + j][data[i].DIMENSION[j]] = true
        } else {
          if (oObj[prefix + j][data[i].DIMENSION[j]] !== true) {
            delete oObj[prefix + j][data[i].DIMENSION[j]]
          }
        }
      }
    }

    for (var k in oObj) {
      if (!_oReturn[k]) {
        _oReturn[k] = []
      }
      for (var l in oObj[k]) {
        if (l && l !== 'null' && l.length > 0) {
          _oReturn[k].push({
            title: l
          })
        }
      }
    }

    if (_oReturn[prefix + categoryText]) {
      var oReturnCategory = []
      for (var t in category) {
        if (oObj[prefix + categoryText][t]) {
          if (category[t].values && category[t].values.length > 0) {
            for (var r = 0; r < category[t].values.length; r++) {
              if (!oObj[prefix + subCategoryText][category[t].values[r].title] || t.length === 0) {
                category[t].values.splice(r, 1)
              }
            }
          }
          if (t !== 'null' && t.length > 0) {
            oReturnCategory.push(category[t])
          }
        }
      }
      _oReturn[prefix + categoryText] = oReturnCategory
      delete _oReturn[prefix + subCategoryText]
    }
    return _oReturn
  },
  getChartDataInFormat: (data, response) => {
    var _aArray = []
    var _oObj = {}
    if (response === null) {
      return []
    }
    for (var i = 0; i < response.length; i++) {
      for (var j in response[i]) {
        var _j = ((data || {}).map || {})[j] || j
        if (!_oObj[_j]) {
          _oObj[_j] = [_j]
        }
        _oObj[_j].push(response[i][j])
      }
    }

    for (var k in _oObj) {
      _aArray.push(_oObj[k])
    }

    return _aArray
  },
  getChartTicksValues: (data, key) => {
    var newArr = []
    if (data && data.length) {
      for (var i = 0; i < data.length; i++) {
        newArr.push(data[i][key])
      }
    }
    return newArr
  },
  mergeResultDimension: (data, addPVP, removeNullProp) => {
    var _aArray = []
    if (data && data.length) {
      for (var i = 0; i < data.length; i++) {
        var oObj = {}
        for (var j in data[i]['RESULT']) {
          oObj[j] = data[i]['RESULT'][j]
        }

        for (j in data[i]['DIMENSION']) {
          oObj[j] = data[i]['DIMENSION'][j]
        }

        if (addPVP) {
          for (j in data[i]['PVP']) {
            oObj['PVP_' + j] = data[i]['PVP'][j]
          }
        }
        if (removeNullProp) {
          if (oObj[removeNullProp] !== undefined) {
            if (oObj[removeNullProp] !== null && oObj[removeNullProp] !== '') {
              _aArray.push(oObj)
            }
          } else {
            _aArray.push(oObj)
          }
        } else {
          _aArray.push(oObj)
        }
      }
    }
    return _aArray
  },
  getCompleteWhereClause: (where, stateSelectedFilters) => {
    var oReturn = {
      dimensionNameValueList: []
    }

    if (where) {
      for (var i = 0; i < where.length; i++) {
        oReturn.dimensionNameValueList.push(where[i])
      }
    }

    if (stateSelectedFilters && stateSelectedFilters.dimensionNameValueList) {
      for (i = 0; i < stateSelectedFilters.dimensionNameValueList.length; i++) {
        if (oReturn.dimensionNameValueList.indexOf(stateSelectedFilters.dimensionNameValueList[i]) === -1) {
          let item = stateSelectedFilters.dimensionNameValueList[i]
          // convert 'BETWEEN' operator to 'GREATER_THAN_OR_EQUAL' and 'LESS_THAN_OR_EQUAL' operators
          if (item.operator && item.operator === 'BETWEEN') {
            let obj1 = {
              operator: 'GREATER_THAN_OR_EQUAL_TO',
              dimensionName: item.dimensionName,
              dimensionValue: Number(item.dimensionValue.split(', ')[0])
            }
            let obj2 = {
              operator: 'LESS_THAN_OR_EQUAL_TO',
              dimensionName: item.dimensionName,
              dimensionValue: Number(item.dimensionValue.split(', ')[1])
            }
            oReturn.dimensionNameValueList.push(obj1, obj2)
          } else {
            oReturn.dimensionNameValueList.push(item)
          }
        }
      }
    }
    return oReturn
  },
  getCellRenderParams (data) {
    var type = data.uiField.uiType.toLowerCase()
    var objToReturn = {
      formatterFn: formatter
    }
    switch (type) {
      case 'percent':
        objToReturn.keyType = 'PERCENTAGE'
        break
      case 'currency':
        objToReturn.keyType = 'CURRENCY'
        break
      case 'numeric':
      // If isFormattingRequired is not passed by the backend we assumne isFormattingRequired to be true
        let formattingRequired = data.uiField.metadata.isFormattingRequired === undefined ? true : data.uiField.metadata.isFormattingRequired
        // If formatting is not required we are removing the formatterFn Field from the object
        if (formattingRequired === false) {
          delete objToReturn.formatterFn
        }
        objToReturn.keyType = 'numeric'
        break
      case 'string':
        objToReturn = {
          keyType: 'string'
        }
        break
      default:
    }
    return objToReturn
  },
  getCustomCellRender (data) {
    var colMetaData = data.uiField.metadata
    var type = colMetaData.widget.toLowerCase()
    var objToReturn = {}
    switch (type) {
      case 'hyperlink':
        objToReturn.cellRendererFramework = 'linkDisplay'
        objToReturn.cellRendererParams = {
          url: colMetaData.urlTableColumnName
        }
        break
      case 'progress':
        objToReturn.cellRendererFramework = 'progressDisplay'
        objToReturn.cellRendererParams = {
          fill: colMetaData.percentTableColumnName,
          decimalRoundOff: colMetaData.decimalRoundOff
        }
        break
      case 'metric':
        objToReturn.cellRendererFramework = 'metricDisplay'
        objToReturn.minWidth = 150
        objToReturn.cellRendererParams = {
          tag1Key: colMetaData.primaryTableColumnName,
          tag2Key: colMetaData.secondaryTableColumnName,
          tag1Unit: this.getTagUnitData(colMetaData.primaryIsPrefix, colMetaData.primaryUnit),
          tag2Unit: this.getTagUnitData(colMetaData.secondaryIsPrefix, colMetaData.secondaryUnit)
        }
        break
      case 'icon':
        objToReturn.cellRendererFramework = 'iconTableCell'
        objToReturn.headerComponentFramework = 'iconTableHeader'
        objToReturn.headerComponentParams = {}
        objToReturn.headerComponentParams.displayIcon = colMetaData.displayIcon
        objToReturn.cellRendererParams = {
          iconSize: !colMetaData.iconSize ? 'medium' : colMetaData.iconSize,
          iconClickEvent: colMetaData.iconClickEvent,
          displayIcon: colMetaData.displayIcon,
          toolTipText: colMetaData.toolTipText,
          toolTipPosition: colMetaData.toolTipPosition,
          contextReturnEvent: colMetaData.contextReturnEvent
        }
        objToReturn.cellRendererParams.type = !colMetaData.type ? 'icon' : colMetaData.type
        if (colMetaData.type === 'iconText') {
          objToReturn.cellRendererParams.formatType = colMetaData.formatType
        }
        break

      case 'input':
        objToReturn.cellRendererFramework = 'inputTypeCell'
        objToReturn.cellRendererParams = {
          type: !colMetaData.type ? 'text' : colMetaData.type,
          blurEvent: colMetaData.blurEvent,
          onchangeEvent: colMetaData.onchangeEvent,
          keyupEvent: colMetaData.keyupEvent,
          defaultValueColumnName: colMetaData.defaultValueColumnName,
          formatType: colMetaData.formatType,
          contextReturnEvent: colMetaData.contextReturnEvent
        }
        break
      default:
    }
    return objToReturn
  },
  getTagUnitData (isPrefix, unit) {
    if (isPrefix) {
      return {
        pre: unit
      }
    } else {
      return {
        suff: unit
      }
    }
  },
  getColumnDefinition (columns, customData, customObject) {
    var colDefinitionToReturn = []
    var columnArray = columns
    for (var i = 0; i < columnArray.length; i++) {
      var currDefinition = columnArray[i]
      var obj = {}
      obj.showOnUi = currDefinition.uiField.metadata.showOnUi
      obj.isDownloadable = currDefinition.uiField.metadata.isDownloadable

      obj.headerComponentFramework = 'genericTableHeader'
      obj.title = currDefinition.uiField.uiLabel
      obj.headerName = currDefinition.uiField.uiLabel
      obj.field = currDefinition.uiField.metadata.tableColumnName === undefined ? currDefinition.name : currDefinition.uiField.metadata.tableColumnName

      obj.cellRendererFramework = 'genericTableCell'
      obj.cellRendererParams = this.getCellRenderParams(currDefinition)

      if (currDefinition.uiField.uiType.toLowerCase() === 'string') {
        if (currDefinition.uiField.metadata.width !== undefined) {
          obj.minWidth = currDefinition.uiField.metadata.width
          obj.width = currDefinition.uiField.metadata.width
        } else {
          obj.minWidth = 180
        }
      } else {
        obj.minWidth = 120
      }
      // obj.minWidth = currDefinition.uiField.uiType.toLowerCase() === 'string' ? 180 : 120;
      if (currDefinition.uiField.uiType === 'custom') {
        obj.minWidth = currDefinition.uiField.metadata.width
        if (customData !== undefined && typeof (customData) === 'object' && Object.keys(customData).length > 0 && customData[currDefinition.name]) {
          obj.cellRendererFramework = customData[currDefinition.name].component
          obj.cellRendererParams = (customData[currDefinition.name] || {}).params || null
          obj.minWidth = 200
        } else {
          if (currDefinition.uiField.metadata.widget === 'progress') {
            obj.field = currDefinition.uiField.metadata.percentTableColumnName
          }
          // if (currDefinition.uiField.metadata.widget === 'icon') {
          // obj.cellRendererFramework = 'iconTableCell';
          // obj.headerComponentFramework = 'iconTableHeader';
          // // obj.cellRendererParams.iconClickEvent = vueRef.openSidePanel;
          // obj.cellRendererParams.displayIcon = 'timeline';
          // // obj.cellRendererParams.toolTipText = dictionary.map[i].toolTipText;
          // obj.notDownloadable = true;
          // obj.headerComponentParams.displayIcon = 'timeline';
          // obj.minWidth = 60;
          // }
          let cellObj = this.getCustomCellRender(currDefinition)
          obj.cellRendererFramework = cellObj.cellRendererFramework
          obj.cellRendererParams = cellObj.cellRendererParams
          if (cellObj.minWidth) {
            obj.minWidth = cellObj.minWidth
          }
          if (customObject !== undefined && customObject.hasOwnProperty(currDefinition.uiField.metadata.tableColumnName)) {
            var columnToRead = currDefinition.uiField.metadata.tableColumnName
            for (let k in customObject[columnToRead]) {
              obj.cellRendererParams[k] = customObject[columnToRead][k]
            }
          }
        }
      }

      obj.keyOrder = currDefinition.uiField.uiOrder
      obj.pinned = currDefinition.uiField.metadata.isFixed
      colDefinitionToReturn[obj.keyOrder] = obj
      obj.headerComponentParams = {
        enableSorting: currDefinition.uiField.metadata.sortOnColumn === undefined ? false : currDefinition.uiField.metadata.sortOnColumn,
        keyType: obj.cellRendererParams.keyType,
        toolTipText: currDefinition.uiField.uiTooltip
      }
      if (currDefinition.uiField.metadata.isDefaultSortColumn) {
        obj.headerComponentParams.sort = currDefinition.uiField.metadata.sortDirection // 'asc'
      }
    }

    var displayColConfigs = colDefinitionToReturn.filter(elm => {
      return elm.showOnUi === true
    })

    var downloadColConfigs = colDefinitionToReturn.filter(elm => {
      return elm.isDownloadable === true
    })
    var objToReturn = {
      displayColConfigs: displayColConfigs,
      downloadColConfigs: downloadColConfigs
    }
    return objToReturn
  },
  getTableDataFromFullResponse: function (apiResponse) {
    var response = apiResponse.fullResponse
    var columns = response.metadata
    var measureList = columns.measureList
    var groupByDimensionList = columns.groupByDimensionList
    measureList = measureList.concat(groupByDimensionList)
    var rows = this.mergeResultDimension(response.data)
    var obj = {
      rows: rows,
      columns: measureList
    }
    return obj
  },
  generateWhereClause (where, filters) {
    let values = filters.values
    for (var key in values) {
      if (values.hasOwnProperty(key)) {
        if (values[key].constructor === Array && key !== 'date_range') { // for filters
          for (let i = 0; i < values[key].length; i++) {
            let temp = values[key]
            if (!where['dimensionNameValueList']) {
              where['dimensionNameValueList'] = []
            }
            if (temp[i].operator) {
              where['dimensionNameValueList'].push({
                'dimensionName': key,
                'dimensionValue': temp[i].value,
                'operator': temp[i].operator.operator
              })
            } else {
              where['dimensionNameValueList'].push({
                'dimensionName': key,
                'dimensionValue': temp[i]
              })
            }
          }
        } else if (key === 'date_range') { // for date range
          where['date'] = {
            from: values[key].from,
            to: values[key].to
          }
        }
      }
    }
    return where
  },
  convertDimensionsToFilterFormat (dimensions) {
    let filter = {
      order: [],
      values: {}
    }
    if (!dimensions) {
      return filter
    }
    console.log(operators)
    for (let i = 0; i < dimensions.length; i++) {
      let { dimensionName } = dimensions[i]
      let orderIndex = filter.order.findIndex((element) => {
        return element === dimensionName
      })
      if (orderIndex === -1) {
        filter.order.push(dimensionName)
      }
      if (!filter.values[dimensionName]) {
        filter.values[dimensionName] = []
        filter.values[dimensionName].push(this.formatToFilterFormat(dimensions[i]))
      } else {
        filter.values[dimensionName].push(this.formatToFilterFormat(dimensions[i]))
      }
    }
    return filter
  },
  formatToFilterFormat (dimension) {
    if (dimension.type === 'EXPRESSION') {
      let obj = {
        operator: {
          operator: dimension.operator,
          title: operators[dimension.operator]
        },
        unit: null,
        value: dimension.dimensionValue
      }
      return obj
    } else {
      return dimension.dimensionValue
    }
  }
}
