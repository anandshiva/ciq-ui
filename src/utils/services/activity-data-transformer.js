import moment from 'moment-timezone'
export default {
  getActivityData: (data) => {
    if (!data || data.length < 0) {
      return data
    }
    let activityData = []
    let entries = []
    let dateBlocks = 0
    data.sort((a, b) => {
      return new Date(b.timeStamp).getTime() - new Date(a.timeStamp).getTime()
    })
    for (let i = 0; i < data.length; i++) {
      let activity = Object.assign({}, data[i])
      let nextActivity = Object.assign({}, data[data.length - 1 === i ? i : i + 1])
      let activityDate = moment(new Date(activity.timeStamp)).format('MM-DD-YYYY')
      let nextActivityDate = moment(new Date(nextActivity.timeStamp)).format('MM-DD-YYYY')
      if (activityDate === nextActivityDate && i !== data.length - 1) {
        entries.push(addEntry(activity))
      } else {
        entries.push(addEntry(activity))
        activity.entries = entries
        activity.date = splitDate(data[i].timeStamp)
        activity.showYear = dateBlocks === 0
        activity.showDate = true
        activity.cssClasses = setClass(dateBlocks)
        entries = []
        dateBlocks++
        activityData.push(activity)
      }
    }
    return activityData
  }
}

let splitDate = (date) => {
  var dateTimeObj = {}
  var calendarMap = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']
  var momentDate = moment(new Date(date)).local()
  let fullDate = new Date(momentDate)
  let zoneName = moment.tz.guess()
  let timezone = moment.tz(zoneName).zoneAbbr()
  dateTimeObj.day = ('0' + fullDate.getDate()).slice(-2)
  dateTimeObj.month = calendarMap[fullDate.getMonth()]
  dateTimeObj.year = fullDate.getFullYear()
  dateTimeObj.time = moment(date).format('hh:mm A') + ' ' + timezone
  dateTimeObj.displayChip = moment(date).format('LL')
  return dateTimeObj
}

let setClass = (index) => {
  if (index === 0) {
    return 'u-border-top u-spacing-pt-xl u-spacing-pb-l u-spacing-mt-xl'
  } else {
    return 'u-spacing-pb-l'
  }
}

let addEntry = (activity) => {
  let entry = Object.assign({}, activity)
  entry.viewPayload.date = splitDate(activity.timeStamp)
  entry.status = getEventStatus(activity)
  entry.viewPayload.email = activity.username
  entry.viewPayload.username = activity.username
  return entry
}

let getEventStatus = (activity) => {
  switch (activity.actionStatus) {
    case 'successful':
      return 'success'
    case 'in progress':
      return 'in-progress'
    case 'error':
      return 'failed'
  }
}
