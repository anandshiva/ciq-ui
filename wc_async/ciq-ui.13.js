(window["ciqUi_jsonp"] = window["ciqUi_jsonp"] || []).push([[13],{

/***/ "5b43":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("a39a");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to Shadow Root
var add = __webpack_require__("35d6").default
module.exports.__inject__ = function (shadowRoot) {
  add("59577240", content, shadowRoot)
};

/***/ }),

/***/ "7832":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_3_node_modules_sass_loader_dist_cjs_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_chart_legends_vue_vue_type_style_index_0_lang_scss_shadow__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("5b43");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_3_node_modules_sass_loader_dist_cjs_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_chart_legends_vue_vue_type_style_index_0_lang_scss_shadow__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_3_node_modules_sass_loader_dist_cjs_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_chart_legends_vue_vue_type_style_index_0_lang_scss_shadow__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_3_node_modules_sass_loader_dist_cjs_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_chart_legends_vue_vue_type_style_index_0_lang_scss_shadow__WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_3_node_modules_sass_loader_dist_cjs_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_chart_legends_vue_vue_type_style_index_0_lang_scss_shadow__WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_3_node_modules_sass_loader_dist_cjs_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_chart_legends_vue_vue_type_style_index_0_lang_scss_shadow__WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "a39a":
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__("24fb");
var ___CSS_LOADER_AT_RULE_IMPORT_0___ = __webpack_require__("5e0d");
exports = ___CSS_LOADER_API_IMPORT___(false);
exports.i(___CSS_LOADER_AT_RULE_IMPORT_0___);
// Module
exports.push([module.i, ".u-font-size-5{font-size:1.4rem}.metric-card-hover-trigger:hover .cross-icon{visibility:visible!important}.cross-button-holder{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:end;-ms-flex-pack:end;justify-content:flex-end}.cross-button-holder .cross-icon{visibility:hidden}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "c23f":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"a784782c-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/basic/chart-legends.vue?vue&type=template&id=74971955&shadow
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"u-width-100"},[_c('div',{staticClass:"u-display-flex u-flex-justify-content-space-between u-flex-align-items-flex-start u-flex-wrap-yes"},[_c('div',{staticClass:"u-display-flex u-flex-wrap-yes"},[_vm._l((_vm.localMetricDisplayList),function(val,index){return _c('div',{key:index,staticClass:"metric-card-hover-trigger",attrs:{"val":val},on:{"click":function($event){$event.stopPropagation();return _vm.selectNewMetric(index, val)}}},[_c('div',{staticClass:"custom-chart-legend u-display-inline-flex u-flex-direction-column u-position-relative u-cursor-pointer workbench-hover-card"},[(_vm.computedSelectedMetric[val.key])?_c('div',{staticClass:"active-metric-card",style:({ 'background-color': _vm.metricColors[val.key] })}):_c('div',{staticClass:"metric-card-hover",style:(_vm.metricColors[val.key] ? { 'background-color': 'transparent' } :{ 'background-color': _vm.getColorPattern[index]})}),(index > _vm.minimumMetric - 1)?_c('div',{staticClass:"cross-button-holder"},[_c('div',{staticClass:"cross-icon",on:{"click":function($event){$event.stopPropagation();return _vm.deleteMetric(index)}}},[_c('rb-icon',{staticClass:"rb-icon--xx-small u-cursor-pointer u-color-grey-lighter",attrs:{"icon":'cross'}})],1)]):_vm._e(),_c('div',{staticClass:"u-spacing-ph-m u-spacing-pb-m",class:[index > (_vm.minimumMetric -1) ? '': 'u-spacing-pt-m']},[_c('div',[(val)?_c('rb-select',{staticClass:"u-spacing-mr-m",attrs:{"width":'240px',"context":[index],"sendDetails":true,"onClose":_vm.metricSelected,"options":_vm.metricsList,"className":'campaigns-select'},scopedSlots:_vm._u([{key:"item",fn:function(option){return [_c('div',{staticClass:"u-display-flex u-flex-align-items-center"},[_c('div',{staticClass:"u-overflow-hidden u-flex-direction-row u-display-flex u-flex-align-items-center u-width-100 u-spacing-pt-s u-spacing-pb-s u-spacing-pr-m u-spacing-pl-m u-flex-justify-content-space-between"},[_c('p',{staticClass:"u-color-grey-base u-text-overflow-ellipsis u-font-size-5 u-text-case-title"},[_vm._v(_vm._s(option.title))])])])]}}],null,true)},[_c('div',{staticClass:"u-display-flex u-flex-align-items-center u-cursor-pointer",attrs:{"slot":"trigger"},slot:"trigger"},[_c('div',{staticClass:"u-display-flex u-flex-align-items-center shareCompBrandsSelectTrigger"},[_c('span',{staticClass:"u-font-size-5"},[_vm._v(_vm._s(val.key))]),_c('rb-icon',{staticClass:"rb-icon--small u-spacing-ml-xs u-color-grey-lighter",attrs:{"icon":'caret-down'}})],1)])]):_vm._e()],1),(_vm.hasPVP && (_vm.metricData || {})[val.key])?_c('metric',{staticClass:"u-display-inline-flex u-spacing-mt-s",attrs:{"size":'l',"config":(_vm.metricConfig || {})[val.key],"data":(_vm.metricData || {})[val.key]}}):_vm._e(),(!val && !(((_vm.metricData || {})[val.key])))?_c('span',{staticClass:"u-color-grey-light u-font-size-5"},[_vm._v("No Data")]):_vm._e()],1)])])}),_c('div',[_c('rb-select',{staticClass:"u-spacing-mr-m",attrs:{"width":'240px',"sendDetails":true,"onClose":_vm.addNewMetric,"options":_vm.metricsList,"className":'campaigns-select u-height-100 select-trigger-height-100'},scopedSlots:_vm._u([{key:"item",fn:function(option){return [_c('div',{staticClass:"u-display-flex u-flex-align-items-center"},[_c('div',{staticClass:"u-overflow-hidden u-flex-direction-row u-display-flex u-flex-align-items-center u-width-100 u-spacing-pt-s u-spacing-pb-s u-spacing-pr-m u-spacing-pl-m u-flex-justify-content-space-between"},[_c('p',{staticClass:"u-color-grey-base u-text-overflow-ellipsis u-font-size-5 u-text-case-title"},[_vm._v(_vm._s(option.title))])])])]}}])},[_c('div',{staticClass:"u-display-flex u-flex-align-items-center u-cursor-pointer u-height-100",attrs:{"slot":"trigger"},slot:"trigger"},[_c('div',{staticClass:"u-display-flex u-flex-align-items-center shareCompBrandsSelectTrigger u-height-100"},[_c('div',{staticClass:"metric-card-hover-trigger u-height-100"},[_c('div',{staticClass:"custom-chart-legend u-display-inline-flex u-flex-direction-column u-position-relative u-cursor-pointer workbench-hover-card u-height-100"},[_c('div',{staticClass:"u-spacing-p-m u-display-flex u-flex-align-items-center u-flex-justify-content-center u-height-100"},[_c('span',{staticClass:"u-display-flex u-font-size-5 u-color-grey-lighter"},[_c('rb-icon',{staticClass:"rb-icon--small u-cursor-pointer u-color-grey-lighter u-spacing-mr-xs",attrs:{"icon":'add-circle-fill'}}),_vm._v("Add metric ")],1)])])])])])])],1)],2)])])}
var staticRenderFns = []


// CONCATENATED MODULE: ./src/components/basic/chart-legends.vue?vue&type=template&id=74971955&shadow

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.find-index.js
var es_array_find_index = __webpack_require__("c740");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.iterator.js
var es_array_iterator = __webpack_require__("e260");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.splice.js
var es_array_splice = __webpack_require__("a434");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.number.constructor.js
var es_number_constructor = __webpack_require__("a9e3");

// EXTERNAL MODULE: ./node_modules/core-js/modules/web.dom-collections.iterator.js
var web_dom_collections_iterator = __webpack_require__("ddb0");

// EXTERNAL MODULE: ./src/components/widgetMixin.js + 7 modules
var widgetMixin = __webpack_require__("6b5d");

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/basic/chart-legends.vue?vue&type=script&lang=js&shadow





//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
// import chart from './chart.vue'
 // import metric from '@/components/basic/metric'

/* harmony default export */ var chart_legendsvue_type_script_lang_js_shadow = ({
  components: {// chart,
    // metric
  },
  props: {
    metricDisplayList: {
      type: Array,
      default: () => []
    },
    metricsList: {
      type: Array,
      default: () => []
    },
    defaultSelectMetric: {
      type: Array,
      default: () => []
    },
    selectedMetricLimit: {
      type: Number,
      default: 2
    },
    metricConfig: {
      type: Object,
      default: () => {}
    },
    metricData: {
      type: Object,
      default: () => {}
    },
    minimumMetric: {
      type: Number,
      default: 2
    },
    hasPVP: {
      type: Boolean,
      default: true
    }
  },

  data() {
    return {
      metricsSelectedIndex: [0, 1],
      chartWkbenchAxes: {},
      selectedMetric: [],
      localMetricDisplayList: []
    };
  },

  created() {
    this.selectedMetric = this.defaultSelectMetric;
    this.localMetricDisplayList = JSON.parse(JSON.stringify(this.metricDisplayList));
    this.emitEventOut();
  },

  watch: {
    defaultSelectMetric(newValue) {
      this.selectedMetric = newValue;
    },

    metricDisplayList(newValue) {
      this.localMetricDisplayList = newValue;
    }

  },
  computed: {
    computedSelectedMetric() {
      var returnData = {};

      for (var i = 0; i < this.selectedMetric.length; i++) {
        returnData[this.selectedMetric[i].key] = this.selectedMetric[i];
      }

      return returnData;
    },

    metricColors() {
      var _colors = {};
      var colors = this.getColorPattern;

      for (var i = 0; i < this.localMetricDisplayList.length; i++) {
        _colors[this.localMetricDisplayList[i].key] = colors[i];
      }

      return _colors;
    }

  },
  methods: {
    // On Select of New Metric
    selectNewMetric(context, val) {
      var indexValue = this.selectedMetric.findIndex(item => {
        return item.key === val.key;
      });

      if (indexValue === -1) {
        this.smartPushQueue(val);
        this.chartWkbenchAxes[this.localMetricDisplayList[context].key] = context % 2 === 0 ? 'y' : 'y2';
      }
    },

    // On Select of new Metric of a card through drop down
    metricSelected(context, val) {
      var selectedIndex = val[0].selectedIndex;

      if (this.selectedMetric.findIndex(item => {
        return item.key === this.metricsList[selectedIndex].key;
      }) === -1) {
        this.localMetricDisplayList[context[0]] = this.metricsList && this.metricsList[selectedIndex];
        this.chartWkbenchAxes[this.metricsList[selectedIndex].key] = context % 2 === 0 ? 'y' : 'y2';
        this.localMetricDisplayList = [...this.localMetricDisplayList];
      }

      this.queueReplace(this.localMetricDisplayList[context[0]], this.metricsList[selectedIndex]);
    },

    // Pushing into the queue and queue is full we removed the last element (First in last out idea)
    smartPushQueue(value) {
      if (this.selectedMetric.length >= this.selectedMetricLimit) {
        this.selectedMetric.shift();
      }

      this.selectedMetric.push(value);
      this.emitEventOut();
    },

    // Searching for the item in the queue and replacing it with newValue
    queueReplace(oldValue, newValue) {
      var indexLocation = this.selectedMetric.findIndex(item => {
        return item.key === oldValue.key;
      });

      if (indexLocation === -1) {
        this.smartPushQueue(newValue);
      } else {
        this.selectedMetric[indexLocation] = newValue;
      }

      this.emitEventOut();
    },

    // Removing the element from the queue if found in the array
    queueDelete(value) {
      var valueIndex = this.selectedMetric.findIndex(item => {
        return item.key === value.key;
      });

      if (valueIndex !== -1) {
        this.selectedMetric.splice(valueIndex, 1);
      }

      this.emitEventOut();
    },

    // Just emitting the event
    emitEventOut() {
      var obj = {
        selectedMetric: this.selectedMetric,
        metricColors: this.metricColors
      };
      this.$emit('selectedList', obj);
    },

    // Adding new metric and pushing into the queue
    addNewMetric(context, val) {
      var selectedIndex = val[0].selectedIndex;
      this.localMetricDisplayList.push(this.metricsList[selectedIndex]);
      this.smartPushQueue(this.metricsList[selectedIndex]);
    },

    // Deleting the metric and deleting from the queue
    deleteMetric(index) {
      this.queueDelete(this.localMetricDisplayList[index]);
      this.localMetricDisplayList.splice(index, 1);
    }

  },
  mixins: [widgetMixin["a" /* default */]]
});
// CONCATENATED MODULE: ./src/components/basic/chart-legends.vue?vue&type=script&lang=js&shadow
 /* harmony default export */ var basic_chart_legendsvue_type_script_lang_js_shadow = (chart_legendsvue_type_script_lang_js_shadow); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__("2877");

// CONCATENATED MODULE: ./src/components/basic/chart-legends.vue?shadow



function injectStyles (context) {
  
  var style0 = __webpack_require__("7832")
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  basic_chart_legendsvue_type_script_lang_js_shadow,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  null
  ,true
)

/* harmony default export */ var chart_legendsshadow = __webpack_exports__["default"] = (component.exports);

/***/ })

}]);
//# sourceMappingURL=ciq-ui.13.js.map