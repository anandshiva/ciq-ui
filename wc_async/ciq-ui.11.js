(window["ciqUi_jsonp"] = window["ciqUi_jsonp"] || []).push([[11,4],{

/***/ "0230":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"a784782c-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/basic/chart-with-legends.vue?vue&type=template&id=b150c1c0&shadow
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"u-width-100"},[_c('chart-legends',{attrs:{"metricDisplayList":_vm.metricsShown,"metricsList":_vm.metricsList,"defaultSelectMetric":_vm.defaultSelectMetric,"metricConfig":_vm.metricConfig,"metricData":_vm.metricData},on:{"selectedList":_vm.metricChanged}}),(_vm.hasChartEvents)?_c('chart-event-legends',{staticClass:"u-spacing-mt-l",attrs:{"metricDisplayList":_vm.eventsShown,"metricsList":_vm.eventsList,"defaultSelectMetric":_vm.defaultSelectEvents,"selectedMetricLimit":3},on:{"selectedList":_vm.eventsChanged}}):_vm._e(),_c('chart',{staticClass:"c3-large u-flex-1 u-spacing-mt-l",staticStyle:{"height":"500px"},attrs:{"id":"workbenchCampaignsChart","config":_vm.chartConfig,"data":_vm.formattedChartData}})],1)}
var staticRenderFns = []


// CONCATENATED MODULE: ./src/components/basic/chart-with-legends.vue?vue&type=template&id=b150c1c0&shadow

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.concat.js
var es_array_concat = __webpack_require__("99af");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.find-index.js
var es_array_find_index = __webpack_require__("c740");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.index-of.js
var es_array_index_of = __webpack_require__("c975");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.sort.js
var es_array_sort = __webpack_require__("4e82");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.splice.js
var es_array_splice = __webpack_require__("a434");

// EXTERNAL MODULE: ./src/components/basic/chart.vue + 4 modules
var chart = __webpack_require__("8b3e");

// EXTERNAL MODULE: ./src/components/basic/chart-legends.vue + 4 modules
var chart_legends = __webpack_require__("8110");

// EXTERNAL MODULE: ./src/components/basic/chart-events-legends.vue + 4 modules
var chart_events_legends = __webpack_require__("cd49");

// EXTERNAL MODULE: ./src/components/widgetMixin.js + 7 modules
var widgetMixin = __webpack_require__("6b5d");

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/basic/chart-with-legends.vue?vue&type=script&lang=js&shadow





//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
// import Vue from 'vue';




/* harmony default export */ var chart_with_legendsvue_type_script_lang_js_shadow = ({
  components: {
    chart: chart["a" /* default */],
    chartLegends: chart_legends["a" /* default */],
    chartEventLegends: chart_events_legends["a" /* default */]
  },
  props: {
    ignoreList: {
      type: Array,
      default: () => []
    },
    metricConfig: {
      type: Object,
      default: () => {}
    },
    metricData: {
      type: Object,
      default: () => {}
    },
    chartData: {
      type: Array,
      default: () => []
    },
    chartConfig: {
      type: Object,
      default: () => {}
    },
    defaultSelectMetric: {
      type: Array,
      default: () => []
    },
    metricsShown: {
      type: Array,
      default: () => []
    },
    eventsShown: {
      type: Array,
      default: () => []
    },
    eventsList: {
      type: Array,
      default: () => []
    },
    defaultSelectEvents: {
      type: Array,
      default: () => []
    },
    hasChartEvents: {
      type: Boolean,
      default: false
    }
  },

  data() {
    return {
      metricsSelectedIndex: [0, 1],
      chartWkbenchAxes: {},
      metricColors: {},
      metricsSelected: [],
      formattedChartData: {},
      selectedMetric: [],
      selectedEvents: []
    };
  },

  mounted() {},

  created() {},

  computed: {
    metricsList() {
      var data = this.chartData;
      var returnArr = [];

      for (var i = 0; i < data.length; i++) {
        returnArr.push({
          title: data[i][0],
          key: data[i][0]
        });
      }

      return returnArr.sort((a, b) => {
        if (a.title.toLowerCase() < b.title.toLowerCase()) {
          return -1;
        }

        if (a.title.toLowerCase() > b.title.toLowerCase()) {
          return 1;
        }

        return 0;
      });
    }

  },
  methods: {
    selectNewMetric(context, val) {
      console.log(context, val);

      if (this.metricsSelectedIndex.indexOf(context[0]) === -1) {
        this.metricsSelectedIndex.shift();
        this.metricsSelectedIndex.push(context[0]);
        this.chartWkbenchAxes[val[0]] = context[0] % 2 === 0 ? 'y' : 'y2';
        console.log(this.chartWkbenchAxes);
        this.isExpand = false;
      }
    },

    metricSelected(context, val) {
      if (this.metricsShown.indexOf(val[0].title) === -1) {
        this.metricsShown.splice(context[0], 1, val[0].title);
        this.selectNewMetric(context, [val[0].title]);
        this.isExpand = false;
      }
    },

    metricChanged(data) {
      console.log('metricChanged', data);
      this.selectedMetric = data['selectedMetric'];
      var metircList = this.selectedMetric.concat(this.selectedEvents);
      this.formatChartData(data['metricColors'], metircList);
    },

    eventsChanged(data) {
      this.selectedEvents = data['selectedMetric'];
      var metircList = this.selectedMetric.concat(this.selectedEvents);
      this.formatChartData(data['metricColors'], metircList);
    },

    formatChartData(metricColors, selectedMetric) {
      var _this = this;

      var axes = {};
      var data = [];

      var _loop = function _loop(i) {
        var metric = selectedMetric[i];
        axes[metric.key] = i % 2 === 0 ? 'y' : 'y2';

        var indexOfMetric = _this.chartData.findIndex(item => {
          return item[0] === metric.key;
        });

        if (indexOfMetric !== -1) {
          data.push(_this.chartData[indexOfMetric]);
        }
      };

      for (var i = 0; i < selectedMetric.length; i++) {
        _loop(i);
      }

      var timeseriesIndex = this.chartData.findIndex(item => {
        return item[0] === this.chartConfig.chartOptions.timeseries;
      });
      data.push(this.chartData[timeseriesIndex]);
      this.formattedChartData = {
        data: data,
        axes: axes,
        colors: metricColors
      };
      console.log(this.formattedChartData);
    }

  },
  mixins: [widgetMixin["a" /* default */]]
});
// CONCATENATED MODULE: ./src/components/basic/chart-with-legends.vue?vue&type=script&lang=js&shadow
 /* harmony default export */ var basic_chart_with_legendsvue_type_script_lang_js_shadow = (chart_with_legendsvue_type_script_lang_js_shadow); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__("2877");

// CONCATENATED MODULE: ./src/components/basic/chart-with-legends.vue?shadow





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  basic_chart_with_legendsvue_type_script_lang_js_shadow,
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  ,true
)

/* harmony default export */ var chart_with_legendsshadow = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "2c33":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_3_node_modules_sass_loader_dist_cjs_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_chart_legends_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("38f9");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_3_node_modules_sass_loader_dist_cjs_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_chart_legends_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_3_node_modules_sass_loader_dist_cjs_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_chart_legends_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_3_node_modules_sass_loader_dist_cjs_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_chart_legends_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_3_node_modules_sass_loader_dist_cjs_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_chart_legends_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_3_node_modules_sass_loader_dist_cjs_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_chart_legends_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "38f9":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("67d2");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to Shadow Root
var add = __webpack_require__("35d6").default
module.exports.__inject__ = function (shadowRoot) {
  add("0dc86de0", content, shadowRoot)
};

/***/ }),

/***/ "4678":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "2bfb",
	"./af.js": "2bfb",
	"./ar": "8e73",
	"./ar-dz": "a356",
	"./ar-dz.js": "a356",
	"./ar-kw": "423e",
	"./ar-kw.js": "423e",
	"./ar-ly": "1cfd",
	"./ar-ly.js": "1cfd",
	"./ar-ma": "0a84",
	"./ar-ma.js": "0a84",
	"./ar-sa": "8230",
	"./ar-sa.js": "8230",
	"./ar-tn": "6d83",
	"./ar-tn.js": "6d83",
	"./ar.js": "8e73",
	"./az": "485c",
	"./az.js": "485c",
	"./be": "1fc1",
	"./be.js": "1fc1",
	"./bg": "84aa",
	"./bg.js": "84aa",
	"./bm": "a7fa",
	"./bm.js": "a7fa",
	"./bn": "9043",
	"./bn.js": "9043",
	"./bo": "d26a",
	"./bo.js": "d26a",
	"./br": "6887",
	"./br.js": "6887",
	"./bs": "2554",
	"./bs.js": "2554",
	"./ca": "d716",
	"./ca.js": "d716",
	"./cs": "3c0d",
	"./cs.js": "3c0d",
	"./cv": "03ec",
	"./cv.js": "03ec",
	"./cy": "9797",
	"./cy.js": "9797",
	"./da": "0f14",
	"./da.js": "0f14",
	"./de": "b469",
	"./de-at": "b3eb",
	"./de-at.js": "b3eb",
	"./de-ch": "bb71",
	"./de-ch.js": "bb71",
	"./de.js": "b469",
	"./dv": "598a",
	"./dv.js": "598a",
	"./el": "8d47",
	"./el.js": "8d47",
	"./en-SG": "cdab",
	"./en-SG.js": "cdab",
	"./en-au": "0e6b",
	"./en-au.js": "0e6b",
	"./en-ca": "3886",
	"./en-ca.js": "3886",
	"./en-gb": "39a6",
	"./en-gb.js": "39a6",
	"./en-ie": "e1d3",
	"./en-ie.js": "e1d3",
	"./en-il": "7333",
	"./en-il.js": "7333",
	"./en-nz": "6f50",
	"./en-nz.js": "6f50",
	"./eo": "65db",
	"./eo.js": "65db",
	"./es": "898b",
	"./es-do": "0a3c",
	"./es-do.js": "0a3c",
	"./es-us": "55c9",
	"./es-us.js": "55c9",
	"./es.js": "898b",
	"./et": "ec18",
	"./et.js": "ec18",
	"./eu": "0ff2",
	"./eu.js": "0ff2",
	"./fa": "8df4",
	"./fa.js": "8df4",
	"./fi": "81e9",
	"./fi.js": "81e9",
	"./fo": "0721",
	"./fo.js": "0721",
	"./fr": "9f26",
	"./fr-ca": "d9f8",
	"./fr-ca.js": "d9f8",
	"./fr-ch": "0e49",
	"./fr-ch.js": "0e49",
	"./fr.js": "9f26",
	"./fy": "7118",
	"./fy.js": "7118",
	"./ga": "5120",
	"./ga.js": "5120",
	"./gd": "f6b4",
	"./gd.js": "f6b4",
	"./gl": "8840",
	"./gl.js": "8840",
	"./gom-latn": "0caa",
	"./gom-latn.js": "0caa",
	"./gu": "e0c5",
	"./gu.js": "e0c5",
	"./he": "c7aa",
	"./he.js": "c7aa",
	"./hi": "dc4d",
	"./hi.js": "dc4d",
	"./hr": "4ba9",
	"./hr.js": "4ba9",
	"./hu": "5b14",
	"./hu.js": "5b14",
	"./hy-am": "d6b6",
	"./hy-am.js": "d6b6",
	"./id": "5038",
	"./id.js": "5038",
	"./is": "0558",
	"./is.js": "0558",
	"./it": "6e98",
	"./it-ch": "6f12",
	"./it-ch.js": "6f12",
	"./it.js": "6e98",
	"./ja": "079e",
	"./ja.js": "079e",
	"./jv": "b540",
	"./jv.js": "b540",
	"./ka": "201b",
	"./ka.js": "201b",
	"./kk": "6d79",
	"./kk.js": "6d79",
	"./km": "e81d",
	"./km.js": "e81d",
	"./kn": "3e92",
	"./kn.js": "3e92",
	"./ko": "22f8",
	"./ko.js": "22f8",
	"./ku": "2421",
	"./ku.js": "2421",
	"./ky": "9609",
	"./ky.js": "9609",
	"./lb": "440c",
	"./lb.js": "440c",
	"./lo": "b29d",
	"./lo.js": "b29d",
	"./lt": "26f9",
	"./lt.js": "26f9",
	"./lv": "b97c",
	"./lv.js": "b97c",
	"./me": "293c",
	"./me.js": "293c",
	"./mi": "688b",
	"./mi.js": "688b",
	"./mk": "6909",
	"./mk.js": "6909",
	"./ml": "02fb",
	"./ml.js": "02fb",
	"./mn": "958b",
	"./mn.js": "958b",
	"./mr": "39bd",
	"./mr.js": "39bd",
	"./ms": "ebe4",
	"./ms-my": "6403",
	"./ms-my.js": "6403",
	"./ms.js": "ebe4",
	"./mt": "1b45",
	"./mt.js": "1b45",
	"./my": "8689",
	"./my.js": "8689",
	"./nb": "6ce3",
	"./nb.js": "6ce3",
	"./ne": "3a39",
	"./ne.js": "3a39",
	"./nl": "facd",
	"./nl-be": "db29",
	"./nl-be.js": "db29",
	"./nl.js": "facd",
	"./nn": "b84c",
	"./nn.js": "b84c",
	"./pa-in": "f3ff",
	"./pa-in.js": "f3ff",
	"./pl": "8d57",
	"./pl.js": "8d57",
	"./pt": "f260",
	"./pt-br": "d2d4",
	"./pt-br.js": "d2d4",
	"./pt.js": "f260",
	"./ro": "972c",
	"./ro.js": "972c",
	"./ru": "957c",
	"./ru.js": "957c",
	"./sd": "6784",
	"./sd.js": "6784",
	"./se": "ffff",
	"./se.js": "ffff",
	"./si": "eda5",
	"./si.js": "eda5",
	"./sk": "7be6",
	"./sk.js": "7be6",
	"./sl": "8155",
	"./sl.js": "8155",
	"./sq": "c8f3",
	"./sq.js": "c8f3",
	"./sr": "cf1e",
	"./sr-cyrl": "13e9",
	"./sr-cyrl.js": "13e9",
	"./sr.js": "cf1e",
	"./ss": "52bd",
	"./ss.js": "52bd",
	"./sv": "5fbd",
	"./sv.js": "5fbd",
	"./sw": "74dc",
	"./sw.js": "74dc",
	"./ta": "3de5",
	"./ta.js": "3de5",
	"./te": "5cbb",
	"./te.js": "5cbb",
	"./tet": "576c",
	"./tet.js": "576c",
	"./tg": "3b1b",
	"./tg.js": "3b1b",
	"./th": "10e8",
	"./th.js": "10e8",
	"./tl-ph": "0f38",
	"./tl-ph.js": "0f38",
	"./tlh": "cf75",
	"./tlh.js": "cf75",
	"./tr": "0e81",
	"./tr.js": "0e81",
	"./tzl": "cf51",
	"./tzl.js": "cf51",
	"./tzm": "c109",
	"./tzm-latn": "b53d",
	"./tzm-latn.js": "b53d",
	"./tzm.js": "c109",
	"./ug-cn": "6117",
	"./ug-cn.js": "6117",
	"./uk": "ada2",
	"./uk.js": "ada2",
	"./ur": "5294",
	"./ur.js": "5294",
	"./uz": "2e8c",
	"./uz-latn": "010e",
	"./uz-latn.js": "010e",
	"./uz.js": "2e8c",
	"./vi": "2921",
	"./vi.js": "2921",
	"./x-pseudo": "fd7e",
	"./x-pseudo.js": "fd7e",
	"./yo": "7f33",
	"./yo.js": "7f33",
	"./zh-cn": "5c3a",
	"./zh-cn.js": "5c3a",
	"./zh-hk": "49ab",
	"./zh-hk.js": "49ab",
	"./zh-tw": "90ea",
	"./zh-tw.js": "90ea"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "4678";

/***/ }),

/***/ "494d":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_3_node_modules_sass_loader_dist_cjs_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_chart_events_legends_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("ed20");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_3_node_modules_sass_loader_dist_cjs_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_chart_events_legends_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_3_node_modules_sass_loader_dist_cjs_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_chart_events_legends_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_3_node_modules_sass_loader_dist_cjs_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_chart_events_legends_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_3_node_modules_sass_loader_dist_cjs_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_chart_events_legends_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_3_node_modules_sass_loader_dist_cjs_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_chart_events_legends_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "67d2":
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__("24fb");
var ___CSS_LOADER_AT_RULE_IMPORT_0___ = __webpack_require__("5e0d");
exports = ___CSS_LOADER_API_IMPORT___(false);
exports.i(___CSS_LOADER_AT_RULE_IMPORT_0___);
// Module
exports.push([module.i, ".u-font-size-5{font-size:1.4rem}.metric-card-hover-trigger:hover .cross-icon{visibility:visible!important}.cross-button-holder{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:end;-ms-flex-pack:end;justify-content:flex-end}.cross-button-holder .cross-icon{visibility:hidden}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "6b5d":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.filter.js
var es_array_filter = __webpack_require__("4de4");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.function.name.js
var es_function_name = __webpack_require__("b0c0");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.object.to-string.js
var es_object_to_string = __webpack_require__("d3b7");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.promise.js
var es_promise = __webpack_require__("e6cf");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.regexp.constructor.js
var es_regexp_constructor = __webpack_require__("4d63");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.regexp.to-string.js
var es_regexp_to_string = __webpack_require__("25f0");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.replace.js
var es_string_replace = __webpack_require__("5319");

// EXTERNAL MODULE: ./node_modules/axios/index.js
var axios = __webpack_require__("bc3a");
var axios_default = /*#__PURE__*/__webpack_require__.n(axios);

// CONCATENATED MODULE: ./src/utils/services/url-factory.js
/* harmony default export */ var url_factory = ({
  getFinalURL(url) {
    return this[url];
  },

  EMAIL: '/api/mail',
  SESSION_DATA: '/api/config',
  RECOMMENDATIONS: '/api/proxy/aramusplatform/rest/recommendation',
  RECOMMENDATIONS_OF_TYPE: '/api/proxy/aramusplatform/rest/recommendation/recommendationMetaData/:recommendationType/:recommendationName',
  RECOMMENDATION_FILTERS: '/api/proxy/aramusplatform/rest/recommendation/dimension',
  RECOMMENDATION_COLUMNS: '/api/proxy/aramusplatform/rest/recommendation/recommendationaLableMapping',
  UPDATE_RECOMMENDATION_COLUMNS: '/api/proxy/aramusplatform/rest/recommendation/uiLabelMapping',
  SEARCH_RECOMMENDATIONS: '/api/proxy/aramusplatform/rest/recommendation/searchRecommendation/:searchString',
  REMOVE_RECOMMENDATIONS: '/api/proxy/aramusplatform/rest/recommendation/removeSKU',
  RESTORE_RECOMMENDATIONS: '/api/proxy/aramusplatform/rest/recommendation/restoreSKU',
  GET_REMOVED_RECOMMENDATIONS: '/api/proxy/aramusplatform/rest/recommendation/getRemovedSKU',
  GET_STRATEGIES: '/api/proxy/aramusplatform/rest/strategies',
  SIMULTAED_ALERTS: '/api/proxy/aramusplatform/rest/recommendation/simulatedAlert/list',
  LOGIN: '/api/login',
  LOGOUT: '/api/logout',
  REGISTER: '/api/user/register',
  PASSWORD_RESET_LINK: '/api/user/forgotPassword',
  PASSWORD_CHANGE: '/api/user/setPassword',
  INVITE_USERS: 'api/user/inviteUsers',
  INVITE_USERS_V2: 'api/user/inviteUsers/v2',
  CHECK_USER_STATUS: '/api/user/welcome',
  TABLEAU_GET_TRUST_URL: '/api/tableau/getTrustedviewUrl',
  REVENUE_CHART: {
    demo: {
      table: 'Table 1',
      view: 'sample'
    },
    prod: '/api/revenueChart'
  },
  GET_CUBE_LIST: '/api/proxy/brands-service/cube/list',
  GET_CUBE_DETAILS: '/api/proxy/brands-service/cube/getDetails',
  GET_CUBE_QUERY: '/api/proxy/brands-service/cube/view',
  EXECUTE_CUBE_API: '/api/proxy/brands-service/cube/execute',
  EXECUTE_CUBE_API_EMAIL: '/api/proxy/brands-service/cube/execute/download',
  EXECUTE_CUBE_CHART_API: '/api/proxy/brands-service/cube/execute/charts',
  EXECUTE_CUSTOM_CUBE_API: '/api/proxy/brands-service/cube/execute/charts/sku',
  EXECUTE_CUSTOM_CUBE_SKU_API: '/api/proxy/brands-service/cube/execute/custom',
  EXECUTE_CUBE_API_V2: '/api/proxy/brands-service/cube/execute/custom/v2',
  SOV_BRANDS_CUSTOM: '/api/proxy/brands-service/cube/execute/v2/sov/brands',
  getOTP: '/api/proxy/aramus/rest/twoFactor/getOtp',
  SEND_FEEDBACK: '/api/feedback/post',
  HARDFETCHSKUDETAILS: '/api/proxy/aramusplatform/rest/recommendation/recommendationDetails',
  KEEPALIVE: '/api/keepalive',
  RECOMMENDATION_ACTION_CHANGE: '/api/proxy/aramusplatform/rest/recommendation/saveTaskAction',
  GET_SELF_STRATEGIES: '/api/proxy/aramusplatform/rest/strategies/getSelfServeStrategyDetails',
  FETCH_FILTERS: '/api/proxy/brands-service/internal/dimension/filters',
  FETCH_FILTERS_V2: '/api/proxy/brands-service/filter/filters',
  SAVE_FILTER: '/api/proxy/brands-service/filter/create',
  DELETE_FILTER: '/api/proxy/brands-service/filter/delete',
  UPDATE_SELF_SERVE_STATUS: '/api/proxy/aramusplatform/rest/strategies/updateStrategyStatus',
  USERS: '/api/user/users',
  SUBMIT_ACTIONS: '/api/proxy/aramusplatform/rest/action/submitActions',
  AMS_ACTIONS: '/api/proxy/brands-service/worklog',
  ENTITY_VALIDATION: '/api/proxy/strategy-service/v1/strategy/entity/validation',
  CREATE_STRATEGY: '/api/proxy/strategy-service/v1/strategy',
  GET_STRATEGY_DATA: '/api/proxy/strategy-service/v1/strategy/data',
  TEMPLATELIST: '/api/proxy/aramusplatform/rest/template/list/alert',
  CREATETEMPLATE: '/api/proxy/aramusplatform/rest/template/create',
  READPARAMSFORALERT: '/api/proxy/aramusplatform/rest/alert/read',
  SIMULATETEMPLATE: '/api/proxy/aramusplatform/rest/selfserve/simulation/simulate',
  FETCHSIMULATIONLIST: '/api/proxy/aramusplatform/rest/selfserve/simulation/list/:templateID',
  PUBLISHTEMPLATE: '/api/proxy/aramusplatform/rest/template/publish',
  READALLPARAMETERS: '/api/proxy/aramusplatform/rest/alert/params/all',
  DELETETEMPLATE: '/api/proxy/aramusplatform/rest/template/delete',
  READTEMPLATE: '/api/proxy/aramusplatform/rest/template/read',
  READSUBSCRIPTION: '/api/proxy/subscription-service/subscription/read',
  UPDATESUBSCRIPTION: '/api/proxy/subscription-service/subscription/update',
  CREATESUBSCRIPTION: '/api/proxy/subscription-service/subscription/create',
  UNSUBSCRIPTION: '/api/proxy/subscription-service/subscription/unsubscribe/internal'
});
// CONCATENATED MODULE: ./src/utils/services/http-service.js






 // import { store } from '@/store/store'

var http_service_getUrl = function getUrl(id, config) {
  // automatically replaces :param in a url like a/:param/b/;param
  var url = url_factory.getFinalURL(id);

  if (config && config.append) {
    url = url + config.append;
  }

  if (!config || !config.pathParams) return url;
  var paramList = Object.keys(config.pathParams);

  for (var _i = 0, _paramList = paramList; _i < _paramList.length; _i++) {
    var param = _paramList[_i];
    var regex1 = '/:' + param + '/';
    var regex2 = '/:' + param + '$';
    url = url.replace(new RegExp(regex1, 'g'), '/' + config.pathParams[param] + '/').replace(new RegExp(regex2, 'g'), '/' + config.pathParams[param]);
  }

  return url;
};

axios_default.a.interceptors.response.use(function (response) {
  return response;
}, function (err) {
  // if (err.response.status === 401 && store.getters.getSessionValidity) {
  if (err.response.status === 401) {
    // store.commit('LOGOUT', 'returnslink=' + encodeURIComponent(window.location));
    return Promise.reject(err);
  } else {
    return Promise.reject(err);
  }
});
/* harmony default export */ var http_service = ({
  all: axios_default.a.all,

  get(id, config) {
    return axios_default.a.get(http_service_getUrl(id, config), config);
  },

  post(id, data, config) {
    return axios_default.a.post(http_service_getUrl(id, config), data, config);
  },

  put(id, data, config) {
    return axios_default.a.put(http_service_getUrl(id, config), data, config);
  },

  patch(id, data, config) {
    return axios_default.a.patch(http_service_getUrl(id, config), data, config);
  }

});
// CONCATENATED MODULE: ./src/utils/services/http-layer.js

/* harmony default export */ var http_layer = ({
  all: http_service.all,
  post: config => {
    if (!config.APIData) {
      config.APIData = {};
    }

    if (!config.header) {
      config.header = {};
    }

    var oPromise = http_service.post(config.cube || 'EXECUTE_CUBE_API', config.APIData, config.header).then(response => {
      var _response = {};

      if (response.data.success) {
        _response.success = true;

        if (response.data.response.data) {
          _response.data = response.data.response.data;
        }

        if (response.data.response.dataSize) {
          _response.dataSize = response.data.response.dataSize;
        }
      } else {
        _response.success = false;
      }

      _response.fullResponse = response.data.response;
      return _response;
    }).catch(error => {
      console.log(error);
      return {
        success: false
      };
    });
    return oPromise;
  }
});
// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.concat.js
var es_array_concat = __webpack_require__("99af");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.find-index.js
var es_array_find_index = __webpack_require__("c740");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.index-of.js
var es_array_index_of = __webpack_require__("c975");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.iterator.js
var es_array_iterator = __webpack_require__("e260");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.map.js
var es_array_map = __webpack_require__("d81d");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.sort.js
var es_array_sort = __webpack_require__("4e82");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.splice.js
var es_array_splice = __webpack_require__("a434");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.number.constructor.js
var es_number_constructor = __webpack_require__("a9e3");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.split.js
var es_string_split = __webpack_require__("1276");

// EXTERNAL MODULE: ./node_modules/core-js/modules/web.dom-collections.iterator.js
var web_dom_collections_iterator = __webpack_require__("ddb0");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.number.to-fixed.js
var es_number_to_fixed = __webpack_require__("b680");

// EXTERNAL MODULE: ./node_modules/vue/dist/vue.esm.js
var vue_esm = __webpack_require__("a026");

// EXTERNAL MODULE: ./node_modules/moment/moment.js
var moment = __webpack_require__("c1df");
var moment_default = /*#__PURE__*/__webpack_require__.n(moment);

// CONCATENATED MODULE: ./src/utils/helpers/formatter.js





function getValueAlphabets(value) {
  var sign = '';

  if (value < 0) {
    sign = '-';
    value = value * -1;
  }

  var q = '';

  if (value > 1000000000) {
    q = 'B';
    value = value / 1000000000;
  } else if (value > 1000000) {
    q = 'M';
    value = value / 1000000;
  } else if (value > 1000) {
    q = 'K';
    value = value / 1000;
  }

  if (!Number.isInteger(value)) {
    value = value.toFixed(2);
  }

  return {
    sign: sign,
    value: value,
    notation: q
  };
}

function formatter(value, format) {
  if (!format) {
    format = 'string';
  }

  if (value === undefined || value === null) {
    return 'NA';
  }

  if (format.toLowerCase() === 'string') {
    return value;
  } else if (format.toLowerCase() === 'currency') {
    var obj = getValueAlphabets(value);
    return obj.sign + vue_esm["a" /* default */].prototype.$currency + obj.value + obj.notation;
  } else if (format.toLowerCase() === 'percent' || format.toLowerCase() === 'percentage') {
    return value === 0 ? '0%' : value.toFixed(2) + '%';
  } else if (format.toLowerCase() === 'date') {
    // return new Date(value)
    //   .toLocaleString('en-US', { timeZone: 'UTC' })
    //   .split(',')[0];
    // Format Change for KC-UK
    return moment_default()(value).format('MMM DD, YYYY');
  } else if (format === 'percentFraction') {
    return value + '/100';
  } else if (format.toLowerCase() === 'numeric') {
    obj = getValueAlphabets(value);
    return obj.sign + obj.value + obj.notation;
  } else if (format.toLowerCase() === 'numberic_x') {
    return value + 'X';
  } else {
    return value;
  }
}
// CONCATENATED MODULE: ./src/utils/helpers/operator.js
var operator = {
  EQUAL_TO: '=',
  GREATER_THAN: '>',
  GREATER_THAN_OR_EQUAL_TO: '> =',
  LESS_THAN: '<',
  LESS_THAN_OR_EQUAL_TO: '< =',
  NOT_EQUAL_TO: '! =',
  ILIKE: 'Has',
  BETWEEN: '> & <'
};
var operatorTitle = {
  '=': 'Equals',
  '>': 'Greater than',
  '> =': 'Greater than equal to',
  '<': 'Less than',
  '< =': 'Less than equal to',
  '! =': 'Not equal to',
  '> & <': 'Between'
};

// CONCATENATED MODULE: ./src/utils/services/data-transformer.js
















/* harmony default export */ var data_transformer = ({
  getUniqueFilters: (data, prefix, filterMappings) => {
    if (!prefix) {
      prefix = '';
    }

    if (!filterMappings) {
      filterMappings = JSON.parse(localStorage.getItem(prefix.replace('_', '') + '_filters_mapping'));
    }

    var categoryText = 'category';
    var subCategoryText = 'subcategory';

    if (data.length > 0 && data[0].DIMENSION['category'] === undefined) {// categoryText = 'l1';
    }

    if (data.length > 0 && data[0].DIMENSION['category'] === undefined && data[0].DIMENSION['l1'] === undefined) {
      categoryText = 'client_category';
    }

    if (data.length > 0 && data[0].DIMENSION['subcategory'] === undefined) {// subCategoryText = 'l2';
    }

    if (data.length > 0 && data[0].DIMENSION['subcategory'] === undefined && data[0].DIMENSION['l2'] === undefined) {
      subCategoryText = 'client_subcategory';
    }

    var category = {};
    var subcategory = {};
    var oObj = {};
    var _oReturn = {};

    for (var i = 0; i < data.length; i++) {
      if (data[i].DIMENSION[categoryText] !== undefined && data[i].DIMENSION[subCategoryText] !== undefined) {
        if (!category[data[i].DIMENSION[categoryText]]) {
          category[data[i].DIMENSION[categoryText]] = {
            title: data[i].DIMENSION[categoryText],
            values: []
          };
          subcategory[data[i].DIMENSION[categoryText]] = {};
        }

        if (!subcategory[data[i].DIMENSION[categoryText]][data[i].DIMENSION[subCategoryText]]) {
          subcategory[data[i].DIMENSION[categoryText]][data[i].DIMENSION[subCategoryText]] = true;

          if (data[i].DIMENSION[subCategoryText]) {
            category[data[i].DIMENSION[categoryText]].values.push({
              title: data[i].DIMENSION[subCategoryText]
            });
          }
        }
      }

      for (var j in data[i].DIMENSION) {
        if (!oObj[prefix + j]) {
          oObj[prefix + j] = {};
        }

        var found = true;

        if (filterMappings && filterMappings[prefix + j]) {
          for (var y in filterMappings[prefix + j]) {
            if (filterMappings[prefix + j][y] && filterMappings[prefix + j][y].length === 0) {
              found = true;
              break;
            }

            if (filterMappings[prefix + j][y].indexOf(data[i].DIMENSION[y.replace(prefix, '')]) === -1) {
              found = false;
              break;
            }
          }
        }

        if (found) {
          oObj[prefix + j][data[i].DIMENSION[j]] = true;
        } else {
          if (oObj[prefix + j][data[i].DIMENSION[j]] !== true) {
            delete oObj[prefix + j][data[i].DIMENSION[j]];
          }
        }
      }
    }

    for (var k in oObj) {
      if (!_oReturn[k]) {
        _oReturn[k] = [];
      }

      for (var l in oObj[k]) {
        if (l && l !== 'null' && l.length > 0) {
          _oReturn[k].push({
            title: l
          });
        }
      }
    }

    if (_oReturn[prefix + categoryText]) {
      var oReturnCategory = [];

      for (var t in category) {
        if (oObj[prefix + categoryText][t]) {
          if (category[t].values && category[t].values.length > 0) {
            for (var r = 0; r < category[t].values.length; r++) {
              if (!oObj[prefix + subCategoryText][category[t].values[r].title] || t.length === 0) {
                category[t].values.splice(r, 1);
              }
            }
          }

          if (t !== 'null' && t.length > 0) {
            oReturnCategory.push(category[t]);
          }
        }
      }

      _oReturn[prefix + categoryText] = oReturnCategory;
      delete _oReturn[prefix + subCategoryText];
    }

    return _oReturn;
  },
  getChartDataInFormat: (data, response) => {
    var _aArray = [];
    var _oObj = {};

    if (response === null) {
      return [];
    }

    for (var i = 0; i < response.length; i++) {
      for (var j in response[i]) {
        var _j = ((data || {}).map || {})[j] || j;

        if (!_oObj[_j]) {
          _oObj[_j] = [_j];
        }

        _oObj[_j].push(response[i][j]);
      }
    }

    for (var k in _oObj) {
      _aArray.push(_oObj[k]);
    }

    return _aArray;
  },
  getChartTicksValues: (data, key) => {
    var newArr = [];

    if (data && data.length) {
      for (var i = 0; i < data.length; i++) {
        newArr.push(data[i][key]);
      }
    }

    return newArr;
  },
  mergeResultDimension: (data, addPVP, removeNullProp) => {
    var _aArray = [];

    if (data && data.length) {
      for (var i = 0; i < data.length; i++) {
        var oObj = {};

        for (var j in data[i]['RESULT']) {
          oObj[j] = data[i]['RESULT'][j];
        }

        for (j in data[i]['DIMENSION']) {
          oObj[j] = data[i]['DIMENSION'][j];
        }

        if (addPVP) {
          for (j in data[i]['PVP']) {
            oObj['PVP_' + j] = data[i]['PVP'][j];
          }
        }

        if (removeNullProp) {
          if (oObj[removeNullProp] !== undefined) {
            if (oObj[removeNullProp] !== null && oObj[removeNullProp] !== '') {
              _aArray.push(oObj);
            }
          } else {
            _aArray.push(oObj);
          }
        } else {
          _aArray.push(oObj);
        }
      }
    }

    return _aArray;
  },
  getCompleteWhereClause: (where, stateSelectedFilters) => {
    var oReturn = {
      dimensionNameValueList: []
    };

    if (where) {
      for (var i = 0; i < where.length; i++) {
        oReturn.dimensionNameValueList.push(where[i]);
      }
    }

    if (stateSelectedFilters && stateSelectedFilters.dimensionNameValueList) {
      for (i = 0; i < stateSelectedFilters.dimensionNameValueList.length; i++) {
        if (oReturn.dimensionNameValueList.indexOf(stateSelectedFilters.dimensionNameValueList[i]) === -1) {
          var item = stateSelectedFilters.dimensionNameValueList[i]; // convert 'BETWEEN' operator to 'GREATER_THAN_OR_EQUAL' and 'LESS_THAN_OR_EQUAL' operators

          if (item.operator && item.operator === 'BETWEEN') {
            var obj1 = {
              operator: 'GREATER_THAN_OR_EQUAL_TO',
              dimensionName: item.dimensionName,
              dimensionValue: Number(item.dimensionValue.split(', ')[0])
            };
            var obj2 = {
              operator: 'LESS_THAN_OR_EQUAL_TO',
              dimensionName: item.dimensionName,
              dimensionValue: Number(item.dimensionValue.split(', ')[1])
            };
            oReturn.dimensionNameValueList.push(obj1, obj2);
          } else {
            oReturn.dimensionNameValueList.push(item);
          }
        }
      }
    }

    return oReturn;
  },

  getCellRenderParams(data) {
    var type = data.uiField.uiType.toLowerCase();
    var objToReturn = {
      formatterFn: formatter
    };

    switch (type) {
      case 'percent':
        objToReturn.keyType = 'PERCENTAGE';
        break;

      case 'currency':
        objToReturn.keyType = 'CURRENCY';
        break;

      case 'numeric':
        // If isFormattingRequired is not passed by the backend we assumne isFormattingRequired to be true
        var formattingRequired = data.uiField.metadata.isFormattingRequired === undefined ? true : data.uiField.metadata.isFormattingRequired; // If formatting is not required we are removing the formatterFn Field from the object

        if (formattingRequired === false) {
          delete objToReturn.formatterFn;
        }

        objToReturn.keyType = 'numeric';
        break;

      case 'string':
        objToReturn = {
          keyType: 'string'
        };
        break;

      default:
    }

    return objToReturn;
  },

  getCustomCellRender(data) {
    var colMetaData = data.uiField.metadata;
    var type = colMetaData.widget.toLowerCase();
    var objToReturn = {};

    switch (type) {
      case 'hyperlink':
        objToReturn.cellRendererFramework = 'linkDisplay';
        objToReturn.cellRendererParams = {
          url: colMetaData.urlTableColumnName
        };
        break;

      case 'progress':
        objToReturn.cellRendererFramework = 'progressDisplay';
        objToReturn.cellRendererParams = {
          fill: colMetaData.percentTableColumnName,
          decimalRoundOff: colMetaData.decimalRoundOff
        };
        break;

      case 'metric':
        objToReturn.cellRendererFramework = 'metricDisplay';
        objToReturn.minWidth = 150;
        objToReturn.cellRendererParams = {
          tag1Key: colMetaData.primaryTableColumnName,
          tag2Key: colMetaData.secondaryTableColumnName,
          tag1Unit: this.getTagUnitData(colMetaData.primaryIsPrefix, colMetaData.primaryUnit),
          tag2Unit: this.getTagUnitData(colMetaData.secondaryIsPrefix, colMetaData.secondaryUnit)
        };
        break;

      case 'icon':
        objToReturn.cellRendererFramework = 'iconTableCell';
        objToReturn.headerComponentFramework = 'iconTableHeader';
        objToReturn.headerComponentParams = {};
        objToReturn.headerComponentParams.displayIcon = colMetaData.displayIcon;
        objToReturn.cellRendererParams = {
          iconSize: !colMetaData.iconSize ? 'medium' : colMetaData.iconSize,
          iconClickEvent: colMetaData.iconClickEvent,
          displayIcon: colMetaData.displayIcon,
          toolTipText: colMetaData.toolTipText,
          toolTipPosition: colMetaData.toolTipPosition,
          contextReturnEvent: colMetaData.contextReturnEvent
        };
        objToReturn.cellRendererParams.type = !colMetaData.type ? 'icon' : colMetaData.type;

        if (colMetaData.type === 'iconText') {
          objToReturn.cellRendererParams.formatType = colMetaData.formatType;
        }

        break;

      case 'input':
        objToReturn.cellRendererFramework = 'inputTypeCell';
        objToReturn.cellRendererParams = {
          type: !colMetaData.type ? 'text' : colMetaData.type,
          blurEvent: colMetaData.blurEvent,
          onchangeEvent: colMetaData.onchangeEvent,
          keyupEvent: colMetaData.keyupEvent,
          defaultValueColumnName: colMetaData.defaultValueColumnName,
          formatType: colMetaData.formatType,
          contextReturnEvent: colMetaData.contextReturnEvent
        };
        break;

      default:
    }

    return objToReturn;
  },

  getTagUnitData(isPrefix, unit) {
    if (isPrefix) {
      return {
        pre: unit
      };
    } else {
      return {
        suff: unit
      };
    }
  },

  getColumnDefinition(columns, customData, customObject) {
    var colDefinitionToReturn = [];
    var columnArray = columns;

    for (var i = 0; i < columnArray.length; i++) {
      var currDefinition = columnArray[i];
      var obj = {};
      obj.showOnUi = currDefinition.uiField.metadata.showOnUi;
      obj.isDownloadable = currDefinition.uiField.metadata.isDownloadable;
      obj.headerComponentFramework = 'genericTableHeader';
      obj.title = currDefinition.uiField.uiLabel;
      obj.headerName = currDefinition.uiField.uiLabel;
      obj.field = currDefinition.uiField.metadata.tableColumnName === undefined ? currDefinition.name : currDefinition.uiField.metadata.tableColumnName;
      obj.cellRendererFramework = 'genericTableCell';
      obj.cellRendererParams = this.getCellRenderParams(currDefinition);

      if (currDefinition.uiField.uiType.toLowerCase() === 'string') {
        if (currDefinition.uiField.metadata.width !== undefined) {
          obj.minWidth = currDefinition.uiField.metadata.width;
          obj.width = currDefinition.uiField.metadata.width;
        } else {
          obj.minWidth = 180;
        }
      } else {
        obj.minWidth = 120;
      } // obj.minWidth = currDefinition.uiField.uiType.toLowerCase() === 'string' ? 180 : 120;


      if (currDefinition.uiField.uiType === 'custom') {
        obj.minWidth = currDefinition.uiField.metadata.width;

        if (customData !== undefined && typeof customData === 'object' && Object.keys(customData).length > 0 && customData[currDefinition.name]) {
          obj.cellRendererFramework = customData[currDefinition.name].component;
          obj.cellRendererParams = (customData[currDefinition.name] || {}).params || null;
          obj.minWidth = 200;
        } else {
          if (currDefinition.uiField.metadata.widget === 'progress') {
            obj.field = currDefinition.uiField.metadata.percentTableColumnName;
          } // if (currDefinition.uiField.metadata.widget === 'icon') {
          // obj.cellRendererFramework = 'iconTableCell';
          // obj.headerComponentFramework = 'iconTableHeader';
          // // obj.cellRendererParams.iconClickEvent = vueRef.openSidePanel;
          // obj.cellRendererParams.displayIcon = 'timeline';
          // // obj.cellRendererParams.toolTipText = dictionary.map[i].toolTipText;
          // obj.notDownloadable = true;
          // obj.headerComponentParams.displayIcon = 'timeline';
          // obj.minWidth = 60;
          // }


          var cellObj = this.getCustomCellRender(currDefinition);
          obj.cellRendererFramework = cellObj.cellRendererFramework;
          obj.cellRendererParams = cellObj.cellRendererParams;

          if (cellObj.minWidth) {
            obj.minWidth = cellObj.minWidth;
          }

          if (customObject !== undefined && customObject.hasOwnProperty(currDefinition.uiField.metadata.tableColumnName)) {
            var columnToRead = currDefinition.uiField.metadata.tableColumnName;

            for (var k in customObject[columnToRead]) {
              obj.cellRendererParams[k] = customObject[columnToRead][k];
            }
          }
        }
      }

      obj.keyOrder = currDefinition.uiField.uiOrder;
      obj.pinned = currDefinition.uiField.metadata.isFixed;
      colDefinitionToReturn[obj.keyOrder] = obj;
      obj.headerComponentParams = {
        enableSorting: currDefinition.uiField.metadata.sortOnColumn === undefined ? false : currDefinition.uiField.metadata.sortOnColumn,
        keyType: obj.cellRendererParams.keyType,
        toolTipText: currDefinition.uiField.uiTooltip
      };

      if (currDefinition.uiField.metadata.isDefaultSortColumn) {
        obj.headerComponentParams.sort = currDefinition.uiField.metadata.sortDirection; // 'asc'
      }
    }

    var displayColConfigs = colDefinitionToReturn.filter(elm => {
      return elm.showOnUi === true;
    });
    var downloadColConfigs = colDefinitionToReturn.filter(elm => {
      return elm.isDownloadable === true;
    });
    var objToReturn = {
      displayColConfigs: displayColConfigs,
      downloadColConfigs: downloadColConfigs
    };
    return objToReturn;
  },

  getTableDataFromFullResponse: function getTableDataFromFullResponse(apiResponse) {
    var response = apiResponse.fullResponse;
    var columns = response.metadata;
    var measureList = columns.measureList;
    var groupByDimensionList = columns.groupByDimensionList;
    measureList = measureList.concat(groupByDimensionList);
    var rows = this.mergeResultDimension(response.data);
    var obj = {
      rows: rows,
      columns: measureList
    };
    return obj;
  },

  generateWhereClause(where, filters) {
    var values = filters.values;

    for (var key in values) {
      if (values.hasOwnProperty(key)) {
        if (values[key].constructor === Array && key !== 'date_range') {
          // for filters
          for (var i = 0; i < values[key].length; i++) {
            var temp = values[key];

            if (!where['dimensionNameValueList']) {
              where['dimensionNameValueList'] = [];
            }

            if (temp[i].operator) {
              where['dimensionNameValueList'].push({
                'dimensionName': key,
                'dimensionValue': temp[i].value,
                'operator': temp[i].operator.operator
              });
            } else {
              where['dimensionNameValueList'].push({
                'dimensionName': key,
                'dimensionValue': temp[i]
              });
            }
          }
        } else if (key === 'date_range') {
          // for date range
          where['date'] = {
            from: values[key].from,
            to: values[key].to
          };
        }
      }
    }

    return where;
  },

  convertDimensionsToFilterFormat(dimensions) {
    var _this = this;

    var filter = {
      order: [],
      values: {}
    };

    if (!dimensions) {
      return filter;
    }

    console.log(operator);

    var _loop = function _loop(i) {
      var dimensionName = dimensions[i].dimensionName;
      var orderIndex = filter.order.findIndex(element => {
        return element === dimensionName;
      });

      if (orderIndex === -1) {
        filter.order.push(dimensionName);
      }

      if (!filter.values[dimensionName]) {
        filter.values[dimensionName] = [];
        filter.values[dimensionName].push(_this.formatToFilterFormat(dimensions[i]));
      } else {
        filter.values[dimensionName].push(_this.formatToFilterFormat(dimensions[i]));
      }
    };

    for (var i = 0; i < dimensions.length; i++) {
      _loop(i);
    }

    return filter;
  },

  formatToFilterFormat(dimension) {
    if (dimension.type === 'EXPRESSION') {
      var obj = {
        operator: {
          operator: dimension.operator,
          title: operator[dimension.operator]
        },
        unit: null,
        value: dimension.dimensionValue
      };
      return obj;
    } else {
      return dimension.dimensionValue;
    }
  }

});
// CONCATENATED MODULE: ./src/utils/services/eventBus.js

var eventBus = new vue_esm["a" /* default */]();
// CONCATENATED MODULE: ./src/components/widgetMixin.js






function fetch(config) {}

function readFromLocalStorage(lsKey, storeSetter) {
  var _selectedFilters = localStorage.getItem(lsKey) || '{}';

  _selectedFilters = JSON.parse(_selectedFilters);

  for (var i in _selectedFilters) {
    this.$store.dispatch(storeSetter, {
      filterValueKey: i,
      values: _selectedFilters[i]
    });
  }
}

var oObject = {
  beforeCreate() {
    this.readFromLocalStorage = readFromLocalStorage.bind(this);
  },

  created() {
    if (this.config) {
      for (var key in this.config.widgets) {
        if (this.config.widgets[key].meta && this.config.widgets[key].meta.localFilters) {
          this.config.widgets[key].meta.localFilters = this.config.widgets[key].meta.localFilters.filter(obj => {
            return !(obj.dimensionName === 'search');
          });
        }
      }

      fetch.call(this, this.config);
    }

    if (this.config && this.config.filters && this.config.filters.listen) {
      for (var i in this.config.filters.listen) {
        eventBus.$on(i, function (data, mapping) {
          if (mapping) {
            this.config.mapping = mapping;

            if (this.config.filters.listen[i].transform) {
              this.config = this.config.filters.listen[i].transform(this.config, data, this);
            }

            this.$store.dispatch(this.config.filters.listen[i].action, this.config);
          }
        }.bind(this));
      }
    }
  },

  computed: {
    getMarketPlace() {
      return this.$store.getters.getMarketPlace;
    },

    outsideIn() {
      return this.$store.getters.getOutsideIn;
    },

    getColorPattern() {
      return ['#ffa800', '#bd10e0', '#ff6072', '#97cc04', '#23b5d3', '#f5d908', '#ff909d', '#ffc24c', '#d158ea', '#f8e552', '#b6dc4f', '#65cce1'];
    }

  },

  data() {
    return {
      filterData: [],
      primaryFilterData: [],
      secondaryFilterData: {}
    };
  },

  methods: {
    fetchFilters(cube, endPoint, page, where) {
      var that = this;
      return http_layer.post({
        cube: endPoint || 'FETCH_FILTERS',
        APIData: {
          cubeName: cube,
          pageName: page,
          where: where
        }
      }).then(response => {
        var data = data_transformer.mergeResultDimension(response.data);
        that.filterData = data; // that.primaryFilterData = ((response && response.fullResponse && response.fullResponse.metadata || {}).dimensionMappingData || [])
      });
    },

    getComputedFn(widget) {
      return this._computedWatchers[widget.dataSource].getter();
    },

    applyFilter() {
      if (this.config) {
        fetch.call(this, this.config);
      }
    },

    getAllDates() {
      var maxDates = this.$store.getters.getMaxDate;
      var returnDates = {};
      var dateRangeValues = {};
      var selectedDateRange = this.$store.getters[this.filterState.getter]['date_range'].name;
      var selectedDateRangeValues = this.$store.getters.getDateRangeValues;

      if (selectedDateRange) {
        for (var i in selectedDateRangeValues) {
          dateRangeValues[i] = selectedDateRangeValues[i][selectedDateRange];
        }

        for (i in maxDates) {
          returnDates[i] = maxDates[i].max_feed_date;
        }
      }

      returnDates['dateRange'] = selectedDateRange;
      returnDates['dateRangeValues'] = dateRangeValues;
      return returnDates;
    }

  }
};
/* harmony default export */ var widgetMixin = __webpack_exports__["a"] = (oObject);

/***/ }),

/***/ "8110":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"a784782c-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/basic/chart-legends.vue?vue&type=template&id=0a675628&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"u-width-100"},[_c('div',{staticClass:"u-display-flex u-flex-justify-content-space-between u-flex-align-items-flex-start u-flex-wrap-yes"},[_c('div',{staticClass:"u-display-flex u-flex-wrap-yes"},[_vm._l((_vm.localMetricDisplayList),function(val,index){return _c('div',{key:index,staticClass:"metric-card-hover-trigger",attrs:{"val":val},on:{"click":function($event){$event.stopPropagation();return _vm.selectNewMetric(index, val)}}},[_c('div',{staticClass:"custom-chart-legend u-display-inline-flex u-flex-direction-column u-position-relative u-cursor-pointer workbench-hover-card"},[(_vm.computedSelectedMetric[val.key])?_c('div',{staticClass:"active-metric-card",style:({ 'background-color': _vm.metricColors[val.key] })}):_c('div',{staticClass:"metric-card-hover",style:(_vm.metricColors[val.key] ? { 'background-color': 'transparent' } :{ 'background-color': _vm.getColorPattern[index]})}),(index > _vm.minimumMetric - 1)?_c('div',{staticClass:"cross-button-holder"},[_c('div',{staticClass:"cross-icon",on:{"click":function($event){$event.stopPropagation();return _vm.deleteMetric(index)}}},[_c('rb-icon',{staticClass:"rb-icon--xx-small u-cursor-pointer u-color-grey-lighter",attrs:{"icon":'cross'}})],1)]):_vm._e(),_c('div',{staticClass:"u-spacing-ph-m u-spacing-pb-m",class:[index > (_vm.minimumMetric -1) ? '': 'u-spacing-pt-m']},[_c('div',[(val)?_c('rb-select',{staticClass:"u-spacing-mr-m",attrs:{"width":'240px',"context":[index],"sendDetails":true,"onClose":_vm.metricSelected,"options":_vm.metricsList,"className":'campaigns-select'},scopedSlots:_vm._u([{key:"item",fn:function(option){return [_c('div',{staticClass:"u-display-flex u-flex-align-items-center"},[_c('div',{staticClass:"u-overflow-hidden u-flex-direction-row u-display-flex u-flex-align-items-center u-width-100 u-spacing-pt-s u-spacing-pb-s u-spacing-pr-m u-spacing-pl-m u-flex-justify-content-space-between"},[_c('p',{staticClass:"u-color-grey-base u-text-overflow-ellipsis u-font-size-5 u-text-case-title"},[_vm._v(_vm._s(option.title))])])])]}}],null,true)},[_c('div',{staticClass:"u-display-flex u-flex-align-items-center u-cursor-pointer",attrs:{"slot":"trigger"},slot:"trigger"},[_c('div',{staticClass:"u-display-flex u-flex-align-items-center shareCompBrandsSelectTrigger"},[_c('span',{staticClass:"u-font-size-5"},[_vm._v(_vm._s(val.key))]),_c('rb-icon',{staticClass:"rb-icon--small u-spacing-ml-xs u-color-grey-lighter",attrs:{"icon":'caret-down'}})],1)])]):_vm._e()],1),(_vm.hasPVP && (_vm.metricData || {})[val.key])?_c('metric',{staticClass:"u-display-inline-flex u-spacing-mt-s",attrs:{"size":'l',"config":(_vm.metricConfig || {})[val.key],"data":(_vm.metricData || {})[val.key]}}):_vm._e(),(!val && !(((_vm.metricData || {})[val.key])))?_c('span',{staticClass:"u-color-grey-light u-font-size-5"},[_vm._v("No Data")]):_vm._e()],1)])])}),_c('div',[_c('rb-select',{staticClass:"u-spacing-mr-m",attrs:{"width":'240px',"sendDetails":true,"onClose":_vm.addNewMetric,"options":_vm.metricsList,"className":'campaigns-select u-height-100 select-trigger-height-100'},scopedSlots:_vm._u([{key:"item",fn:function(option){return [_c('div',{staticClass:"u-display-flex u-flex-align-items-center"},[_c('div',{staticClass:"u-overflow-hidden u-flex-direction-row u-display-flex u-flex-align-items-center u-width-100 u-spacing-pt-s u-spacing-pb-s u-spacing-pr-m u-spacing-pl-m u-flex-justify-content-space-between"},[_c('p',{staticClass:"u-color-grey-base u-text-overflow-ellipsis u-font-size-5 u-text-case-title"},[_vm._v(_vm._s(option.title))])])])]}}])},[_c('div',{staticClass:"u-display-flex u-flex-align-items-center u-cursor-pointer u-height-100",attrs:{"slot":"trigger"},slot:"trigger"},[_c('div',{staticClass:"u-display-flex u-flex-align-items-center shareCompBrandsSelectTrigger u-height-100"},[_c('div',{staticClass:"metric-card-hover-trigger u-height-100"},[_c('div',{staticClass:"custom-chart-legend u-display-inline-flex u-flex-direction-column u-position-relative u-cursor-pointer workbench-hover-card u-height-100"},[_c('div',{staticClass:"u-spacing-p-m u-display-flex u-flex-align-items-center u-flex-justify-content-center u-height-100"},[_c('span',{staticClass:"u-display-flex u-font-size-5 u-color-grey-lighter"},[_c('rb-icon',{staticClass:"rb-icon--small u-cursor-pointer u-color-grey-lighter u-spacing-mr-xs",attrs:{"icon":'add-circle-fill'}}),_vm._v("Add metric ")],1)])])])])])])],1)],2)])])}
var staticRenderFns = []


// CONCATENATED MODULE: ./src/components/basic/chart-legends.vue?vue&type=template&id=0a675628&

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.find-index.js
var es_array_find_index = __webpack_require__("c740");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.iterator.js
var es_array_iterator = __webpack_require__("e260");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.splice.js
var es_array_splice = __webpack_require__("a434");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.number.constructor.js
var es_number_constructor = __webpack_require__("a9e3");

// EXTERNAL MODULE: ./node_modules/core-js/modules/web.dom-collections.iterator.js
var web_dom_collections_iterator = __webpack_require__("ddb0");

// EXTERNAL MODULE: ./src/components/widgetMixin.js + 7 modules
var widgetMixin = __webpack_require__("6b5d");

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/basic/chart-legends.vue?vue&type=script&lang=js&





//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
// import chart from './chart.vue'
 // import metric from '@/components/basic/metric'

/* harmony default export */ var chart_legendsvue_type_script_lang_js_ = ({
  components: {// chart,
    // metric
  },
  props: {
    metricDisplayList: {
      type: Array,
      default: () => []
    },
    metricsList: {
      type: Array,
      default: () => []
    },
    defaultSelectMetric: {
      type: Array,
      default: () => []
    },
    selectedMetricLimit: {
      type: Number,
      default: 2
    },
    metricConfig: {
      type: Object,
      default: () => {}
    },
    metricData: {
      type: Object,
      default: () => {}
    },
    minimumMetric: {
      type: Number,
      default: 2
    },
    hasPVP: {
      type: Boolean,
      default: true
    }
  },

  data() {
    return {
      metricsSelectedIndex: [0, 1],
      chartWkbenchAxes: {},
      selectedMetric: [],
      localMetricDisplayList: []
    };
  },

  created() {
    this.selectedMetric = this.defaultSelectMetric;
    this.localMetricDisplayList = JSON.parse(JSON.stringify(this.metricDisplayList));
    this.emitEventOut();
  },

  watch: {
    defaultSelectMetric(newValue) {
      this.selectedMetric = newValue;
    },

    metricDisplayList(newValue) {
      this.localMetricDisplayList = newValue;
    }

  },
  computed: {
    computedSelectedMetric() {
      var returnData = {};

      for (var i = 0; i < this.selectedMetric.length; i++) {
        returnData[this.selectedMetric[i].key] = this.selectedMetric[i];
      }

      return returnData;
    },

    metricColors() {
      var _colors = {};
      var colors = this.getColorPattern;

      for (var i = 0; i < this.localMetricDisplayList.length; i++) {
        _colors[this.localMetricDisplayList[i].key] = colors[i];
      }

      return _colors;
    }

  },
  methods: {
    // On Select of New Metric
    selectNewMetric(context, val) {
      var indexValue = this.selectedMetric.findIndex(item => {
        return item.key === val.key;
      });

      if (indexValue === -1) {
        this.smartPushQueue(val);
        this.chartWkbenchAxes[this.localMetricDisplayList[context].key] = context % 2 === 0 ? 'y' : 'y2';
      }
    },

    // On Select of new Metric of a card through drop down
    metricSelected(context, val) {
      var selectedIndex = val[0].selectedIndex;

      if (this.selectedMetric.findIndex(item => {
        return item.key === this.metricsList[selectedIndex].key;
      }) === -1) {
        this.localMetricDisplayList[context[0]] = this.metricsList && this.metricsList[selectedIndex];
        this.chartWkbenchAxes[this.metricsList[selectedIndex].key] = context % 2 === 0 ? 'y' : 'y2';
        this.localMetricDisplayList = [...this.localMetricDisplayList];
      }

      this.queueReplace(this.localMetricDisplayList[context[0]], this.metricsList[selectedIndex]);
    },

    // Pushing into the queue and queue is full we removed the last element (First in last out idea)
    smartPushQueue(value) {
      if (this.selectedMetric.length >= this.selectedMetricLimit) {
        this.selectedMetric.shift();
      }

      this.selectedMetric.push(value);
      this.emitEventOut();
    },

    // Searching for the item in the queue and replacing it with newValue
    queueReplace(oldValue, newValue) {
      var indexLocation = this.selectedMetric.findIndex(item => {
        return item.key === oldValue.key;
      });

      if (indexLocation === -1) {
        this.smartPushQueue(newValue);
      } else {
        this.selectedMetric[indexLocation] = newValue;
      }

      this.emitEventOut();
    },

    // Removing the element from the queue if found in the array
    queueDelete(value) {
      var valueIndex = this.selectedMetric.findIndex(item => {
        return item.key === value.key;
      });

      if (valueIndex !== -1) {
        this.selectedMetric.splice(valueIndex, 1);
      }

      this.emitEventOut();
    },

    // Just emitting the event
    emitEventOut() {
      var obj = {
        selectedMetric: this.selectedMetric,
        metricColors: this.metricColors
      };
      this.$emit('selectedList', obj);
    },

    // Adding new metric and pushing into the queue
    addNewMetric(context, val) {
      var selectedIndex = val[0].selectedIndex;
      this.localMetricDisplayList.push(this.metricsList[selectedIndex]);
      this.smartPushQueue(this.metricsList[selectedIndex]);
    },

    // Deleting the metric and deleting from the queue
    deleteMetric(index) {
      this.queueDelete(this.localMetricDisplayList[index]);
      this.localMetricDisplayList.splice(index, 1);
    }

  },
  mixins: [widgetMixin["a" /* default */]]
});
// CONCATENATED MODULE: ./src/components/basic/chart-legends.vue?vue&type=script&lang=js&
 /* harmony default export */ var basic_chart_legendsvue_type_script_lang_js_ = (chart_legendsvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__("2877");

// CONCATENATED MODULE: ./src/components/basic/chart-legends.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__("2c33")
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  basic_chart_legendsvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  null
  ,true
)

/* harmony default export */ var chart_legends = __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ "8b3e":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"a784782c-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/basic/chart.vue?vue&type=template&id=6f9b89f5&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{class:{'u-spacing-mb-l': _vm.config.chartOptions.legend != false}},[(_vm.config.chartOptions.type === 'line')?_c('div',{staticClass:"c3-line-chart"}):_vm._e(),(_vm.config.chartOptions.type === 'donut')?_c('div',{staticClass:"c3-donut-chart"}):_vm._e()])}
var staticRenderFns = []


// CONCATENATED MODULE: ./src/components/basic/chart.vue?vue&type=template&id=6f9b89f5&

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.object.assign.js
var es_object_assign = __webpack_require__("cca6");

// EXTERNAL MODULE: ./src/utils/mixins/boomerangChartMixin.js + 1 modules
var boomerangChartMixin = __webpack_require__("b32b");

// EXTERNAL MODULE: ./node_modules/c3/c3.js
var c3 = __webpack_require__("2d5f");
var c3_default = /*#__PURE__*/__webpack_require__.n(c3);

// EXTERNAL MODULE: ./node_modules/vue/dist/vue.esm.js
var vue_esm = __webpack_require__("a026");

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/basic/chart.vue?vue&type=script&lang=js&

//
//
//
//
//
//
//



/* harmony default export */ var chartvue_type_script_lang_js_ = ({
  mixins: [boomerangChartMixin["a" /* default */]],

  mounted() {
    c3_default.a.generate(this.chartConfig);
  },

  watch: {
    data: function data(newData) {
      console.log(newData);

      if (newData) {
        if (newData.constructor === Object) {
          if (newData.legends) {
            this.customLegends = newData.legends;
          }

          if (newData.xs) {
            delete this.chartConfig.data.x;
            this.chartConfig.data.xs = newData.xs;
          }

          if (newData.groups) {
            this.chartConfig.data.groups = newData.groups;
          }

          if (newData.colors) {
            this.chartConfig.data.colors = newData.colors;
          }

          if (newData.axes) {
            this.chartConfig.data.axes = newData.axes;
          }

          if (newData.types) {
            this.chartConfig.data.types = newData.types;
          } // Bar Configuration, To change width of the bar.


          if (newData.bar) {
            this.chartConfig.bar = newData.bar;
          }

          if (newData.axis_format && newData.axis_format.y) {
            this.chartConfig.axis.y = Object.assign(this.chartConfig.axis.y, newData.axis_format.y);

            this.chartConfig.axis.y.tick.format = value => {
              return vue_esm["a" /* default */].options.filters.num_format(value, newData.axis_format.y.pre, newData.axis_format.y.suff, newData.axis_format.y.min, newData.axis_format.y.roundoff);
            };
          }

          if (newData.axis_format && newData.axis_format.y2) {
            this.chartConfig.axis.y2 = Object.assign(this.chartConfig.axis.y2, newData.axis_format.y2);

            this.chartConfig.axis.y2.tick.format = value => {
              return vue_esm["a" /* default */].options.filters.num_format(value, newData.axis_format.y2.pre, newData.axis_format.y2.suff, newData.axis_format.y2.min, newData.axis_format.y2.roundoff);
            };
          }

          this.chartConfig.data.columns = newData.data || [];
          this.chartConfig.data.classes = newData.classes || [];
          c3_default.a.generate(this.chartConfig);
        } else {
          this.chartConfig.data.columns = newData || [];
          c3_default.a.generate(this.chartConfig);
        }
      }
    }
  }
});
// CONCATENATED MODULE: ./src/components/basic/chart.vue?vue&type=script&lang=js&
 /* harmony default export */ var basic_chartvue_type_script_lang_js_ = (chartvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__("2877");

// CONCATENATED MODULE: ./src/components/basic/chart.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  basic_chartvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  ,true
)

/* harmony default export */ var chart = __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ "b32b":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.concat.js
var es_array_concat = __webpack_require__("99af");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.find-index.js
var es_array_find_index = __webpack_require__("c740");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.index-of.js
var es_array_index_of = __webpack_require__("c975");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.function.name.js
var es_function_name = __webpack_require__("b0c0");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.number.to-fixed.js
var es_number_to_fixed = __webpack_require__("b680");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.object.to-string.js
var es_object_to_string = __webpack_require__("d3b7");

// EXTERNAL MODULE: ./node_modules/core-js/modules/web.dom-collections.for-each.js
var web_dom_collections_for_each = __webpack_require__("159b");

// EXTERNAL MODULE: ./node_modules/vue/dist/vue.esm.js
var vue_esm = __webpack_require__("a026");

// CONCATENATED MODULE: ./src/utils/helpers/chartSvg.js
var base = '/static/chartIcons/';
/* harmony default export */ var chartSvg = ({
  '3P-variant': {
    src: base + '3P-variant.svg'
  },
  'add-on': {
    src: base + 'add-on.svg'
  },
  'amazon-choice': {
    src: base + 'amazon-choice.svg'
  },
  'best-seller': {
    src: base + 'best-seller.svg'
  },
  'campaign': {
    src: base + 'campaign.svg'
  },
  'content-change': {
    src: base + 'content-change.svg'
  },
  'OOS': {
    src: base + 'OOS.svg'
  },
  'search-event': {
    src: base + 'search-event.svg'
  },
  'SnS': {
    src: base + 'SnS.svg'
  },
  'suppressed': {
    src: base + 'suppressed.svg'
  },
  'unavailable': {
    src: base + 'unavailable.svg'
  }
});
// CONCATENATED MODULE: ./src/utils/mixins/boomerangChartMixin.js










function isFunction(functionToCheck) {
  return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}

/* harmony default export */ var boomerangChartMixin = __webpack_exports__["a"] = ({
  props: {
    config: Object,
    data: [Object, Array, String]
  },

  created() {
    this.config.xAxisType = this.config.xAxisType || 'category';
    this.config.stack = this.config.stack || [];
    this.config.chartGetter = this.config.chartGetter || '';
    this.chartConfig.data.type = this.config.chartOptions.type || this.config.type || 'line';

    if (this.config.chartOptions.types) {
      this.chartConfig.data.types = this.config.chartOptions.types;
    }

    this.chartConfig.axis.x.type = this.config.xAxisType;

    if (this.config.axis) {
      this.chartConfig.axis.rotated = this.config.axis.rotated || false;
    }

    if (this.chartConfig.data.type === 'donut') {
      this.chartConfig.donut = this.config.donut || undefined;
    }

    if (this.chartConfig.data.type === 'bar') {
      // this.chartConfig.data.groups = this.config.groups;
      this.chartConfig.data.x = 'x';

      if (this.config.bar !== undefined && this.config.bar.width !== undefined && this.config.bar.width.ratio !== undefined) {
        this.chartConfig.bar = this.config.bar;
      }

      this.chartConfig.data.x = 'x';
      this.chartConfig.axis.x = {
        show: true,
        type: 'category'
      };

      if (this.chartConfig.xDataKey !== undefined) {
        this.chartConfig.data.x = this.chartConfig.xDataKey;
      }
    }

    if (this.config.chartOptions && this.config.chartOptions.timeseries !== undefined) {
      // -----------------------------------------------------------------------------------------
      //  /_\  Please inform Subhash about changes in this part of code so that recommendations
      //       screens can be adapted.
      // -----------------------------------------------------------------------------------------
      if (typeof this.config.chartOptions.timeseries === 'object') {
        this.chartConfig.data.xs = this.config.chartOptions.timeseries;
      } else {
        this.chartConfig.data.x = this.config.chartOptions.timeseries;
      }

      this.chartConfig.axis.x = {
        show: true,
        type: 'timeseries',
        tick: {
          format: '%e %b',
          fit: true,
          outer: false,
          count: this.config.chartOptions.chartWidth === 's' ? 5 : undefined
        }
      };

      if (this.config.chartOptions.axis_format && this.config.chartOptions.axis_format.x) {
        this.chartConfig.axis.x.tick.format = this.config.chartOptions.axis_format.x;
      }

      this.chartConfig.axis.y = {
        show: false,
        outer: false,
        tick: {
          format: function format(a) {
            return vue_esm["a" /* default */].prototype.$currency + a.toFixed(2);
          }
        }
      };

      if (this.config.chartOptions.axis_format && this.config.chartOptions.axis_format.y) {
        this.chartConfig.axis.y.show = true;

        this.chartConfig.axis.y.tick.format = value => {
          return vue_esm["a" /* default */].options.filters.num_format(value, this.config.chartOptions.axis_format.y.pre, this.config.chartOptions.axis_format.y.suff, this.config.chartOptions.axis_format.y.min, this.config.chartOptions.axis_format.y.roundoff);
        };

        if (this.config.chartOptions.axis_format.y.inverted) {
          this.chartConfig.axis.y.inverted = true;
        }

        if (this.config.chartOptions.axis_format.y.min) {
          this.chartConfig.axis.y.min = this.config.chartOptions.axis_format.y.min;
        }
      }

      this.chartConfig.axis.y2 = {
        show: false,
        tick: {
          outer: false,
          format: function format(a) {
            return vue_esm["a" /* default */].prototype.$currency + a.toFixed(2);
          }
        }
      };

      if (this.config.chartOptions.axis_format && this.config.chartOptions.axis_format.y2) {
        this.chartConfig.axis.y2.show = true;

        this.chartConfig.axis.y2.tick.format = value => {
          return vue_esm["a" /* default */].options.filters.num_format(value, this.config.chartOptions.axis_format.y2.pre, this.config.chartOptions.axis_format.y2.suff, this.config.chartOptions.axis_format.y2.min, this.config.chartOptions.axis_format.y2.roundoff);
        };

        if (this.config.chartOptions.axis_format.y2.inverted) {
          this.chartConfig.axis.y2.inverted = true;
        }

        if (this.config.chartOptions.axis_format.y2.min) {
          this.chartConfig.axis.y2.min = this.config.chartOptions.axis_format.y2.min;
        }
      }

      this.chartConfig.grid = {
        y: {
          show: false
        },
        x: {
          show: false
        }
      };

      if (this.config.chartOptions.grid && this.config.chartOptions.grid.constructor === String) {
        if (this.config.chartOptions.grid.indexOf('x') !== -1) {
          this.chartConfig.grid.x.show = true;
        }

        if (this.config.chartOptions.grid.indexOf('y') !== -1) {
          this.chartConfig.grid.y.show = true;
        }
      } else if (this.config.chartOptions.grid && this.config.chartOptions.grid.constructor === Object) {
        this.chartConfig.grid = this.config.chartOptions.grid;
      }
    } else if (this.config.xAxisType === 'category') {
      this.chartConfig.axis.x.categories = [];
    }

    if (this.config.chartOptions.padding) {
      for (var i in this.config.chartOptions.padding) {
        this.chartConfig.padding[i] = this.config.chartOptions.padding[i];
      }
    }

    if (this.config.chartOptions.axes) {
      this.chartConfig.data.axes = this.config.chartOptions.axes;

      if (this.config.chartOptions.hideY2 === true) {
        this.chartConfig.axis.y2 = {
          show: false
        };
      }
    }

    if (this.config.chartOptions.xFormat) {
      this.chartConfig.data.xFormat = this.config.chartOptions.xFormat;
    }

    if (this.config.chartOptions.tooltip) {
      this.chartConfig.tooltip = this.config.chartOptions.tooltip;
    }

    if (this.config.chartOptions.regions) {
      this.chartConfig.data.regions = this.config.chartOptions.regions;
    }

    if (this.config.chartOptions.events) {
      this.chartConfig.events = this.config.chartOptions.events;
    }

    if (this.config.regions) {
      this.chartConfig.regions = this.config.regions;
    }
  },

  mounted() {
    this.chartConfig.data.columns = this.data || [];
    this.chartConfig.bindto = this.$el;

    if (this.chartConfig.grid !== undefined && (this.chartConfig.grid.x.show || this.chartConfig.grid.y.show)) {
      this.$el.classList.add('graphWithGrids');
    }
  },

  data() {
    var chartWidget = this;
    var chartOptions = this.config.chartOptions;
    var localChartSvg = chartSvg;
    return {
      customLegends: null,
      chartInstance: null,
      chartConfig: {
        line: {
          connectNull: true
        },
        axis: {
          x: {
            show: false
          },
          y: {
            show: false
          },
          y2: {
            min: 0,
            show: false
          }
        },
        point: {
          r: function (data) {
            var pointFormat = ((this.config.chartOptions || {}).point_format || {})[data.id] || {};

            if ((this.config.chartOptions.events || []).length > 0) {
              var eventsKey = this.config.chartOptions.events.findIndex(item => {
                return item.key === data.id;
              });

              if (eventsKey !== -1) {
                return 8;
              }
            }

            if (isFunction(pointFormat)) {
              return pointFormat(data);
            }

            if ((data.id.toLowerCase().indexOf('promo') > -1 || data.id.toLowerCase().indexOf('timeline') > -1) && data.id.toLowerCase().indexOf('promotions') === -1) {
              return 5;
            }

            return 3;
          }.bind(this)
        },
        data: {
          columns: []
        },
        color: {
          pattern: ['#ffa800', '#bd10e0', '#ff6072', '#97cc04', '#23b5d3', '#f5d908', '#ff909d', '#ffc24c', '#d158ea', '#f8e552', '#b6dc4f', '#65cce1']
        },
        oninit: function oninit() {
          var legendItems = [];
          this.chartOptions = chartOptions;
          this.localChartSvg = localChartSvg;
          var element = this.config.bindto;

          if ((this.chartOptions.events || []).length > 0) {
            var svgElement = this.d3.select(element).select('svg');

            for (var _i = 0; _i < this.chartOptions.events.length; _i++) {
              var event = this.chartOptions.events[_i];
              var url = this.localChartSvg[event.icon].src;
              svgElement.append('filter').attr('id', event.key).attr('width', '100%').attr('height', '100%').append('feImage').attr('xlink:href', url);
            }
          }

          if (this.data.targets && this.data.targets.length > 0 && chartOptions.legend !== false) {
            for (var i = 0; i < this.data.targets.length; i++) {
              legendItems.push(this.data.targets[i]['id']);
            }

            var that = this;
            this.d3.select(element).insert('div', '.chart').attr('class', 'u-display-flex u-flex-justify-content-center u-spacing-mb-m').selectAll('span').data(legendItems).enter().append('span').attr('data-id', function (id) {
              return id;
            }).attr('class', 'u-display-flex u-flex-align-items-center').html(function (id) {
              return '<span class="legendText u-spacing-ml-m u-spacing-mr-xs u-font-size-6 u-cursor-pointer"><span class="u-spacing-mr-xs" style="border-radius:100%; display:inline-block; width: 8px; height:8px; background:' + that.color(id) + '"></span><span>' + id + '</span></span>' + (chartWidget.customLegends && chartWidget.customLegends[id] ? '<a target="_blank" href="' + chartWidget.customLegends[id] + '"><span class="rb-icon icon-open-new-window rb-icon--x-small u-color-grey-x-light"></span></a>' : '');
            }).select('.legendText').on('mouseover', function (id) {
              that.api.focus(id);
            }).on('mouseout', function (id) {
              that.api.revert();
            }).on('click', function (id) {
              var opacity = this.style.opacity;

              if (opacity === '1' || !opacity) {
                this.style.opacity = '0.3';
              } else {
                this.style.opacity = '1';
              }

              that.api.toggle(id);
            });
            this.d3.select(element).selectAll('.legendText').attr('data-id', function (id) {
              if (that.chartOptions && that.chartOptions.disableLegends && that.chartOptions.disableLegends.length > 0) {
                if (that.chartOptions.disableLegends.indexOf(id) !== -1) {
                  setTimeout(function () {
                    this.click();
                  }.bind(this), 100);
                }
              }

              return id;
            });
          }
        },
        onrendered: function onrendered() {
          var _this = this;

          var $$ = this;

          if (this.data.targets && this.data.targets.length > 1 && chartOptions.show_axis_colors) {
            var element = this.config.bindto;
            this.data.targets.forEach(item => {
              var axisToColor = $$.config.data_axes[item.id];
              var classToSelect = axisToColor === 'y2' ? '.c3-axis-y2 .tick' : '.c3-axis-y .tick';
              var color = $$.config.data_axes[item.id + '_color'];
              $$.d3.select(element).selectAll(classToSelect).style('fill', color);
            });
          }

          var circles = $$.getCircles();
          var singleCircleMap = {};

          if ((this.chartOptions.events || []).length > 0) {
            var _loop = function _loop(_i2) {
              var singleCircle = circles._groups[0][_i2];

              if (singleCircle.__data__.value == null) {
                return "continue";
              }

              var eventsKey = _this.chartOptions.events.findIndex(item => {
                return item.key === singleCircle.__data__.id;
              });

              if (eventsKey !== -1) {
                if (!singleCircleMap[singleCircle.__data__.x]) {
                  singleCircleMap[singleCircle.__data__.x] = [];
                }

                singleCircleMap[singleCircle.__data__.x].push(singleCircle);

                singleCircle.setAttribute('filter', "url(#".concat(_this.chartOptions.events[eventsKey].key, ")"));
                var classString = singleCircle.getAttribute('class') + ' u-opacity-1';
                singleCircle.setAttribute('class', classString);
              }
            };

            for (var _i2 = 0; _i2 < circles._groups[0].length; _i2++) {
              var _ret = _loop(_i2);

              if (_ret === "continue") continue;
            }
          }

          var shiftBy = 8;

          for (var key in singleCircleMap) {
            var dayArray = singleCircleMap[key] || [];

            if (dayArray.length > 1) {
              for (var _i3 = 0; _i3 < dayArray.length; _i3++) {
                var currentDayPoint = dayArray[_i3];
                var currentCy = parseFloat(currentDayPoint.getAttribute('cy'));
                var currentCx = parseFloat(currentDayPoint.getAttribute('cx'));
                var newCx = currentCx;
                var newCy = currentCy;

                switch (_i3) {
                  case 0:
                    newCx = currentCx + shiftBy;
                    break;

                  case 1:
                    newCx = currentCx - shiftBy;
                    break;

                  case 2:
                    newCy = currentCy - shiftBy;
                }

                currentDayPoint.setAttribute('cy', newCy);
                currentDayPoint.setAttribute('cx', newCx);
              }
            }
          }

          for (var i = 0; i < circles.length; i++) {
            for (var j = 0; j < circles[i].length; j++) {
              $$.getCircles(j).style('fill', '#FFF').style('stroke', $$.color).style('stroke-width', 0.75);
            }
          }
        },
        legend: {
          show: false
        },
        tooltip: {
          contents: function contents(d, defaultTitleFormat, defaultValueFormat, color) {
            var $$ = this;
            var config = $$.config;
            var titleFormat = config.tooltip_format_title || defaultTitleFormat;

            var nameFormat = config.tooltip_format_name || function (name) {
              return name;
            };

            var valueFormat = config.tooltip_format_value || defaultValueFormat;
            var text = '';
            var i = 0;
            var title = '';
            var value = '';
            var name = '';
            var bgcolor = '';
            var meta = this.config.data_classes;
            var eventCounts = 0;
            var eventText = "\n              <div class=\"u-spacing-pt-s u-spacing-pb-m u-spacing-pl-m u-border-top u-border-width-s u-border-color-grey-xxx-light u-display-flex\">\n                <span class=\"u-font-size-5 u-color-grey-light u-font-weight-600\">Events</span>\n               </div>";

            for (i = 0; i < d.length; i++) {
              if (!(d[i] && (d[i].value || d[i].value === 0))) {
                continue;
              }

              var eventIndex = (this.chartOptions.events || []).findIndex(item => {
                return item.key === d[i].id;
              });
              var nameFromData = d[i].name;
              var indexFromData = d[i].index;

              if (!text) {
                title = titleFormat ? titleFormat(d[i].x) : d[i].x;

                if (d[i].x && d[i].x.constructor.name === 'Date') {
                  title = vue_esm["a" /* default */].options.filters.printable_date(new Date(d[i].x));
                }

                text = "<div class=\"card u-spacing-pb-s\" style=\"background:rgba(255, 255, 255, 0.98); box-shadow: 0 0 4px 0 #caccce !important;\">\n                          <div class=\"u-spacing-p-m u-border-bottom u-border-width-s u-border-color-grey-xxx-light u-display-flex u-spacing-mb-s\">\n                            <span class=\"u-font-size-4 u-color-grey-light u-font-weight-600\">".concat(title, "</span>\n                          </div>\n                          <div class=\"u-spacing-pl-m u-spacing-pt-s u-spacing-pb-m u-spacing-pr-s u-display-flex\">\n                            <span class=\"u-font-size-5 u-color-grey-light u-font-weight-600\">Metrics</span>\n                          </div>");
              }

              if (Object.keys(meta).length > 0 && meta[d[i].name] && this.chartOptions.tooltip_mapper[nameFromData] && meta[nameFromData] && meta[nameFromData][indexFromData] && meta[nameFromData][indexFromData][this.chartOptions.tooltip_mapper[nameFromData]]) {
                name = meta[nameFromData][indexFromData][this.chartOptions.tooltip_mapper[nameFromData]];
              } else {
                name = d[i].name;
              }

              name = nameFormat(name);
              value = valueFormat(d[i].value, d[i].ratio, d[i].id, d[i].index);
              bgcolor = $$.levelColor ? $$.levelColor(d[i].value) : color(d[i].id);

              if (eventIndex !== -1) {
                var url = this.localChartSvg[d[i].id].src;
                eventText += "<div style=\"min-width: 240px; max-width: 500px;\" class=\"u-display-flex u-flex-justify-content-space-between u-flex-align-items-flex-start u-font-size-5 u-color-grey-light u-spacing-pb-s u-spacing-ph-m\">\n                            <span class=\"u-display-flex u-flex-align-items-center\" style=\"min-width:136px\">\n                              <div class=\"u-spacing-mr-s\" style=\"border-radius:100%; display:inline-block; width: 12px; height:12px;\">\n                                <img src=\"".concat(url, "\" class=\"u-display-flex u-height-100 u-width-100\" />\n                              </div>\n                              <span class=\"u-spacing-mr-s\">").concat(name, "</span>\n                            </span>\n                          </div>");
                eventCounts++;
              } else {
                text += "<div style=\"min-width: 240px; max-width: 500px;\" class=\"u-display-flex u-flex-justify-content-space-between u-flex-align-items-flex-start u-font-size-5 u-color-grey-light u-spacing-pb-s u-spacing-ph-m\">\n                <span class=\"u-display-flex u-flex-align-items-center\" style=\"min-width:136px\">\n                  <span class=\"u-spacing-mr-s\" style=\"border-radius:100%; display:inline-block; width: 8px; height:8px; background:".concat(bgcolor, "\"></span>\n                  <span class=\"u-spacing-mr-s\">").concat(name, "</span>\n                </span>\n                <span  class=\"u-font-weight-600 u-line-height-1-3\">").concat(value, "</span>\n              </div>");
              }
            }

            if (eventCounts > 0) {
              text += "<div class=\"u-spacing-pb-s\"></div>";
              return text + eventText + '</div>';
            } else {
              return text + '</div>';
            }
          },
          format: {
            value: function (value, ratio, id, index) {
              var tooltipFormat = ((this.config.chartOptions || {}).tooltip_format || {})[id] || {};

              if (isFunction(tooltipFormat)) {
                return tooltipFormat(value, ratio, id, index);
              }

              if (Object.keys(tooltipFormat).length === 0) {
                tooltipFormat = ((this.config.chartOptions || {}).tooltip_format || {})['All'] || {};
              }

              if (tooltipFormat.format !== false) {
                return vue_esm["a" /* default */].options.filters.num_format(value, tooltipFormat.pre, tooltipFormat.suff, tooltipFormat.min, tooltipFormat.roundoff);
              } else {
                return value;
              }
            }.bind(this)
          }
        }
      }
    };
  }

});

/***/ }),

/***/ "cd49":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"a784782c-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/basic/chart-events-legends.vue?vue&type=template&id=eeb5720e&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"u-width-100"},[_c('div',{staticClass:"u-display-flex u-flex-justify-content-space-between u-flex-align-items-flex-start u-flex-wrap-yes"},[_c('div',{staticClass:"u-display-flex u-flex-wrap-yes"},[_vm._l((_vm.localMetricDisplayList),function(val,index){return _c('div',{key:index,staticClass:"metric-card-hover-trigger",class:{'disabled': !_vm.computedSelectedMetric[val.key]},attrs:{"val":val},on:{"click":function($event){$event.stopPropagation();return _vm.selectNewMetric(index, val)}}},[_c('div',{staticClass:"custom-chart-events-legend u-display-inline-flex u-flex-direction-column u-position-relative u-cursor-pointer workbench-hover-card"},[_c('div',{staticClass:"u-spacing-p-xs"},[_c('div',{staticClass:"u-flex-complete-center"},[_c('rb-icon',{staticClass:"rb-icon--x-medium u-spacing-mr-s",class:val.color,attrs:{"icon":val.icon}}),(val)?_c('rb-select',{staticClass:"u-spacing-mr-s",attrs:{"width":'240px',"context":[index],"sendDetails":true,"onClose":_vm.metricSelected,"options":_vm.metricsList,"className":'campaigns-select'},scopedSlots:_vm._u([{key:"item",fn:function(option){return [_c('div',{staticClass:"u-display-flex u-flex-align-items-center"},[_c('div',{staticClass:"u-overflow-hidden u-flex-direction-row u-display-flex u-flex-align-items-center u-width-100 u-spacing-pt-s u-spacing-pb-s u-spacing-pr-m u-spacing-pl-m u-flex-justify-content-space-between"},[_c('p',{staticClass:"u-color-grey-base u-text-overflow-ellipsis u-font-size-5 u-text-case-title"},[_vm._v(_vm._s(option.title))])])])]}}],null,true)},[_c('div',{staticClass:"u-display-flex u-flex-align-items-center u-cursor-pointer",attrs:{"slot":"trigger"},slot:"trigger"},[_c('div',{staticClass:"u-display-flex u-flex-align-items-center shareCompBrandsSelectTrigger"},[_c('span',{staticClass:"u-font-size-5"},[_vm._v(_vm._s(val.key))]),_c('rb-icon',{staticClass:"rb-icon--small u-spacing-ml-xs u-color-grey-lighter",attrs:{"icon":'caret-down'}})],1)])]):_vm._e()],1)])])])}),_c('div',[_c('rb-select',{staticClass:"u-spacing-mr-m",attrs:{"width":'240px',"sendDetails":true,"onClose":_vm.addNewMetric,"options":_vm.metricsList,"className":'campaigns-select u-height-100 select-trigger-height-100'},scopedSlots:_vm._u([{key:"item",fn:function(option){return [_c('div',{staticClass:"u-display-flex u-flex-align-items-center"},[_c('div',{staticClass:"u-overflow-hidden u-flex-direction-row u-display-flex u-flex-align-items-center u-width-100 u-spacing-pt-s u-spacing-pb-s u-spacing-pr-m u-spacing-pl-m u-flex-justify-content-space-between"},[_c('p',{staticClass:"u-color-grey-base u-text-overflow-ellipsis u-font-size-5 u-text-case-title"},[_vm._v(_vm._s(option.title))])])])]}}])},[_c('div',{staticClass:"u-display-flex u-flex-align-items-center u-cursor-pointer u-height-100",attrs:{"slot":"trigger"},slot:"trigger"},[_c('div',{staticClass:"u-display-flex u-flex-align-items-center shareCompBrandsSelectTrigger u-height-100"},[_c('div',{staticClass:"metric-card-hover-trigger u-height-100"},[_c('div',{staticClass:"custom-chart-events-legend u-display-inline-flex u-flex-direction-column u-position-relative u-cursor-pointer workbench-hover-card u-height-100"},[_c('div',{staticClass:"u-spacing-p-xs u-display-flex u-flex-align-items-center u-flex-justify-content-center u-height-100"},[_c('span',{staticClass:"u-display-flex u-font-size-5 u-color-grey-lighter"},[_c('rb-icon',{staticClass:"rb-icon--small u-cursor-pointer u-color-grey-lighter u-spacing-mr-xs",attrs:{"icon":'add-circle-fill'}}),_vm._v("Add event ")],1)])])])])])])],1)],2)])])}
var staticRenderFns = []


// CONCATENATED MODULE: ./src/components/basic/chart-events-legends.vue?vue&type=template&id=eeb5720e&

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.find-index.js
var es_array_find_index = __webpack_require__("c740");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.index-of.js
var es_array_index_of = __webpack_require__("c975");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.iterator.js
var es_array_iterator = __webpack_require__("e260");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.splice.js
var es_array_splice = __webpack_require__("a434");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.number.constructor.js
var es_number_constructor = __webpack_require__("a9e3");

// EXTERNAL MODULE: ./node_modules/core-js/modules/web.dom-collections.iterator.js
var web_dom_collections_iterator = __webpack_require__("ddb0");

// EXTERNAL MODULE: ./src/components/widgetMixin.js + 7 modules
var widgetMixin = __webpack_require__("6b5d");

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/basic/chart-events-legends.vue?vue&type=script&lang=js&






//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
// import chart from './chart.vue'
 // import metric from '@/components/basic/metric'

/* harmony default export */ var chart_events_legendsvue_type_script_lang_js_ = ({
  components: {// chart,
    // metric
  },
  props: {
    metricDisplayList: {
      type: Array,
      default: () => []
    },
    metricsList: {
      type: Array,
      default: () => []
    },
    defaultSelectMetric: {
      type: Array,
      default: () => []
    },
    selectedMetricLimit: {
      type: Number,
      default: 2
    },
    metricConfig: {
      type: Object,
      default: () => {}
    },
    metricData: {
      type: Object,
      default: () => {}
    },
    minimumMetric: {
      type: Number,
      default: 2
    }
  },

  data() {
    return {
      metricsSelectedIndex: [0, 1],
      chartWkbenchAxes: {},
      selectedMetric: [],
      localMetricDisplayList: []
    };
  },

  created() {
    this.selectedMetric = this.defaultSelectMetric;
    this.localMetricDisplayList = JSON.parse(JSON.stringify(this.metricDisplayList));
    this.emitEventOut();
  },

  watch: {
    defaultSelectMetric(newValue) {
      this.selectedMetric = newValue;
    },

    metricDisplayList(newValue) {
      this.localMetricDisplayList = newValue;
    }

  },
  computed: {
    computedSelectedMetric() {
      var returnData = {};

      for (var i = 0; i < this.selectedMetric.length; i++) {
        returnData[this.selectedMetric[i].key] = this.selectedMetric[i];
      }

      return returnData;
    },

    metricColors() {
      var _colors = {};
      var colors = this.getColorPattern;

      for (var i = 0; i < this.localMetricDisplayList.length; i++) {
        _colors[this.localMetricDisplayList[i].key] = colors[i];
      }

      return _colors;
    }

  },
  methods: {
    // On Select of New Metric
    selectNewMetric(context, val) {
      var valueIndex = this.selectedMetric.findIndex(item => {
        return item.key === val.key;
      });

      if (valueIndex === -1) {
        this.smartPushQueue(val);
        this.chartWkbenchAxes[this.localMetricDisplayList[context].key] = context % 2 === 0 ? 'y' : 'y2';
      } else {
        this.queueDelete(val.key);
      }
    },

    // On Select of new Metric of a card through drop down
    metricSelected(context, val) {
      var selectedIndex = val[0].selectedIndex;

      if (this.selectedMetric.indexOf(item => {
        return item.key === this.metricsList[selectedIndex].key;
      }) === -1) {
        this.localMetricDisplayList[context[0]] = this.metricsList && this.metricsList[selectedIndex];
        this.chartWkbenchAxes[this.metricsList[selectedIndex].key] = context % 2 === 0 ? 'y' : 'y2';
        this.localMetricDisplayList = [...this.localMetricDisplayList];
      }

      this.queueReplace(this.localMetricDisplayList[context[0]], this.metricsList[selectedIndex]);
    },

    // Pushing into the queue and queue is full we removed the last element (First in last out idea)
    smartPushQueue(value) {
      if (this.selectedMetric.length >= this.selectedMetricLimit) {
        this.selectedMetric.shift();
      }

      this.selectedMetric.push(value);
      this.emitEventOut();
    },

    // Searching for the item in the queue and replacing it with newValue
    queueReplace(oldValue, newValue) {
      var indexLocation = this.selectedMetric.indexOf(item => {
        return item.key === oldValue.key;
      });

      if (indexLocation === -1) {
        this.smartPushQueue(newValue);
      } else {
        this.selectedMetric[indexLocation] = newValue;
      }

      this.emitEventOut();
    },

    // Removing the element from the queue if found in the array
    queueDelete(value) {
      debugger;
      var valueIndex = this.selectedMetric.findIndex(item => {
        console.log(item.key, value);
        return item.key === value;
      });

      if (valueIndex !== -1) {
        this.selectedMetric.splice(valueIndex, 1);
      }

      this.emitEventOut();
    },

    // Just emitting the event
    emitEventOut() {
      var obj = {
        selectedMetric: this.selectedMetric,
        metricColors: this.metricColors
      };
      this.$emit('selectedList', obj);
    },

    // Adding new metric and pushing into the queue
    addNewMetric(context, val) {
      var selectedIndex = val[0].selectedIndex;
      this.localMetricDisplayList.push(this.metricsList[selectedIndex]);
      this.smartPushQueue(this.metricsList[selectedIndex]);
    },

    // Deleting the metric and deleting from the queue
    deleteMetric(index) {
      this.queueDelete(this.localMetricDisplayList[index]);
      this.localMetricDisplayList.splice(index, 1);
    }

  },
  mixins: [widgetMixin["a" /* default */]]
});
// CONCATENATED MODULE: ./src/components/basic/chart-events-legends.vue?vue&type=script&lang=js&
 /* harmony default export */ var basic_chart_events_legendsvue_type_script_lang_js_ = (chart_events_legendsvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__("2877");

// CONCATENATED MODULE: ./src/components/basic/chart-events-legends.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__("494d")
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  basic_chart_events_legendsvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  null
  ,true
)

/* harmony default export */ var chart_events_legends = __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ "d430":
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__("24fb");
var ___CSS_LOADER_AT_RULE_IMPORT_0___ = __webpack_require__("5e0d");
exports = ___CSS_LOADER_API_IMPORT___(false);
exports.i(___CSS_LOADER_AT_RULE_IMPORT_0___);
// Module
exports.push([module.i, ".u-font-size-5{font-size:1.4rem}.metric-card-hover-trigger:hover .cross-icon{visibility:visible!important}.cross-button-holder{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:end;-ms-flex-pack:end;justify-content:flex-end}.cross-button-holder .cross-icon{visibility:hidden}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "ed20":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("d430");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to Shadow Root
var add = __webpack_require__("35d6").default
module.exports.__inject__ = function (shadowRoot) {
  add("66f1078e", content, shadowRoot)
};

/***/ })

}]);
//# sourceMappingURL=ciq-ui.11.js.map