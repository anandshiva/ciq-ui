(window["ciqUi_jsonp"] = window["ciqUi_jsonp"] || []).push([[4],{

/***/ "4678":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "2bfb",
	"./af.js": "2bfb",
	"./ar": "8e73",
	"./ar-dz": "a356",
	"./ar-dz.js": "a356",
	"./ar-kw": "423e",
	"./ar-kw.js": "423e",
	"./ar-ly": "1cfd",
	"./ar-ly.js": "1cfd",
	"./ar-ma": "0a84",
	"./ar-ma.js": "0a84",
	"./ar-sa": "8230",
	"./ar-sa.js": "8230",
	"./ar-tn": "6d83",
	"./ar-tn.js": "6d83",
	"./ar.js": "8e73",
	"./az": "485c",
	"./az.js": "485c",
	"./be": "1fc1",
	"./be.js": "1fc1",
	"./bg": "84aa",
	"./bg.js": "84aa",
	"./bm": "a7fa",
	"./bm.js": "a7fa",
	"./bn": "9043",
	"./bn.js": "9043",
	"./bo": "d26a",
	"./bo.js": "d26a",
	"./br": "6887",
	"./br.js": "6887",
	"./bs": "2554",
	"./bs.js": "2554",
	"./ca": "d716",
	"./ca.js": "d716",
	"./cs": "3c0d",
	"./cs.js": "3c0d",
	"./cv": "03ec",
	"./cv.js": "03ec",
	"./cy": "9797",
	"./cy.js": "9797",
	"./da": "0f14",
	"./da.js": "0f14",
	"./de": "b469",
	"./de-at": "b3eb",
	"./de-at.js": "b3eb",
	"./de-ch": "bb71",
	"./de-ch.js": "bb71",
	"./de.js": "b469",
	"./dv": "598a",
	"./dv.js": "598a",
	"./el": "8d47",
	"./el.js": "8d47",
	"./en-SG": "cdab",
	"./en-SG.js": "cdab",
	"./en-au": "0e6b",
	"./en-au.js": "0e6b",
	"./en-ca": "3886",
	"./en-ca.js": "3886",
	"./en-gb": "39a6",
	"./en-gb.js": "39a6",
	"./en-ie": "e1d3",
	"./en-ie.js": "e1d3",
	"./en-il": "7333",
	"./en-il.js": "7333",
	"./en-nz": "6f50",
	"./en-nz.js": "6f50",
	"./eo": "65db",
	"./eo.js": "65db",
	"./es": "898b",
	"./es-do": "0a3c",
	"./es-do.js": "0a3c",
	"./es-us": "55c9",
	"./es-us.js": "55c9",
	"./es.js": "898b",
	"./et": "ec18",
	"./et.js": "ec18",
	"./eu": "0ff2",
	"./eu.js": "0ff2",
	"./fa": "8df4",
	"./fa.js": "8df4",
	"./fi": "81e9",
	"./fi.js": "81e9",
	"./fo": "0721",
	"./fo.js": "0721",
	"./fr": "9f26",
	"./fr-ca": "d9f8",
	"./fr-ca.js": "d9f8",
	"./fr-ch": "0e49",
	"./fr-ch.js": "0e49",
	"./fr.js": "9f26",
	"./fy": "7118",
	"./fy.js": "7118",
	"./ga": "5120",
	"./ga.js": "5120",
	"./gd": "f6b4",
	"./gd.js": "f6b4",
	"./gl": "8840",
	"./gl.js": "8840",
	"./gom-latn": "0caa",
	"./gom-latn.js": "0caa",
	"./gu": "e0c5",
	"./gu.js": "e0c5",
	"./he": "c7aa",
	"./he.js": "c7aa",
	"./hi": "dc4d",
	"./hi.js": "dc4d",
	"./hr": "4ba9",
	"./hr.js": "4ba9",
	"./hu": "5b14",
	"./hu.js": "5b14",
	"./hy-am": "d6b6",
	"./hy-am.js": "d6b6",
	"./id": "5038",
	"./id.js": "5038",
	"./is": "0558",
	"./is.js": "0558",
	"./it": "6e98",
	"./it-ch": "6f12",
	"./it-ch.js": "6f12",
	"./it.js": "6e98",
	"./ja": "079e",
	"./ja.js": "079e",
	"./jv": "b540",
	"./jv.js": "b540",
	"./ka": "201b",
	"./ka.js": "201b",
	"./kk": "6d79",
	"./kk.js": "6d79",
	"./km": "e81d",
	"./km.js": "e81d",
	"./kn": "3e92",
	"./kn.js": "3e92",
	"./ko": "22f8",
	"./ko.js": "22f8",
	"./ku": "2421",
	"./ku.js": "2421",
	"./ky": "9609",
	"./ky.js": "9609",
	"./lb": "440c",
	"./lb.js": "440c",
	"./lo": "b29d",
	"./lo.js": "b29d",
	"./lt": "26f9",
	"./lt.js": "26f9",
	"./lv": "b97c",
	"./lv.js": "b97c",
	"./me": "293c",
	"./me.js": "293c",
	"./mi": "688b",
	"./mi.js": "688b",
	"./mk": "6909",
	"./mk.js": "6909",
	"./ml": "02fb",
	"./ml.js": "02fb",
	"./mn": "958b",
	"./mn.js": "958b",
	"./mr": "39bd",
	"./mr.js": "39bd",
	"./ms": "ebe4",
	"./ms-my": "6403",
	"./ms-my.js": "6403",
	"./ms.js": "ebe4",
	"./mt": "1b45",
	"./mt.js": "1b45",
	"./my": "8689",
	"./my.js": "8689",
	"./nb": "6ce3",
	"./nb.js": "6ce3",
	"./ne": "3a39",
	"./ne.js": "3a39",
	"./nl": "facd",
	"./nl-be": "db29",
	"./nl-be.js": "db29",
	"./nl.js": "facd",
	"./nn": "b84c",
	"./nn.js": "b84c",
	"./pa-in": "f3ff",
	"./pa-in.js": "f3ff",
	"./pl": "8d57",
	"./pl.js": "8d57",
	"./pt": "f260",
	"./pt-br": "d2d4",
	"./pt-br.js": "d2d4",
	"./pt.js": "f260",
	"./ro": "972c",
	"./ro.js": "972c",
	"./ru": "957c",
	"./ru.js": "957c",
	"./sd": "6784",
	"./sd.js": "6784",
	"./se": "ffff",
	"./se.js": "ffff",
	"./si": "eda5",
	"./si.js": "eda5",
	"./sk": "7be6",
	"./sk.js": "7be6",
	"./sl": "8155",
	"./sl.js": "8155",
	"./sq": "c8f3",
	"./sq.js": "c8f3",
	"./sr": "cf1e",
	"./sr-cyrl": "13e9",
	"./sr-cyrl.js": "13e9",
	"./sr.js": "cf1e",
	"./ss": "52bd",
	"./ss.js": "52bd",
	"./sv": "5fbd",
	"./sv.js": "5fbd",
	"./sw": "74dc",
	"./sw.js": "74dc",
	"./ta": "3de5",
	"./ta.js": "3de5",
	"./te": "5cbb",
	"./te.js": "5cbb",
	"./tet": "576c",
	"./tet.js": "576c",
	"./tg": "3b1b",
	"./tg.js": "3b1b",
	"./th": "10e8",
	"./th.js": "10e8",
	"./tl-ph": "0f38",
	"./tl-ph.js": "0f38",
	"./tlh": "cf75",
	"./tlh.js": "cf75",
	"./tr": "0e81",
	"./tr.js": "0e81",
	"./tzl": "cf51",
	"./tzl.js": "cf51",
	"./tzm": "c109",
	"./tzm-latn": "b53d",
	"./tzm-latn.js": "b53d",
	"./tzm.js": "c109",
	"./ug-cn": "6117",
	"./ug-cn.js": "6117",
	"./uk": "ada2",
	"./uk.js": "ada2",
	"./ur": "5294",
	"./ur.js": "5294",
	"./uz": "2e8c",
	"./uz-latn": "010e",
	"./uz-latn.js": "010e",
	"./uz.js": "2e8c",
	"./vi": "2921",
	"./vi.js": "2921",
	"./x-pseudo": "fd7e",
	"./x-pseudo.js": "fd7e",
	"./yo": "7f33",
	"./yo.js": "7f33",
	"./zh-cn": "5c3a",
	"./zh-cn.js": "5c3a",
	"./zh-hk": "49ab",
	"./zh-hk.js": "49ab",
	"./zh-tw": "90ea",
	"./zh-tw.js": "90ea"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "4678";

/***/ }),

/***/ "6b5d":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.filter.js
var es_array_filter = __webpack_require__("4de4");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.function.name.js
var es_function_name = __webpack_require__("b0c0");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.object.to-string.js
var es_object_to_string = __webpack_require__("d3b7");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.promise.js
var es_promise = __webpack_require__("e6cf");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.regexp.constructor.js
var es_regexp_constructor = __webpack_require__("4d63");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.regexp.to-string.js
var es_regexp_to_string = __webpack_require__("25f0");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.replace.js
var es_string_replace = __webpack_require__("5319");

// EXTERNAL MODULE: ./node_modules/axios/index.js
var axios = __webpack_require__("bc3a");
var axios_default = /*#__PURE__*/__webpack_require__.n(axios);

// CONCATENATED MODULE: ./src/utils/services/url-factory.js
/* harmony default export */ var url_factory = ({
  getFinalURL(url) {
    return this[url];
  },

  EMAIL: '/api/mail',
  SESSION_DATA: '/api/config',
  RECOMMENDATIONS: '/api/proxy/aramusplatform/rest/recommendation',
  RECOMMENDATIONS_OF_TYPE: '/api/proxy/aramusplatform/rest/recommendation/recommendationMetaData/:recommendationType/:recommendationName',
  RECOMMENDATION_FILTERS: '/api/proxy/aramusplatform/rest/recommendation/dimension',
  RECOMMENDATION_COLUMNS: '/api/proxy/aramusplatform/rest/recommendation/recommendationaLableMapping',
  UPDATE_RECOMMENDATION_COLUMNS: '/api/proxy/aramusplatform/rest/recommendation/uiLabelMapping',
  SEARCH_RECOMMENDATIONS: '/api/proxy/aramusplatform/rest/recommendation/searchRecommendation/:searchString',
  REMOVE_RECOMMENDATIONS: '/api/proxy/aramusplatform/rest/recommendation/removeSKU',
  RESTORE_RECOMMENDATIONS: '/api/proxy/aramusplatform/rest/recommendation/restoreSKU',
  GET_REMOVED_RECOMMENDATIONS: '/api/proxy/aramusplatform/rest/recommendation/getRemovedSKU',
  GET_STRATEGIES: '/api/proxy/aramusplatform/rest/strategies',
  SIMULTAED_ALERTS: '/api/proxy/aramusplatform/rest/recommendation/simulatedAlert/list',
  LOGIN: '/api/login',
  LOGOUT: '/api/logout',
  REGISTER: '/api/user/register',
  PASSWORD_RESET_LINK: '/api/user/forgotPassword',
  PASSWORD_CHANGE: '/api/user/setPassword',
  INVITE_USERS: 'api/user/inviteUsers',
  INVITE_USERS_V2: 'api/user/inviteUsers/v2',
  CHECK_USER_STATUS: '/api/user/welcome',
  TABLEAU_GET_TRUST_URL: '/api/tableau/getTrustedviewUrl',
  REVENUE_CHART: {
    demo: {
      table: 'Table 1',
      view: 'sample'
    },
    prod: '/api/revenueChart'
  },
  GET_CUBE_LIST: '/api/proxy/brands-service/cube/list',
  GET_CUBE_DETAILS: '/api/proxy/brands-service/cube/getDetails',
  GET_CUBE_QUERY: '/api/proxy/brands-service/cube/view',
  EXECUTE_CUBE_API: '/api/proxy/brands-service/cube/execute',
  EXECUTE_CUBE_API_EMAIL: '/api/proxy/brands-service/cube/execute/download',
  EXECUTE_CUBE_CHART_API: '/api/proxy/brands-service/cube/execute/charts',
  EXECUTE_CUSTOM_CUBE_API: '/api/proxy/brands-service/cube/execute/charts/sku',
  EXECUTE_CUSTOM_CUBE_SKU_API: '/api/proxy/brands-service/cube/execute/custom',
  EXECUTE_CUBE_API_V2: '/api/proxy/brands-service/cube/execute/custom/v2',
  SOV_BRANDS_CUSTOM: '/api/proxy/brands-service/cube/execute/v2/sov/brands',
  getOTP: '/api/proxy/aramus/rest/twoFactor/getOtp',
  SEND_FEEDBACK: '/api/feedback/post',
  HARDFETCHSKUDETAILS: '/api/proxy/aramusplatform/rest/recommendation/recommendationDetails',
  KEEPALIVE: '/api/keepalive',
  RECOMMENDATION_ACTION_CHANGE: '/api/proxy/aramusplatform/rest/recommendation/saveTaskAction',
  GET_SELF_STRATEGIES: '/api/proxy/aramusplatform/rest/strategies/getSelfServeStrategyDetails',
  FETCH_FILTERS: '/api/proxy/brands-service/internal/dimension/filters',
  FETCH_FILTERS_V2: '/api/proxy/brands-service/filter/filters',
  SAVE_FILTER: '/api/proxy/brands-service/filter/create',
  DELETE_FILTER: '/api/proxy/brands-service/filter/delete',
  UPDATE_SELF_SERVE_STATUS: '/api/proxy/aramusplatform/rest/strategies/updateStrategyStatus',
  USERS: '/api/user/users',
  SUBMIT_ACTIONS: '/api/proxy/aramusplatform/rest/action/submitActions',
  AMS_ACTIONS: '/api/proxy/brands-service/worklog',
  ENTITY_VALIDATION: '/api/proxy/strategy-service/v1/strategy/entity/validation',
  CREATE_STRATEGY: '/api/proxy/strategy-service/v1/strategy',
  GET_STRATEGY_DATA: '/api/proxy/strategy-service/v1/strategy/data',
  TEMPLATELIST: '/api/proxy/aramusplatform/rest/template/list/alert',
  CREATETEMPLATE: '/api/proxy/aramusplatform/rest/template/create',
  READPARAMSFORALERT: '/api/proxy/aramusplatform/rest/alert/read',
  SIMULATETEMPLATE: '/api/proxy/aramusplatform/rest/selfserve/simulation/simulate',
  FETCHSIMULATIONLIST: '/api/proxy/aramusplatform/rest/selfserve/simulation/list/:templateID',
  PUBLISHTEMPLATE: '/api/proxy/aramusplatform/rest/template/publish',
  READALLPARAMETERS: '/api/proxy/aramusplatform/rest/alert/params/all',
  DELETETEMPLATE: '/api/proxy/aramusplatform/rest/template/delete',
  READTEMPLATE: '/api/proxy/aramusplatform/rest/template/read',
  READSUBSCRIPTION: '/api/proxy/subscription-service/subscription/read',
  UPDATESUBSCRIPTION: '/api/proxy/subscription-service/subscription/update',
  CREATESUBSCRIPTION: '/api/proxy/subscription-service/subscription/create',
  UNSUBSCRIPTION: '/api/proxy/subscription-service/subscription/unsubscribe/internal'
});
// CONCATENATED MODULE: ./src/utils/services/http-service.js






 // import { store } from '@/store/store'

var http_service_getUrl = function getUrl(id, config) {
  // automatically replaces :param in a url like a/:param/b/;param
  var url = url_factory.getFinalURL(id);

  if (config && config.append) {
    url = url + config.append;
  }

  if (!config || !config.pathParams) return url;
  var paramList = Object.keys(config.pathParams);

  for (var _i = 0, _paramList = paramList; _i < _paramList.length; _i++) {
    var param = _paramList[_i];
    var regex1 = '/:' + param + '/';
    var regex2 = '/:' + param + '$';
    url = url.replace(new RegExp(regex1, 'g'), '/' + config.pathParams[param] + '/').replace(new RegExp(regex2, 'g'), '/' + config.pathParams[param]);
  }

  return url;
};

axios_default.a.interceptors.response.use(function (response) {
  return response;
}, function (err) {
  // if (err.response.status === 401 && store.getters.getSessionValidity) {
  if (err.response.status === 401) {
    // store.commit('LOGOUT', 'returnslink=' + encodeURIComponent(window.location));
    return Promise.reject(err);
  } else {
    return Promise.reject(err);
  }
});
/* harmony default export */ var http_service = ({
  all: axios_default.a.all,

  get(id, config) {
    return axios_default.a.get(http_service_getUrl(id, config), config);
  },

  post(id, data, config) {
    return axios_default.a.post(http_service_getUrl(id, config), data, config);
  },

  put(id, data, config) {
    return axios_default.a.put(http_service_getUrl(id, config), data, config);
  },

  patch(id, data, config) {
    return axios_default.a.patch(http_service_getUrl(id, config), data, config);
  }

});
// CONCATENATED MODULE: ./src/utils/services/http-layer.js

/* harmony default export */ var http_layer = ({
  all: http_service.all,
  post: config => {
    if (!config.APIData) {
      config.APIData = {};
    }

    if (!config.header) {
      config.header = {};
    }

    var oPromise = http_service.post(config.cube || 'EXECUTE_CUBE_API', config.APIData, config.header).then(response => {
      var _response = {};

      if (response.data.success) {
        _response.success = true;

        if (response.data.response.data) {
          _response.data = response.data.response.data;
        }

        if (response.data.response.dataSize) {
          _response.dataSize = response.data.response.dataSize;
        }
      } else {
        _response.success = false;
      }

      _response.fullResponse = response.data.response;
      return _response;
    }).catch(error => {
      console.log(error);
      return {
        success: false
      };
    });
    return oPromise;
  }
});
// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.concat.js
var es_array_concat = __webpack_require__("99af");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.find-index.js
var es_array_find_index = __webpack_require__("c740");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.index-of.js
var es_array_index_of = __webpack_require__("c975");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.iterator.js
var es_array_iterator = __webpack_require__("e260");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.map.js
var es_array_map = __webpack_require__("d81d");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.sort.js
var es_array_sort = __webpack_require__("4e82");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.splice.js
var es_array_splice = __webpack_require__("a434");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.number.constructor.js
var es_number_constructor = __webpack_require__("a9e3");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.split.js
var es_string_split = __webpack_require__("1276");

// EXTERNAL MODULE: ./node_modules/core-js/modules/web.dom-collections.iterator.js
var web_dom_collections_iterator = __webpack_require__("ddb0");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.number.to-fixed.js
var es_number_to_fixed = __webpack_require__("b680");

// EXTERNAL MODULE: ./node_modules/vue/dist/vue.esm.js
var vue_esm = __webpack_require__("a026");

// EXTERNAL MODULE: ./node_modules/moment/moment.js
var moment = __webpack_require__("c1df");
var moment_default = /*#__PURE__*/__webpack_require__.n(moment);

// CONCATENATED MODULE: ./src/utils/helpers/formatter.js





function getValueAlphabets(value) {
  var sign = '';

  if (value < 0) {
    sign = '-';
    value = value * -1;
  }

  var q = '';

  if (value > 1000000000) {
    q = 'B';
    value = value / 1000000000;
  } else if (value > 1000000) {
    q = 'M';
    value = value / 1000000;
  } else if (value > 1000) {
    q = 'K';
    value = value / 1000;
  }

  if (!Number.isInteger(value)) {
    value = value.toFixed(2);
  }

  return {
    sign: sign,
    value: value,
    notation: q
  };
}

function formatter(value, format) {
  if (!format) {
    format = 'string';
  }

  if (value === undefined || value === null) {
    return 'NA';
  }

  if (format.toLowerCase() === 'string') {
    return value;
  } else if (format.toLowerCase() === 'currency') {
    var obj = getValueAlphabets(value);
    return obj.sign + vue_esm["a" /* default */].prototype.$currency + obj.value + obj.notation;
  } else if (format.toLowerCase() === 'percent' || format.toLowerCase() === 'percentage') {
    return value === 0 ? '0%' : value.toFixed(2) + '%';
  } else if (format.toLowerCase() === 'date') {
    // return new Date(value)
    //   .toLocaleString('en-US', { timeZone: 'UTC' })
    //   .split(',')[0];
    // Format Change for KC-UK
    return moment_default()(value).format('MMM DD, YYYY');
  } else if (format === 'percentFraction') {
    return value + '/100';
  } else if (format.toLowerCase() === 'numeric') {
    obj = getValueAlphabets(value);
    return obj.sign + obj.value + obj.notation;
  } else if (format.toLowerCase() === 'numberic_x') {
    return value + 'X';
  } else {
    return value;
  }
}
// CONCATENATED MODULE: ./src/utils/helpers/operator.js
var operator = {
  EQUAL_TO: '=',
  GREATER_THAN: '>',
  GREATER_THAN_OR_EQUAL_TO: '> =',
  LESS_THAN: '<',
  LESS_THAN_OR_EQUAL_TO: '< =',
  NOT_EQUAL_TO: '! =',
  ILIKE: 'Has',
  BETWEEN: '> & <'
};
var operatorTitle = {
  '=': 'Equals',
  '>': 'Greater than',
  '> =': 'Greater than equal to',
  '<': 'Less than',
  '< =': 'Less than equal to',
  '! =': 'Not equal to',
  '> & <': 'Between'
};

// CONCATENATED MODULE: ./src/utils/services/data-transformer.js
















/* harmony default export */ var data_transformer = ({
  getUniqueFilters: (data, prefix, filterMappings) => {
    if (!prefix) {
      prefix = '';
    }

    if (!filterMappings) {
      filterMappings = JSON.parse(localStorage.getItem(prefix.replace('_', '') + '_filters_mapping'));
    }

    var categoryText = 'category';
    var subCategoryText = 'subcategory';

    if (data.length > 0 && data[0].DIMENSION['category'] === undefined) {// categoryText = 'l1';
    }

    if (data.length > 0 && data[0].DIMENSION['category'] === undefined && data[0].DIMENSION['l1'] === undefined) {
      categoryText = 'client_category';
    }

    if (data.length > 0 && data[0].DIMENSION['subcategory'] === undefined) {// subCategoryText = 'l2';
    }

    if (data.length > 0 && data[0].DIMENSION['subcategory'] === undefined && data[0].DIMENSION['l2'] === undefined) {
      subCategoryText = 'client_subcategory';
    }

    var category = {};
    var subcategory = {};
    var oObj = {};
    var _oReturn = {};

    for (var i = 0; i < data.length; i++) {
      if (data[i].DIMENSION[categoryText] !== undefined && data[i].DIMENSION[subCategoryText] !== undefined) {
        if (!category[data[i].DIMENSION[categoryText]]) {
          category[data[i].DIMENSION[categoryText]] = {
            title: data[i].DIMENSION[categoryText],
            values: []
          };
          subcategory[data[i].DIMENSION[categoryText]] = {};
        }

        if (!subcategory[data[i].DIMENSION[categoryText]][data[i].DIMENSION[subCategoryText]]) {
          subcategory[data[i].DIMENSION[categoryText]][data[i].DIMENSION[subCategoryText]] = true;

          if (data[i].DIMENSION[subCategoryText]) {
            category[data[i].DIMENSION[categoryText]].values.push({
              title: data[i].DIMENSION[subCategoryText]
            });
          }
        }
      }

      for (var j in data[i].DIMENSION) {
        if (!oObj[prefix + j]) {
          oObj[prefix + j] = {};
        }

        var found = true;

        if (filterMappings && filterMappings[prefix + j]) {
          for (var y in filterMappings[prefix + j]) {
            if (filterMappings[prefix + j][y] && filterMappings[prefix + j][y].length === 0) {
              found = true;
              break;
            }

            if (filterMappings[prefix + j][y].indexOf(data[i].DIMENSION[y.replace(prefix, '')]) === -1) {
              found = false;
              break;
            }
          }
        }

        if (found) {
          oObj[prefix + j][data[i].DIMENSION[j]] = true;
        } else {
          if (oObj[prefix + j][data[i].DIMENSION[j]] !== true) {
            delete oObj[prefix + j][data[i].DIMENSION[j]];
          }
        }
      }
    }

    for (var k in oObj) {
      if (!_oReturn[k]) {
        _oReturn[k] = [];
      }

      for (var l in oObj[k]) {
        if (l && l !== 'null' && l.length > 0) {
          _oReturn[k].push({
            title: l
          });
        }
      }
    }

    if (_oReturn[prefix + categoryText]) {
      var oReturnCategory = [];

      for (var t in category) {
        if (oObj[prefix + categoryText][t]) {
          if (category[t].values && category[t].values.length > 0) {
            for (var r = 0; r < category[t].values.length; r++) {
              if (!oObj[prefix + subCategoryText][category[t].values[r].title] || t.length === 0) {
                category[t].values.splice(r, 1);
              }
            }
          }

          if (t !== 'null' && t.length > 0) {
            oReturnCategory.push(category[t]);
          }
        }
      }

      _oReturn[prefix + categoryText] = oReturnCategory;
      delete _oReturn[prefix + subCategoryText];
    }

    return _oReturn;
  },
  getChartDataInFormat: (data, response) => {
    var _aArray = [];
    var _oObj = {};

    if (response === null) {
      return [];
    }

    for (var i = 0; i < response.length; i++) {
      for (var j in response[i]) {
        var _j = ((data || {}).map || {})[j] || j;

        if (!_oObj[_j]) {
          _oObj[_j] = [_j];
        }

        _oObj[_j].push(response[i][j]);
      }
    }

    for (var k in _oObj) {
      _aArray.push(_oObj[k]);
    }

    return _aArray;
  },
  getChartTicksValues: (data, key) => {
    var newArr = [];

    if (data && data.length) {
      for (var i = 0; i < data.length; i++) {
        newArr.push(data[i][key]);
      }
    }

    return newArr;
  },
  mergeResultDimension: (data, addPVP, removeNullProp) => {
    var _aArray = [];

    if (data && data.length) {
      for (var i = 0; i < data.length; i++) {
        var oObj = {};

        for (var j in data[i]['RESULT']) {
          oObj[j] = data[i]['RESULT'][j];
        }

        for (j in data[i]['DIMENSION']) {
          oObj[j] = data[i]['DIMENSION'][j];
        }

        if (addPVP) {
          for (j in data[i]['PVP']) {
            oObj['PVP_' + j] = data[i]['PVP'][j];
          }
        }

        if (removeNullProp) {
          if (oObj[removeNullProp] !== undefined) {
            if (oObj[removeNullProp] !== null && oObj[removeNullProp] !== '') {
              _aArray.push(oObj);
            }
          } else {
            _aArray.push(oObj);
          }
        } else {
          _aArray.push(oObj);
        }
      }
    }

    return _aArray;
  },
  getCompleteWhereClause: (where, stateSelectedFilters) => {
    var oReturn = {
      dimensionNameValueList: []
    };

    if (where) {
      for (var i = 0; i < where.length; i++) {
        oReturn.dimensionNameValueList.push(where[i]);
      }
    }

    if (stateSelectedFilters && stateSelectedFilters.dimensionNameValueList) {
      for (i = 0; i < stateSelectedFilters.dimensionNameValueList.length; i++) {
        if (oReturn.dimensionNameValueList.indexOf(stateSelectedFilters.dimensionNameValueList[i]) === -1) {
          var item = stateSelectedFilters.dimensionNameValueList[i]; // convert 'BETWEEN' operator to 'GREATER_THAN_OR_EQUAL' and 'LESS_THAN_OR_EQUAL' operators

          if (item.operator && item.operator === 'BETWEEN') {
            var obj1 = {
              operator: 'GREATER_THAN_OR_EQUAL_TO',
              dimensionName: item.dimensionName,
              dimensionValue: Number(item.dimensionValue.split(', ')[0])
            };
            var obj2 = {
              operator: 'LESS_THAN_OR_EQUAL_TO',
              dimensionName: item.dimensionName,
              dimensionValue: Number(item.dimensionValue.split(', ')[1])
            };
            oReturn.dimensionNameValueList.push(obj1, obj2);
          } else {
            oReturn.dimensionNameValueList.push(item);
          }
        }
      }
    }

    return oReturn;
  },

  getCellRenderParams(data) {
    var type = data.uiField.uiType.toLowerCase();
    var objToReturn = {
      formatterFn: formatter
    };

    switch (type) {
      case 'percent':
        objToReturn.keyType = 'PERCENTAGE';
        break;

      case 'currency':
        objToReturn.keyType = 'CURRENCY';
        break;

      case 'numeric':
        // If isFormattingRequired is not passed by the backend we assumne isFormattingRequired to be true
        var formattingRequired = data.uiField.metadata.isFormattingRequired === undefined ? true : data.uiField.metadata.isFormattingRequired; // If formatting is not required we are removing the formatterFn Field from the object

        if (formattingRequired === false) {
          delete objToReturn.formatterFn;
        }

        objToReturn.keyType = 'numeric';
        break;

      case 'string':
        objToReturn = {
          keyType: 'string'
        };
        break;

      default:
    }

    return objToReturn;
  },

  getCustomCellRender(data) {
    var colMetaData = data.uiField.metadata;
    var type = colMetaData.widget.toLowerCase();
    var objToReturn = {};

    switch (type) {
      case 'hyperlink':
        objToReturn.cellRendererFramework = 'linkDisplay';
        objToReturn.cellRendererParams = {
          url: colMetaData.urlTableColumnName
        };
        break;

      case 'progress':
        objToReturn.cellRendererFramework = 'progressDisplay';
        objToReturn.cellRendererParams = {
          fill: colMetaData.percentTableColumnName,
          decimalRoundOff: colMetaData.decimalRoundOff
        };
        break;

      case 'metric':
        objToReturn.cellRendererFramework = 'metricDisplay';
        objToReturn.minWidth = 150;
        objToReturn.cellRendererParams = {
          tag1Key: colMetaData.primaryTableColumnName,
          tag2Key: colMetaData.secondaryTableColumnName,
          tag1Unit: this.getTagUnitData(colMetaData.primaryIsPrefix, colMetaData.primaryUnit),
          tag2Unit: this.getTagUnitData(colMetaData.secondaryIsPrefix, colMetaData.secondaryUnit)
        };
        break;

      case 'icon':
        objToReturn.cellRendererFramework = 'iconTableCell';
        objToReturn.headerComponentFramework = 'iconTableHeader';
        objToReturn.headerComponentParams = {};
        objToReturn.headerComponentParams.displayIcon = colMetaData.displayIcon;
        objToReturn.cellRendererParams = {
          iconSize: !colMetaData.iconSize ? 'medium' : colMetaData.iconSize,
          iconClickEvent: colMetaData.iconClickEvent,
          displayIcon: colMetaData.displayIcon,
          toolTipText: colMetaData.toolTipText,
          toolTipPosition: colMetaData.toolTipPosition,
          contextReturnEvent: colMetaData.contextReturnEvent
        };
        objToReturn.cellRendererParams.type = !colMetaData.type ? 'icon' : colMetaData.type;

        if (colMetaData.type === 'iconText') {
          objToReturn.cellRendererParams.formatType = colMetaData.formatType;
        }

        break;

      case 'input':
        objToReturn.cellRendererFramework = 'inputTypeCell';
        objToReturn.cellRendererParams = {
          type: !colMetaData.type ? 'text' : colMetaData.type,
          blurEvent: colMetaData.blurEvent,
          onchangeEvent: colMetaData.onchangeEvent,
          keyupEvent: colMetaData.keyupEvent,
          defaultValueColumnName: colMetaData.defaultValueColumnName,
          formatType: colMetaData.formatType,
          contextReturnEvent: colMetaData.contextReturnEvent
        };
        break;

      default:
    }

    return objToReturn;
  },

  getTagUnitData(isPrefix, unit) {
    if (isPrefix) {
      return {
        pre: unit
      };
    } else {
      return {
        suff: unit
      };
    }
  },

  getColumnDefinition(columns, customData, customObject) {
    var colDefinitionToReturn = [];
    var columnArray = columns;

    for (var i = 0; i < columnArray.length; i++) {
      var currDefinition = columnArray[i];
      var obj = {};
      obj.showOnUi = currDefinition.uiField.metadata.showOnUi;
      obj.isDownloadable = currDefinition.uiField.metadata.isDownloadable;
      obj.headerComponentFramework = 'genericTableHeader';
      obj.title = currDefinition.uiField.uiLabel;
      obj.headerName = currDefinition.uiField.uiLabel;
      obj.field = currDefinition.uiField.metadata.tableColumnName === undefined ? currDefinition.name : currDefinition.uiField.metadata.tableColumnName;
      obj.cellRendererFramework = 'genericTableCell';
      obj.cellRendererParams = this.getCellRenderParams(currDefinition);

      if (currDefinition.uiField.uiType.toLowerCase() === 'string') {
        if (currDefinition.uiField.metadata.width !== undefined) {
          obj.minWidth = currDefinition.uiField.metadata.width;
          obj.width = currDefinition.uiField.metadata.width;
        } else {
          obj.minWidth = 180;
        }
      } else {
        obj.minWidth = 120;
      } // obj.minWidth = currDefinition.uiField.uiType.toLowerCase() === 'string' ? 180 : 120;


      if (currDefinition.uiField.uiType === 'custom') {
        obj.minWidth = currDefinition.uiField.metadata.width;

        if (customData !== undefined && typeof customData === 'object' && Object.keys(customData).length > 0 && customData[currDefinition.name]) {
          obj.cellRendererFramework = customData[currDefinition.name].component;
          obj.cellRendererParams = (customData[currDefinition.name] || {}).params || null;
          obj.minWidth = 200;
        } else {
          if (currDefinition.uiField.metadata.widget === 'progress') {
            obj.field = currDefinition.uiField.metadata.percentTableColumnName;
          } // if (currDefinition.uiField.metadata.widget === 'icon') {
          // obj.cellRendererFramework = 'iconTableCell';
          // obj.headerComponentFramework = 'iconTableHeader';
          // // obj.cellRendererParams.iconClickEvent = vueRef.openSidePanel;
          // obj.cellRendererParams.displayIcon = 'timeline';
          // // obj.cellRendererParams.toolTipText = dictionary.map[i].toolTipText;
          // obj.notDownloadable = true;
          // obj.headerComponentParams.displayIcon = 'timeline';
          // obj.minWidth = 60;
          // }


          var cellObj = this.getCustomCellRender(currDefinition);
          obj.cellRendererFramework = cellObj.cellRendererFramework;
          obj.cellRendererParams = cellObj.cellRendererParams;

          if (cellObj.minWidth) {
            obj.minWidth = cellObj.minWidth;
          }

          if (customObject !== undefined && customObject.hasOwnProperty(currDefinition.uiField.metadata.tableColumnName)) {
            var columnToRead = currDefinition.uiField.metadata.tableColumnName;

            for (var k in customObject[columnToRead]) {
              obj.cellRendererParams[k] = customObject[columnToRead][k];
            }
          }
        }
      }

      obj.keyOrder = currDefinition.uiField.uiOrder;
      obj.pinned = currDefinition.uiField.metadata.isFixed;
      colDefinitionToReturn[obj.keyOrder] = obj;
      obj.headerComponentParams = {
        enableSorting: currDefinition.uiField.metadata.sortOnColumn === undefined ? false : currDefinition.uiField.metadata.sortOnColumn,
        keyType: obj.cellRendererParams.keyType,
        toolTipText: currDefinition.uiField.uiTooltip
      };

      if (currDefinition.uiField.metadata.isDefaultSortColumn) {
        obj.headerComponentParams.sort = currDefinition.uiField.metadata.sortDirection; // 'asc'
      }
    }

    var displayColConfigs = colDefinitionToReturn.filter(elm => {
      return elm.showOnUi === true;
    });
    var downloadColConfigs = colDefinitionToReturn.filter(elm => {
      return elm.isDownloadable === true;
    });
    var objToReturn = {
      displayColConfigs: displayColConfigs,
      downloadColConfigs: downloadColConfigs
    };
    return objToReturn;
  },

  getTableDataFromFullResponse: function getTableDataFromFullResponse(apiResponse) {
    var response = apiResponse.fullResponse;
    var columns = response.metadata;
    var measureList = columns.measureList;
    var groupByDimensionList = columns.groupByDimensionList;
    measureList = measureList.concat(groupByDimensionList);
    var rows = this.mergeResultDimension(response.data);
    var obj = {
      rows: rows,
      columns: measureList
    };
    return obj;
  },

  generateWhereClause(where, filters) {
    var values = filters.values;

    for (var key in values) {
      if (values.hasOwnProperty(key)) {
        if (values[key].constructor === Array && key !== 'date_range') {
          // for filters
          for (var i = 0; i < values[key].length; i++) {
            var temp = values[key];

            if (!where['dimensionNameValueList']) {
              where['dimensionNameValueList'] = [];
            }

            if (temp[i].operator) {
              where['dimensionNameValueList'].push({
                'dimensionName': key,
                'dimensionValue': temp[i].value,
                'operator': temp[i].operator.operator
              });
            } else {
              where['dimensionNameValueList'].push({
                'dimensionName': key,
                'dimensionValue': temp[i]
              });
            }
          }
        } else if (key === 'date_range') {
          // for date range
          where['date'] = {
            from: values[key].from,
            to: values[key].to
          };
        }
      }
    }

    return where;
  },

  convertDimensionsToFilterFormat(dimensions) {
    var _this = this;

    var filter = {
      order: [],
      values: {}
    };

    if (!dimensions) {
      return filter;
    }

    console.log(operator);

    var _loop = function _loop(i) {
      var dimensionName = dimensions[i].dimensionName;
      var orderIndex = filter.order.findIndex(element => {
        return element === dimensionName;
      });

      if (orderIndex === -1) {
        filter.order.push(dimensionName);
      }

      if (!filter.values[dimensionName]) {
        filter.values[dimensionName] = [];
        filter.values[dimensionName].push(_this.formatToFilterFormat(dimensions[i]));
      } else {
        filter.values[dimensionName].push(_this.formatToFilterFormat(dimensions[i]));
      }
    };

    for (var i = 0; i < dimensions.length; i++) {
      _loop(i);
    }

    return filter;
  },

  formatToFilterFormat(dimension) {
    if (dimension.type === 'EXPRESSION') {
      var obj = {
        operator: {
          operator: dimension.operator,
          title: operator[dimension.operator]
        },
        unit: null,
        value: dimension.dimensionValue
      };
      return obj;
    } else {
      return dimension.dimensionValue;
    }
  }

});
// CONCATENATED MODULE: ./src/utils/services/eventBus.js

var eventBus = new vue_esm["a" /* default */]();
// CONCATENATED MODULE: ./src/components/widgetMixin.js






function fetch(config) {}

function readFromLocalStorage(lsKey, storeSetter) {
  var _selectedFilters = localStorage.getItem(lsKey) || '{}';

  _selectedFilters = JSON.parse(_selectedFilters);

  for (var i in _selectedFilters) {
    this.$store.dispatch(storeSetter, {
      filterValueKey: i,
      values: _selectedFilters[i]
    });
  }
}

var oObject = {
  beforeCreate() {
    this.readFromLocalStorage = readFromLocalStorage.bind(this);
  },

  created() {
    if (this.config) {
      for (var key in this.config.widgets) {
        if (this.config.widgets[key].meta && this.config.widgets[key].meta.localFilters) {
          this.config.widgets[key].meta.localFilters = this.config.widgets[key].meta.localFilters.filter(obj => {
            return !(obj.dimensionName === 'search');
          });
        }
      }

      fetch.call(this, this.config);
    }

    if (this.config && this.config.filters && this.config.filters.listen) {
      for (var i in this.config.filters.listen) {
        eventBus.$on(i, function (data, mapping) {
          if (mapping) {
            this.config.mapping = mapping;

            if (this.config.filters.listen[i].transform) {
              this.config = this.config.filters.listen[i].transform(this.config, data, this);
            }

            this.$store.dispatch(this.config.filters.listen[i].action, this.config);
          }
        }.bind(this));
      }
    }
  },

  computed: {
    getMarketPlace() {
      return this.$store.getters.getMarketPlace;
    },

    outsideIn() {
      return this.$store.getters.getOutsideIn;
    },

    getColorPattern() {
      return ['#ffa800', '#bd10e0', '#ff6072', '#97cc04', '#23b5d3', '#f5d908', '#ff909d', '#ffc24c', '#d158ea', '#f8e552', '#b6dc4f', '#65cce1'];
    }

  },

  data() {
    return {
      filterData: [],
      primaryFilterData: [],
      secondaryFilterData: {}
    };
  },

  methods: {
    fetchFilters(cube, endPoint, page, where) {
      var that = this;
      return http_layer.post({
        cube: endPoint || 'FETCH_FILTERS',
        APIData: {
          cubeName: cube,
          pageName: page,
          where: where
        }
      }).then(response => {
        var data = data_transformer.mergeResultDimension(response.data);
        that.filterData = data; // that.primaryFilterData = ((response && response.fullResponse && response.fullResponse.metadata || {}).dimensionMappingData || [])
      });
    },

    getComputedFn(widget) {
      return this._computedWatchers[widget.dataSource].getter();
    },

    applyFilter() {
      if (this.config) {
        fetch.call(this, this.config);
      }
    },

    getAllDates() {
      var maxDates = this.$store.getters.getMaxDate;
      var returnDates = {};
      var dateRangeValues = {};
      var selectedDateRange = this.$store.getters[this.filterState.getter]['date_range'].name;
      var selectedDateRangeValues = this.$store.getters.getDateRangeValues;

      if (selectedDateRange) {
        for (var i in selectedDateRangeValues) {
          dateRangeValues[i] = selectedDateRangeValues[i][selectedDateRange];
        }

        for (i in maxDates) {
          returnDates[i] = maxDates[i].max_feed_date;
        }
      }

      returnDates['dateRange'] = selectedDateRange;
      returnDates['dateRangeValues'] = dateRangeValues;
      return returnDates;
    }

  }
};
/* harmony default export */ var widgetMixin = __webpack_exports__["a"] = (oObject);

/***/ })

}]);
//# sourceMappingURL=ciq-ui.4.js.map